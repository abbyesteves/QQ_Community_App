//
//  ApiService.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 09/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import CouchbaseLiteSwift

class ApiService {
    
    lazy var googlePlaceAPIKey = "AIzaSyDpYyTYlXZPZMIIWMLhiu2rXfIJUUnIe7E"
    
    func get_image(url_str:String, thumbnailView : UIImageView)
    {
        let url:URL = URL(string: url_str)!
        let session = URLSession.shared
        
        let task = session.dataTask(with: url, completionHandler: {
            (
            data, response, error) in
            if data != nil
            {
                let image = UIImage(data: data!)
                if(image != nil)
                {
                    DispatchQueue.main.async(execute: {
                        thumbnailView.image = image
                    })
                }
            }
            if error != nil {
                print("ERROR \(error)")
            }
        })
        task.resume()
    }
    
    func getApi(url : String, array : Array<Any>) {
        var arrayData = array
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = url
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                
                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    let json = try JSONDecoder().decode(Routes.self, from: data)
                    arrayData = json as! [Any]
//                    print("json : ", json)
                    
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }
                
            }.resume()
        }
//        return array
    }
    
    func replication() {
        let url = NSURL(string: "http://192.168.1.96:4984/qq-community/") //http://172.17.0.2:11210/QQCommunity - http://localhost:4984/
//        var pull : CBLReplication = database.createPullReplication(url)
//        pull.channels = ["qqcommunity_traffic"]
//        let push = database.createPushReplication(url)
//        let pull = database.createPullReplication(url)
//        push.continuous = true
//        pull.continuous = true
//        var auth : CBLAuthenticatorProtocol?
//        auth = CBLAuthenticator.basicAuthenticator(withName: "Administrator", password: "password")
//        push.authenticator = auth
//        pull.authenticator = auth
//
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "replicationChanged:", name: kCBLReplicationChangeNotification, object : push)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "replicationChanged:", name: kCBLReplicationChangeNotification, object : pull)
//
//        push.start()
//        pull.start()
//
//        self.push = push
//        self.pull = pull
    }
    
//    func replicationChanged(n: NSNotification){
//        let active = pull.status == CBLReplicationStatus.Active || push.status == CBLReplicationStatus.Active
//        self.activityIndicator.state = active
//        // Now show a progress indicator:
//        self.progressBar.hidden = !active;
//        if active {
//            var progress = 0.0
//            let total = push.changesCount + pull.changesCount
//            let completed = push.completedChangesCount + pull.completedChangesCount
//            if total > 0 {
//                progress = Double(completed) / Double(total);
//            }
//            self.progressBar.progress = progress;
//            print("progress : ", progress)
//        }
//    }
    func tryPullData(){
        //        let replConfig = ReplicatorConfiguration(database: database, targetURL: url)
        //        let database = try! cblManager.databaseNamed("")
        //        let url = NSURL(string:)
    }
    
    func createDBWithName(name: String) {
        do {
            do {
                print("start replication")
                let database = try Database(name: name)
                let url = URL(string: "http://192.168.1.96:4984/qq-community/")! //_all_docs?include_docs=true
                let replConfig = ReplicatorConfiguration(database: database, targetURL: url)
                let replication = Replicator(config: replConfig)
                replication.start()
                
                //                replication.addChangeListener(<#T##block: (ReplicatorChange) -> Void##(ReplicatorChange) -> Void#>)
            } catch let error as NSError {
                print(" ERROR REPLICATION: ",error)
            }
            
            
            //            replication.addChangeListener {_ : in
            //                print(replicatorChange)
            //            }
            
            //            replication.addChangeListener(_ :) {_ in
            //                public void changed(Replicator replicator, Replicator.Status status, CouchbaseLiteException error) {
            //                    if (status.getActivityLevel().equals(Replicator.ActivityLevel.STOPPED)) {
            //                        Log.d("app", "Replication was completed.");
            //                    }
            //                }
            //            }
            
            //            print("my database \(database)")
        } catch let error as NSError {
            NSLog("Cannot open the database: %@", error);
        }
    }
}

