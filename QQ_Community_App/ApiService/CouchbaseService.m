//
//  DemoAppDelegate.m
//  QQ_Community_App
//
//  Created by Abby Esteves on 01/30/2018.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//
//#import "RootViewController.h"
#import "CouchbaseService.h"
//#import "../Controller/NewsViewController.swift"
#import <Couchbaselite/CouchbaseLite.h>

#define localDatabase @"qq-community"
#define serverURL @"http://192.168.1.96:4984/qq-community/"

@implementation DemoAppDelegate
{
    NSURL* remoteSyncURL;
    CBLReplication* _pull;
    CBLReplication* _push;
    NSError* _syncError;
}

@synthesize database;


-(void)pullPush {
    NSURL* serverDbURL = [NSURL URLWithString: serverURL];
    _pull = [database createPullReplication: serverDbURL];
    _push = [database createPushReplication: serverDbURL];
    _pull.continuous = _push.continuous = YES;
    // Observe replication progress changes, in both directions:
    NSNotificationCenter* nctr = [NSNotificationCenter defaultCenter];
    [nctr addObserver: self selector: @selector(replicationProgress:)
                 name: kCBLReplicationChangeNotification object: _pull];
    [nctr addObserver: self selector: @selector(replicationProgress:)
                 name: kCBLReplicationChangeNotification object: _push];
    [_push start];
    [_pull start];
}


- (void) replicationProgress: (NSNotificationCenter*)n {
    if (_pull.status == kCBLReplicationActive || _push.status == kCBLReplicationActive) {
        // Sync is active -- aggregate the progress of both replications and compute a fraction:
        unsigned completed = _pull.completedChangesCount + _push.completedChangesCount;
        unsigned total = _pull.changesCount+ _push.changesCount;
        NSLog(@"SYNC progress: %u / %u", completed, total);
        // Update the progress bar, avoiding divide-by-zero exceptions:
//        [self.rootViewController showSyncStatus: (completed / (float)MAX(total, 1u))];
    } else {
        // Sync is idle -- hide the progress bar and show the config button:
        NSLog(@"SYNC idle");
//        [self.rootViewController hideSyncStatus];
    }

    // Check for any change in error status and display new errors:
    NSError* error = _pull.lastError ? _pull.lastError : _push.lastError;
    if (error != _syncError) {
        _syncError = error;
        if (error) {
            [self showAlert: @"Error syncing" error: error fatal: NO];
        }
    }
}


// Display an error alert, without blocking.
// If 'fatal' is true, the app will quit when it's dismissed.
- (void)showAlert: (NSString*)message error: (NSError*)error fatal: (BOOL)fatal {
    if (error) {
        message = [message stringByAppendingFormat: @"\n\n%@", error.localizedDescription];
    }
    NSLog(@"ALERT: %@ (error=%@)", message, error);
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: (fatal ? @"Fatal Error" : @"Error")
                                                    message: message
                                                   delegate: (fatal ? self : nil)
                                          cancelButtonTitle: (fatal ? @"Quit" : @"Sorry")
                                          otherButtonTitles: nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    exit(0);
}




@end
