//
//  DemoAppDelegate.h
//  QQ_Community_App
//
//  Created by Abby Esteves on 01/30/2018.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//
#import <UIKit/UIKit.h>
@class CBLDatabase, RootViewController;


@interface DemoAppDelegate : NSObject <UIApplicationDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) CBLDatabase *database;

- (void)showAlert: (NSString*)message error: (NSError*)error fatal: (BOOL)fatal;

@end

