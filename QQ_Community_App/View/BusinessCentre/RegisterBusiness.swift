//
//  RegisterBusiness.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 29/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class RegisterBusiness: BaseCell, UITextFieldDelegate {
    
//    let category = ["Restaurant", "Hotel", "Personal Care", "Bank", "Education", "Service", "Others"]
    let nameBoarder = UIView()
    let addressBoarder = UIView()
    let contactBoarder = UIView()
    let emailBoarder = UIView()
    static var businessName = ""
    static var businessAddress = ""
    static var contactNumber = ""
    static var emailAddress = ""
    
    let dropPressView = UIView()
    let dropView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 0.5
        view.backgroundColor = UIColor.white
        return view
    }()
    let dropOptionView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: 5)
        view.layer.shadowRadius = 8
        return view
    }()
    
    let restaurantButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Restaurant", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let hotelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Hotel", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let personalCareButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Personal Care", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let bankButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Bank", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let educationButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Education", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let serviceButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Service", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    
    let otherButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Others", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let dropSelectedText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = label.font.withSize(15)
        label.text = "Restaurant"
        return label
    }()
    
    let dropImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_down")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.gray
        return image
    }()
    
    //
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.text = "Business Name"
        label.textColor = UIColor.gray
        label.font = label.font.withSize(12)
        return label
    }()
    
    let addressLabel : UILabel = {
        let label = UILabel()
        label.text = "Business Address"
        label.textColor = UIColor.gray
        label.font = label.font.withSize(12)
        return label
    }()
    
    let contactLabel : UILabel = {
        let label = UILabel()
        label.text = "Contact Number"
        label.textColor = UIColor.gray
        label.font = label.font.withSize(12)
        return label
    }()
    
    let emailLabel : UILabel = {
        let label = UILabel()
        label.text = "E-mail Address"
        label.textColor = UIColor.gray
        label.font = label.font.withSize(12)
        return label
    }()

    let titleHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Join our Business Centre"
        return label
    }()
    
    let categoryHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.text = "Business Category"
        return label
    }()
    
    let scanHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(11)
        label.text = "SCANNED COPY OF THE FF."
        return label
    }()
    
    let dtiHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(13)
        label.text = "DTI Certificate"
        return label
    }()
    
    let dtiButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.backgroundColor = UIColor.lightGray
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        button.setTitle("UPLOAD", for: .normal)
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 1
        button.layer.cornerRadius = 1
        button.addTarget(self, action: #selector(dtiTapped), for: .touchUpInside)
        return button
    }()
    
    let businessPermitHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(13)
        label.text = "Business Permit"
        return label
    }()
    
    let businessPermitButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.backgroundColor = UIColor.lightGray
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        button.setTitle("UPLOAD", for: .normal)
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 1
        button.layer.cornerRadius = 1
        button.addTarget(self, action: #selector(businessPermitTapped), for: .touchUpInside)
        return button
    }()
    
    //

    let nameText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.layer.borderColor = UIColor.gray.cgColor
        text.textColor = UIColor.white
        return text
    }()

    let addressText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.layer.borderColor = UIColor.gray.cgColor
        text.textColor = UIColor.white
        return text
    }()

    let contactText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.layer.borderColor = UIColor.gray.cgColor
        text.textColor = UIColor.white
        return text
    }()

    let emailText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.layer.borderColor = UIColor.gray.cgColor
        text.textColor = UIColor.white
        return text
    }()
    
    private func nameAttribute() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.nameText.textColor = UIColor.gray
            self.nameLabel.textColor = UIColor.gray
            self.nameBoarder.backgroundColor = UIColor.gray
            self.nameLabel.font = self.nameLabel.font.withSize(12)
            
            if self.nameText.text?.count == 0 {
                self.nameLabel.frame = CGRect(x: 10, y: 0, width: self.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    private func addressAttribute() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.addressLabel.textColor = UIColor.gray
            self.addressText.textColor = UIColor.gray
            self.addressBoarder.backgroundColor = UIColor.gray
            self.addressLabel.font = self.addressLabel.font.withSize(12)
            
            if self.addressText.text!.count == 0 {
                self.addressLabel.frame = CGRect(x: 10, y: 0, width: self.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    private func contactAttribute() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.contactText.textColor = UIColor.gray
            self.contactLabel.textColor = UIColor.gray
            self.contactBoarder.backgroundColor = UIColor.gray
            self.contactLabel.font = self.contactLabel.font.withSize(12)
            
            if self.contactText.text!.count == 0 {
                self.contactLabel.frame = CGRect(x: 10, y: 0, width: self.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    private func emailAttribute() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.emailText.textColor = UIColor.gray
            self.emailLabel.textColor = UIColor.gray
            self.emailBoarder.backgroundColor = UIColor.gray
            self.emailLabel.font = self.emailLabel.font.withSize(12)
            
            if self.emailText.text!.count == 0 {
                self.emailLabel.frame = CGRect(x: 10, y: 0, width: self.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    // tapped functions
    @objc private func nameTapped() {
        nameText.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.nameLabel.font = self.nameLabel.font.withSize(12)
            self.nameLabel.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.nameLabel.frame = CGRect(x: 0, y: -20, width: self.frame.width-20, height: 20)
            self.nameText.attributedPlaceholder = .none
            self.nameText.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.nameBoarder.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            
            self.contactAttribute()
            self.addressAttribute()
            self.emailAttribute()
        }, completion:  { (Bool) in
            
        })
    }
    
    @objc private func addressTapped() {
        addressText.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.addressLabel.font = self.addressLabel.font.withSize(12)
            self.addressLabel.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.addressLabel.frame = CGRect(x: 0, y: -20, width: self.frame.width-20, height: 20)
            self.addressText.attributedPlaceholder = .none
            self.addressText.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.addressBoarder.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            
            self.nameAttribute()
            self.contactAttribute()
            self.emailAttribute()
        }, completion:  { (Bool) in
            
        })
    }
    
    @objc private func contactTapped() {
        contactText.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.contactLabel.font = self.contactLabel.font.withSize(12)
            self.contactLabel.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.contactLabel.frame = CGRect(x: 0, y: -20, width: self.frame.width-20, height: 20)
            self.contactText.attributedPlaceholder = .none
            self.contactText.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.contactBoarder.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            
            self.nameAttribute()
            self.addressAttribute()
            self.emailAttribute()
        }, completion:  { (Bool) in
            
        })
    }
    
    @objc private func emailTapped() {
        emailText.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.emailLabel.font = self.emailLabel.font.withSize(12)
            self.emailLabel.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.emailLabel.frame = CGRect(x: 0, y: -20, width: self.frame.width-20, height: 20)
            self.emailText.attributedPlaceholder = .none
            self.emailText.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            self.emailBoarder.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
            
            self.nameAttribute()
            self.contactAttribute()
            self.addressAttribute()
        }, completion:  { (Bool) in })
    }
    
    @objc private func dtiTapped() {
        
    }
    
    @objc private func businessPermitTapped() {
        
    }
    
    private func setupConstraints() {
        
        let width = frame.width-100
        nameLabel.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 20)
        contactLabel.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 20)
        addressLabel.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 20)
        emailLabel.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 20)
        dtiButton.frame = CGRect(x: width, y: 0, width: 80, height: 25)
        businessPermitButton.frame = CGRect(x: width, y: 0, width: 80, height: 25)
        
        addConstraintsFormat(format: "H:|[v0]|", views: nameBoarder)
        addConstraintsFormat(format: "V:|-30-[v0(1)]|", views: nameBoarder)
        addConstraintsFormat(format: "H:|[v0]|", views: addressBoarder)
        addConstraintsFormat(format: "V:|-30-[v0(1)]|", views: addressBoarder)
        addConstraintsFormat(format: "H:|[v0]|", views: contactBoarder)
        addConstraintsFormat(format: "V:|-30-[v0(1)]|", views: contactBoarder)
        addConstraintsFormat(format: "H:|[v0]|", views: emailBoarder)
        addConstraintsFormat(format: "V:|-30-[v0(1)]|", views: emailBoarder)

        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: dtiHeader)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: businessPermitHeader)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: scanHeader)
        addConstraintsFormat(format: "H:|-10-[v0][v1(15)]-10-|", views: dropSelectedText, dropImage)
        addConstraintsFormat(format: "V:|-12.5-[v0(15)]|", views: dropImage)
        addConstraintsFormat(format: "V:|[v0]|", views: dropSelectedText)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: dropView)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: categoryHeader)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: titleHeader)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: nameText)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: addressText)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: contactText)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: emailText)
        addConstraintsFormat(format: "V:|-10-[v0(30)]|", views: titleHeader)
        addConstraintsFormat(format: "V:|-70-[v0(35)]-20-[v1(35)]-20-[v2(35)]-20-[v3(35)]-35-[v4(20)]-10-[v5(40)]-25-[v6(20)]-20-[v7(25)]-10-[v8(25)]", views: nameText, addressText, contactText, emailText, categoryHeader, dropView, scanHeader, dtiHeader, businessPermitHeader)
    }
    
    private func setupGestures() {
        nameText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nameTapped)))
        addressText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addressTapped)))
        contactText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(contactTapped)))
        emailText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emailTapped)))
        dropView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dropTapped)))
    }
    
    @objc private func dropTapped(){
        self.nameText.resignFirstResponder()
        self.addressText.resignFirstResponder()
        self.contactText.resignFirstResponder()
        self.emailText.resignFirstResponder()
        
        if let window = UIApplication.shared.keyWindow {
            dropPressView.backgroundColor = UIColor(white: 0, alpha: 0)
            
            dropPressView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            restaurantButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(restaurantTapped)))
            hotelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hotelTapped)))
            personalCareButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(personalCareTapped)))
            bankButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bankTapped)))
            educationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(educationTapped)))
            serviceButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceTapped)))
            otherButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(otherTapped)))
            
            window.addSubview(dropPressView)
            window.addSubview(dropOptionView)
            dropOptionView.addSubview(restaurantButton)
            dropOptionView.addSubview(hotelButton)
            dropOptionView.addSubview(personalCareButton)
            dropOptionView.addSubview(bankButton)
            dropOptionView.addSubview(educationButton)
            dropOptionView.addSubview(serviceButton)
            dropOptionView.addSubview(otherButton)
            
            restaurantButton.alpha = 0
            hotelButton.alpha = 0
            bankButton.alpha = 0
            personalCareButton.alpha = 0
            educationButton.alpha = 0
            serviceButton.alpha = 0
            otherButton.alpha = 0
            
            dropOptionView.frame = CGRect(x: 10, y: dropView.frame.maxY+25, width: frame.width-20, height: 0)
            dropPressView.alpha = 0
            dropPressView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            
            dropOptionView.addConstraintsFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(20)]-5-[v5(20)]-5-[v6(20)]-10-|", views: restaurantButton, hotelButton, personalCareButton, bankButton, educationButton, serviceButton, otherButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: restaurantButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: hotelButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: personalCareButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: bankButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: educationButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: serviceButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: otherButton)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.dropPressView.alpha = 1
                self.dropOptionView.frame = CGRect(x: 10, y: self.dropView.frame.maxY+25, width: self.frame.width-20, height: 190)
                
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.restaurantButton.alpha = 1
                    self.hotelButton.alpha = 1
                    self.bankButton.alpha = 1
                    self.personalCareButton.alpha = 1
                    self.educationButton.alpha = 1
                    self.serviceButton.alpha = 1
                    self.otherButton.alpha = 1
                }, completion: { (Bool) in })
            })
            
        }
    }
    
    @objc private func restaurantTapped() {
        dropSelectedText.text = "Restaurant"
        self.bgDismiss()
    }
    
    @objc private func hotelTapped() {
        dropSelectedText.text = "Hotel"
        self.bgDismiss()
    }
    
    @objc private func personalCareTapped() {
        dropSelectedText.text = "Personal Care"
        self.bgDismiss()
    }
    
    @objc private func bankTapped() {
        dropSelectedText.text = "Bank"
        self.bgDismiss()
    }
    
    @objc private func educationTapped() {
        dropSelectedText.text = "Education"
        self.bgDismiss()
    }
    
    @objc private func serviceTapped() {
        dropSelectedText.text = "Service"
        self.bgDismiss()
    }
    
    @objc private func otherTapped() {
        dropSelectedText.text = "Others"
        self.bgDismiss()
    }
    
    func ifAnswered() -> Bool {
        if nameText.text! != "" &&
            addressText.text! != "" &&
            contactText.text! != "" &&
            addressText.text! != "" {
            return true
        }
        return false
    }
    
    
    
    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.restaurantButton.alpha = 0
            self.hotelButton.alpha = 0
            self.bankButton.alpha = 0
            self.personalCareButton.alpha = 0
            self.educationButton.alpha = 0
            self.serviceButton.alpha = 0
            self.otherButton.alpha = 0
            
            
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.dropPressView.alpha = 0
                self.dropOptionView.frame = CGRect(x: 10, y: self.dropView.frame.maxY+25, width: self.frame.width-20, height: 0)
            }, completion: { (Bool) in
                self.dropOptionView.removeFromSuperview()
            })
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameText {
            
        }
        return false
    }
    
    
    override func setupViews() {
        super.setupViews()
        
        contactText.keyboardType = UIKeyboardType.numberPad
        
        nameBoarder.backgroundColor = UIColor.gray
        addressBoarder.backgroundColor = UIColor.gray
        contactBoarder.backgroundColor = UIColor.gray
        emailBoarder.backgroundColor = UIColor.gray
        backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)

        addSubview(titleHeader)
        addSubview(nameText)
        nameText.addSubview(nameLabel)
        nameText.addSubview(nameBoarder)
        addSubview(addressText)
        addressText.addSubview(addressLabel)
        addressText.addSubview(addressBoarder)
        addSubview(contactText)
        contactText.addSubview(contactLabel)
        contactText.addSubview(contactBoarder)
        addSubview(emailText)
        emailText.addSubview(emailLabel)
        emailText.addSubview(emailBoarder)
        addSubview(categoryHeader)
        addSubview(dropView)
        dropView.addSubview(dropSelectedText)
        dropView.addSubview(dropImage)
        addSubview(scanHeader)
        addSubview(dtiHeader)
        dtiHeader.addSubview(dtiButton)
        addSubview(businessPermitHeader)
        businessPermitHeader.addSubview(businessPermitButton)
        
        setupConstraints()
        setupGestures()
    }
    
}
