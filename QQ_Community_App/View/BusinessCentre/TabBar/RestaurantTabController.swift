//
//  TabOneViewController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 01/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class RestaurantTabController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    let recommendedId = "recommendedId"
    let newId = "newId"
    
    let restaurantLabel: UILabel = {
        let label = UILabel()
        label.text = "RESTAURANTS"
        label.textColor = UIColor.white
        label.font = label.font.withSize(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let recommendedLabel: UILabel = {
        let label = UILabel()
        label.text = "RECOMMENDED"
        label.textColor = UIColor.white
        label.font = label.font.withSize(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let newlyLabel: UILabel = {
        let label = UILabel()
        label.text = "NEWLY OPENED"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 125)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let seeAllButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = button.titleLabel?.font.withSize(13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitle("See All", for: .normal)
        return button
    }()
    
    let seeAllImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "ic_next")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    lazy var businessDetail: BusinessDetail = {
        let launcher = BusinessDetail()
        return launcher
    }()
    
    var newlyOpened: [BusinessFeed] = {

        let data1 = BusinessFeed()
        data1.thumbnailImage = "https://equitablepaperworksupport.files.wordpress.com/2013/02/small-business-store-front-i.jpg"
        data1.name = "Restaurant Name 1"
        data1.contact = "(044) 766 2037"
        data1.category = "CATEGORY 1"
        data1.rating = 4.0
        data1.about = "Restaurant 1 description"
        data1.hours = "9:00 AM – 9:00 PM"
        data1.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        let data2 = BusinessFeed()
        data2.thumbnailImage = "https://images.containerstore.com/medialibrary/images/locations/locatorAnimation/animate-Store4-AVA.jpg"
        data2.name = "Restaurant Name 2"
        data2.contact = "(044) 766 2037"
        data2.category = "CATEGORY 2"
        data2.rating = 4.0
        data2.about = "Restaurant 2 description"
        data2.hours = "9:00 AM – 9:00 PM"
        data2.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        let data3 = BusinessFeed()
        data3.thumbnailImage = "http://anewscafe.com/wp-content/uploads/2010/11/enjoy-the-store-1.jpeg"
        data3.name = "Restaurant Name 3"
        data3.contact = "(044) 766 2037"
        data3.category = "CATEGORY 3"
        data3.rating = 4.0
        data3.about = "Restaurant 3 description"
        data3.hours = "9:00 AM – 9:00 PM"
        data3.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        let data4 = BusinessFeed()
        data4.thumbnailImage = "http://www.spreevahq.com/assets/target-retail-store-location-night-cbff70245cd4fe514abeb423a2c088a24a2f2a61cb88531bb81f1a6afe907a60.jpg"
        data4.name = "Restaurant Name 4"
        data4.contact = "(044) 766 2037"
        data4.category = "CATEGORY 4"
        data4.rating = 4.0
        data4.about = "Restaurant 4 description"
        data4.hours = "9:00 AM – 9:00 PM"
        data4.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        let data5 = BusinessFeed()
        data5.thumbnailImage = "https://c.s-microsoft.com/en-au/CMSImages/SYD_flaghship_storefront.jpg?version=fcc2919f-2108-cae5-7ce8-60d8c84380ae"
        data5.name = "Restaurant Name 5"
        data5.contact = "(044) 766 2037"
        data5.category = "CATEGORY 5"
        data5.rating = 4.0
        data5.about = "Restaurant 5 description"
        data5.hours = "9:00 AM – 9:00 PM"
        data5.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        let data6 = BusinessFeed()
        data6.thumbnailImage = "https://c.s-microsoft.com/en-au/CMSImages/SYD_flaghship_storefront.jpg?version=fcc2919f-2108-cae5-7ce8-60d8c84380ae"
        data6.name = "Restaurant Name 6"
        data6.contact = "(044) 766 2037"
        data6.category = "CATEGORY 6"
        data6.rating = 4.0
        data6.about = "Restaurant 6 description"
        data6.hours = "9:00 AM – 9:00 PM"
        data6.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"

        return [data1, data2, data3, data4, data5, data6]
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let bannerHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }

    func setUpViews(){
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(RecommendedRestaurantCell.self, forCellWithReuseIdentifier: recommendedId)
        collectionView.register(BusinessNewCell.self, forCellWithReuseIdentifier: newId)
        
        view.addSubview(bannerHeader)
        view.addSubview(collectionView)
        collectionView.addSubview(seeAllButton)
        seeAllButton.addSubview(seeAllImage)
        
        collectionView.addSubview(recommendedLabel)
        collectionView.addSubview(newlyLabel)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        view.addConstraintsFormat(format: "V:|[v0(200)]|", views: bannerHeader)
        view.addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        view.addConstraintsFormat(format: "V:|[v0]|", views: collectionView)
        view.addConstraintsFormat(format: "H:|-25-[v0]|", views: recommendedLabel)
        view.addConstraintsFormat(format: "V:|-20-[v0(20)]", views: recommendedLabel)
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-85)-[v0(60)]|", views: seeAllButton)
        view.addConstraintsFormat(format: "V:|-20-[v0(20)]", views: seeAllButton)
        
        view.addConstraintsFormat(format: "H:|-45-[v0(15)]|", views: seeAllImage)
        view.addConstraintsFormat(format: "V:|-2.5-[v0(15)]|", views: seeAllImage)
        
        view.addConstraintsFormat(format: "H:|-25-[v0]|", views: newlyLabel)
        view.addConstraintsFormat(format: "V:|-300-[v0(20)]", views: newlyLabel)
        
        seeAllButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToAll)))
    }
    
    @objc func goToAll(){
        let layout = UICollectionViewFlowLayout()
        let seeAllDetail = SeeAllDetail(collectionViewLayout: layout)
        navigationController?.pushViewController(seeAllDetail, animated: true)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return newlyOpened.count
        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: newId, for: indexPath) as! BusinessNewCell
            cell.backgroundColor = UIColor.white
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = CGSize(width: 0, height: 3)
            cell.layer.shadowRadius = 2
            cell.layer.cornerRadius = 2
            cell.businessFeed = newlyOpened[indexPath.item]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: recommendedId, for: indexPath) as! RecommendedRestaurantCell
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 1 {
            return CGSize(width: (view.frame.width)-50, height: 110)
        }
        return CGSize(width: view.frame.width, height: 300)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        businessDetail.openCell(name: newlyOpened[indexPath.item].name!,
                 about: newlyOpened[indexPath.item].about!,
                 address: newlyOpened[indexPath.item].address!,
                 hours: newlyOpened[indexPath.item].hours!,
                 category : newlyOpened[indexPath.item].category!,
                 image: newlyOpened[indexPath.item].thumbnailImage!,
                 rating: CGFloat(newlyOpened[indexPath.item].rating!),
                 contact: newlyOpened[indexPath.item].contact!)
    }

}

class RecommendedRestaurantCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let recommendedId = "recommendedId"
    
    lazy var businessDetail: BusinessDetail = {
        let launcher = BusinessDetail()
        return launcher
    }()
    
    var recommended: [BusinessFeed] = {
        let data1 = BusinessFeed()
        data1.thumbnailImage = "http://3.bp.blogspot.com/-HI-sF0dB7zQ/UjKTmbt4GLI/AAAAAAAAAGA/LQy_oURS3rQ/s1600/SAM_0181.JPG"
        data1.name = "La Familia Sizzlers & Catering Services"
        data1.contact = "(044) 766 2037"
        data1.category = "FILIPINO"
        data1.rating = 4.0
        data1.about = "The family that cooks together stays together. This much is true with La Familia Restaurant, a dining place established and ran by the Alejo family. Located at a place accessible to drivers, La Familia serves as a convenient food stop for both travelers and patrons. Today, most of La Familia's customers are also families who want to enjoy lutong bahay dishes."
        data1.hours = "9:00 AM – 9:00 PM"
        data1.address = "Doña Remedios Trinidad Hwy, Baliuag, Bulacan"
        
        
        let data2 = BusinessFeed()
        data2.thumbnailImage = "http://www.jobhero.ph/media/emp/emp_7071.jpg"
        data2.name = "Hapag Kainan"
        data2.contact = "(044) 671 5680"
        data2.category = "FILIPINO"
        data2.rating = 4.5
        data2.about = "Hapag Kainan is a Filipino restaurant that traces its humble beginnings to a catering business. It also functions as a venue for special occasions such as birthday parties and wedding receptions."
        data2.hours = "10:00 AM – 9:00 PM"
        data2.address = "Super 8 Grocery Ware House, Dr Gonzales Street, Baliuag, 3006 Bulacan"
        
        
        let data3 = BusinessFeed()
        data3.thumbnailImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/01461jfAugustine_Square_Rosemary%27s_Kitchen_Landmarks_Baliwag_Highwayfvf_19.jpg/1280px-01461jfAugustine_Square_Rosemary%27s_Kitchen_Landmarks_Baliwag_Highwayfvf_19.jpg"
        data3.name = "Rosemary's Kitchen"
        data3.contact = "0943 342 7555"
        data3.category = "BUFFET"
        data3.rating = 4
        data3.about = "Rosemary’s Kitchen is one of the home grown restaurants in Bulacan. They offer an eat-all-you-can buffet of various Filipino dishes for only ₱199 per person."
        data3.hours = "10:00 AM – 10:00 PM"
        data3.address = "3 Pan-Philippine Hwy Baliuag 3006 Pan-Philippine Hwy, Baliuag, 3006"
        
        
        let data4 = BusinessFeed()
        data4.thumbnailImage = "https://igx.4sqi.net/img/general/200x200/2928825_lGhDF93GpBgRdN_2a9bRqC57dbDCgWURHJ2DvpGzkyY.jpg"
        data4.name = "Bulalo Republic"
        data4.contact = "0927 576 7290"
        data4.category = "FILIPINO"
        data4.rating = 3.5
        data4.about = "Bulalo Republic, Filipino Restaurant, found in Baliwag that specializing in Filipino cuisine and assure to serve only the best and freshest dishes."
        data4.hours = "6:00 AM – 11:00 PM"
        data4.address = "575 Doña Remedios Trinidad Hwy, Tarcan, Baliuag, 3006 Bulacan"
        
        
        return [data1, data2, data3, data4]
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
    }
    
    func setUpViews() {
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsetsMake(70, 0, 50, 0)
        collectionView.register(BusinessRecommendedCell.self, forCellWithReuseIdentifier: recommendedId)
        
        addSubview(collectionView)
        addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsFormat(format: "V:|[v0]|", views: collectionView)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommended.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: recommendedId, for: indexPath) as! BusinessRecommendedCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.businessFeed = recommended[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: frame.height-60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 14, 0, 14)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        businessDetail.openCell(name: recommended[indexPath.item].name!,
                 about: recommended[indexPath.item].about!,
                 address: recommended[indexPath.item].address!,
                 hours: recommended[indexPath.item].hours!,
                 category : recommended[indexPath.item].category!,
                 image: recommended[indexPath.item].thumbnailImage!,
                 rating: CGFloat(recommended[indexPath.item].rating!),
                 contact: recommended[indexPath.item].contact!)
    }
}
