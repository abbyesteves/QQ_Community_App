//
//  TabBarController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 26/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    static var tabSelected = ""
    static var indexSelected = 0
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func selectedIndex(int: Int, tab : String) {
        TabBarController.indexSelected = int
        TabBarController.tabSelected = tab
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        
        let restaurantTab = RestaurantTabController()
        let restaurantTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_food"), selectedImage: UIImage(named: "ic_food"))
        restaurantTab.tabBarItem = restaurantTabBarItem
//        let navigationController = UINavigationController(rootViewController: restaurantTab)
//        navigationController.title = ""
//        navigationController.tabBarItem.image = UIImage(named: "ic_work")
        
        let hotelTab = HotelTabController()
        let hotelTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_bed"), selectedImage: UIImage(named: "ic_bed"))
        hotelTab.tabBarItem = hotelTabBarItem
        
        let personalCareTab = PersonalCareTabController()
        let personalTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_barbershop"), selectedImage: UIImage(named: "ic_barber"))
        personalCareTab.tabBarItem = personalTabBarItem
        
        let bankTab = BankTabController()
        let bankTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_receive_cash"), selectedImage: UIImage(named: "ic_receive_cash"))
        bankTab.tabBarItem = bankTabBarItem
        
        let educationCareTab = EducationTabController()
        let educationTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_graduation_cap"), selectedImage: UIImage(named: "ic_graduation_cap"))
        educationCareTab.tabBarItem = educationTabBarItem
        
        let serviceCareTab = ServicesTabController()
        let serviceTabBarItem = UITabBarItem(title: .none, image: UIImage(named: "ic_work"), selectedImage: UIImage(named: "ic_work"))
        serviceCareTab.tabBarItem = serviceTabBarItem
        
        self.viewControllers = [restaurantTab, hotelTab, personalCareTab, bankTab, educationCareTab, serviceCareTab]
        
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.Theme(alpha: 1.0)
        moreNavigationController.navigationBar.isHidden = true
        
        self.selectedIndex = TabBarController.indexSelected
        self.navigationItem.title = TabBarController.tabSelected
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        print("SELECTED :", self.selectedIndex)
        let index = self.selectedIndex
//        DispatchQueue.main.async(execute: {
            if index == 0 {
                self.navigationItem.title = "Restaurants"
            } else if index == 1 {
                self.navigationItem.title = "Hotels"
            } else if index == 2 {
                self.navigationItem.title = "Personal Care"
            } else if index == 3 {
                self.navigationItem.title = "Banks"
            } else if index == 4 {
                self.navigationItem.title = "Educations"
            } else if index == 5 {
                self.navigationItem.title = "Services"
            }
//        })
    }
    
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
//        print("Selected view controller", tabBarController.selectedIndex)
    }
    
}
