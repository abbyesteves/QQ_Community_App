//
//  PaymentOptions.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 29/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
class PaymentOptions: BaseCell {
    
    let municipalBody = UIView()
    let paypalBody = UIView()
    let creditCardBody = UIView()
    let municipalView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let payPalView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let creditCardView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let bannerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(18)
        label.text = "Payment Options"
        return label
    }()
    
    let amountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.text = "100 PHP"
        return label
    }()
    
    let registrationLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(15)
        label.text = "Registration Fee:"
        return label
    }()
    
    let municipalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(10)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Pay at the Municipal Hall"
        return label
    }()
    
    let municipalImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_building")
//        image.image = UIImage(named: "ic_down")?.withRenderingMode(.alwaysTemplate)as UIImage?
//        image.tintColor = UIColor.gray
        return image
    }()
    
    let payPalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(10)
        label.text = "Pay via PayPal"
        return label
    }()
    
    let payPalImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_paypal")
        return image
    }()
    
    let creditCardLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(10)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Credit Card or Debit Card"
        return label
    }()
    
    let creditCardImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_credit_card")
        return image
    }()
    
    let doneImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_almost_done")
        return image
    }()
    
    let doneLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(20)
        label.textAlignment = .center
        label.text = "Almost Done!"
        return label
    }()
    
    let doneDescLabel: UITextView = {
        let textView = UITextView()
        textView.text = "Please proceed to Baliwag Municipal Hall Treasurer's Office to pay the registration fee. For your convenience, other payment options are also available."
        textView.textColor = UIColor.darkGray
        textView.backgroundColor = UIColor.Background(alpha: 1.0)
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    //
    let nameBoarder = UIView()
    let nameText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Fullname", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let addressBoarder = UIView()
    let addressText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Address", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let cardNumberBoarder = UIView()
    let cardNumberText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Card Number", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 35)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let expiryBoarder = UIView()
    let expiryText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Expiry", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 30)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let cvcBoarder = UIView()
    let cvcText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "CVC", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 35)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let zipBoarder = UIView()
    let zipText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "ZIP", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 30)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let confirmPaymentButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("CONFIRM PAYMENT", for: .normal)
        return button
    }()
    
    private func setupConstraints() {
        
        let middle = frame.width/2-50
        self.municipalBody.alpha = 1
        municipalBody.frame = CGRect(x: 0, y: 200, width: frame.width, height: frame.height-200)
        self.paypalBody.alpha = 0
        paypalBody.frame = CGRect(x: 0, y: 200, width: frame.width, height: frame.height-200)
        self.creditCardBody.alpha = 0
        creditCardBody.frame = CGRect(x: 0, y: 200, width: frame.width, height: frame.height-200)
        
        addConstraintsFormat(format: "H:|[v0]|", views: bannerView)
        addConstraintsFormat(format: "V:|[v0(200)]|", views: bannerView)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: registrationLabel)
        addConstraintsFormat(format: "H:|-\(frame.width-100)-[v0(100)]-20-|", views: amountLabel)
        addConstraintsFormat(format: "V:|[v0]|", views: amountLabel)
        addConstraintsFormat(format: "V:|-20-[v0(20)]-10-[v1(25)]", views: titleLabel, registrationLabel)
        addConstraintsFormat(format: "H:|-20-[v0(100)]-20-|", views: municipalView)
        addConstraintsFormat(format: "H:|-\(middle)-[v0(100)][v1(100)]-20-|", views:  payPalView, creditCardView)
        addConstraintsFormat(format: "V:|-\(200-110)-[v0(100)]|", views: municipalView)
        addConstraintsFormat(format: "V:|-\(200-110)-[v0(100)]|", views: payPalView)
        addConstraintsFormat(format: "V:|-\(200-110)-[v0(100)]|", views: creditCardView)
        
        addConstraintsFormat(format: "H:|-25-[v0(50)]|", views: payPalImage)
        addConstraintsFormat(format: "H:|[v0]|", views: payPalLabel)
        addConstraintsFormat(format: "V:|-10-[v0(50)][v1]|", views: payPalImage, payPalLabel)
        
        addConstraintsFormat(format: "H:|-25-[v0(50)]|", views: municipalImage)
        addConstraintsFormat(format: "H:|[v0]|", views: municipalLabel)
        addConstraintsFormat(format: "V:|-10-[v0(50)][v1]|", views: municipalImage, municipalLabel)
        
        addConstraintsFormat(format: "H:|-25-[v0(50)]|", views: creditCardImage)
        addConstraintsFormat(format: "H:|[v0]|", views: creditCardLabel)
        addConstraintsFormat(format: "V:|-10-[v0(50)][v1]|", views: creditCardImage, creditCardLabel)
        
        addConstraintsFormat(format: "H:|-\(municipalBody.frame.width/2-50)-[v0(100)]-20-|", views: doneImage)
        addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: doneLabel)
        addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: doneDescLabel)
        addConstraintsFormat(format: "V:|-\(municipalBody.frame.height/2-100)-[v0(100)]-10-[v1(25)][v2]-50-|", views: doneImage, doneLabel, doneDescLabel)
        
        addConstraintsFormat(format: "H:|[v0]|", views: addressBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: addressBoarder)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: addressText)
        addConstraintsFormat(format: "H:|[v0]|", views: nameBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: nameBoarder)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: nameText)
        addConstraintsFormat(format: "H:|[v0]|", views: cardNumberBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: cardNumberBoarder)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: cardNumberText)
        addConstraintsFormat(format: "H:|[v0]|", views: expiryBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: expiryBoarder)
        addConstraintsFormat(format: "H:|[v0]|", views: cvcBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: cvcBoarder)
        addConstraintsFormat(format: "H:|[v0]|", views: zipBoarder)
        addConstraintsFormat(format: "V:|-29.5-[v0(1.5)]|", views: zipBoarder)
        addConstraintsFormat(format: "V:|-\(municipalBody.frame.height/2+20)-[v0(30)]|", views: cvcText)
        addConstraintsFormat(format: "V:|-\(municipalBody.frame.height/2+20)-[v0(30)]|", views: zipText)
        addConstraintsFormat(format: "H:|-20-[v0(\(frame.width/3-15))]-5-[v1(\(frame.width/3-15))]-5-[v2(\(frame.width/3-15))]-20-|", views: expiryText, cvcText, zipText)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: confirmPaymentButton)
        addConstraintsFormat(format: "V:|-\(municipalBody.frame.height/2-130)-[v0(30)]-20-[v1(30)]-20-[v2(30)]-20-[v3(30)]-25-[v4(40)]", views: nameText, addressText, cardNumberText, expiryText, confirmPaymentButton)
    }
    
    private func setupGestures() {
        municipalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(municipalTapped)))
        payPalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(paypalTapped)))
        creditCardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(creditcardTapped)))
    }
    
    @objc private func municipalTapped() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.municipalView.backgroundColor = UIColor.white
            self.municipalImage.alpha = 1
            
            self.municipalBody.alpha = 1
            self.creditCardBody.alpha = 0
            self.paypalBody.alpha = 0
            
            self.payPalImage.alpha = 0.5
            self.payPalView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
            self.creditCardImage.alpha = 0.5
            self.creditCardView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
            
        }, completion: { (Bool) in })
        
    }
    
    @objc private func paypalTapped() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.payPalView.backgroundColor = UIColor.white
            self.payPalImage.alpha = 1
            
            self.municipalBody.alpha = 0
            self.creditCardBody.alpha = 0
            self.paypalBody.alpha = 1
            
            self.municipalImage.alpha = 0.5
            self.municipalView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
            self.creditCardImage.alpha = 0.5
            self.creditCardView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
            
        }, completion: { (Bool) in })
    }
    
    @objc private func creditcardTapped() {
        
      
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.creditCardView.backgroundColor = UIColor.white
            self.creditCardImage.alpha = 1
            
            self.municipalBody.alpha = 0
            self.creditCardBody.alpha = 1
            self.paypalBody.alpha = 0
            
            self.municipalImage.alpha = 0.5
            self.municipalView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
            self.payPalImage.alpha = 0.5
            self.payPalView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        }, completion: { (Bool) in })
    }
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.Background(alpha: 1.0)
        nameBoarder.backgroundColor = UIColor.gray
        addressBoarder.backgroundColor = UIColor.gray
        cardNumberBoarder.backgroundColor = UIColor.gray
        expiryBoarder.backgroundColor = UIColor.gray
        cvcBoarder.backgroundColor = UIColor.gray
        zipBoarder.backgroundColor = UIColor.gray
        
        payPalImage.alpha = 0.5
        creditCardImage.alpha = 0.5
        
        addSubview(bannerView)
        bannerView.addSubview(titleLabel)
        bannerView.addSubview(registrationLabel)
        registrationLabel.addSubview(amountLabel)
        bannerView.addSubview(municipalView)
        municipalView.addSubview(municipalImage)
        municipalView.addSubview(municipalLabel)
        bannerView.addSubview(payPalView)
        payPalView.addSubview(payPalImage)
        payPalView.addSubview(payPalLabel)
        bannerView.addSubview(creditCardView)
        creditCardView.addSubview(creditCardImage)
        creditCardView.addSubview(creditCardLabel)
        addSubview(municipalBody)
        municipalBody.addSubview(doneImage)
        municipalBody.addSubview(doneLabel)
        municipalBody.addSubview(doneDescLabel)
        addSubview(paypalBody)
        paypalBody.addSubview(nameText)
        nameText.addSubview(nameBoarder)
        paypalBody.addSubview(addressText)
        addressText.addSubview(addressBoarder)
        
        addSubview(creditCardBody)
        creditCardBody.addSubview(nameText)
        nameText.addSubview(nameBoarder)
        creditCardBody.addSubview(addressText)
        addressText.addSubview(addressBoarder)
        creditCardBody.addSubview(cardNumberText)
        cardNumberText.addSubview(cardNumberBoarder)
        creditCardBody.addSubview(expiryText)
        expiryText.addSubview(expiryBoarder)
        creditCardBody.addSubview(cvcText)
        cvcText.addSubview(cvcBoarder)
        creditCardBody.addSubview(zipText)
        zipText.addSubview(zipBoarder)
        creditCardBody.addSubview(confirmPaymentButton)
        
        setupConstraints()
        setupGestures()
    }
    
}
