//
//  SeeBusinessController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 04/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class SeeBusinessController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let page1Id = "registerBusiness",
    page2Id = "paymentOptions"
    
    lazy var surveyBasicInfo : SurveyBasicInfo = {
        let launcher = SurveyBasicInfo()
        return launcher
    }()
    
    lazy var surveySelect: SurveySelect = {
        let launcher = SurveySelect()
        return launcher
    }()
    
    private let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 2
        pc.backgroundColor = UIColor.white
        pc.currentPageIndicatorTintColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        pc.pageIndicatorTintColor = UIColor.rgba(red: 237, green: 237, blue: 237, alpha: 1.0)
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("NEXT", for: .normal)
        button.tag = 6
        button.addTarget(self, action: #selector(nextClicked), for: .touchUpInside)
        return button
    }()
    
    let nextImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_next")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.gray
        return image
    }()
    
    let prevButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("PREV", for: .normal)
        button.tag = 6
        button.addTarget(self, action: #selector(prevClicked), for: .touchUpInside)
        return button
    }()
    
    let prevImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_previous")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.gray
        return image
    }()
    
    lazy var registerBusiness : RegisterBusiness = {
        let launcher = RegisterBusiness()
        return launcher
    }()
    
    @objc func nextClicked() {
        print("next clicked", pageControl.currentPage, registerBusiness.ifAnswered())
        
//        if registerBusiness.ifAnswered() {
                next()
//        } else {
//            view.showToast(message: "Complete Further Details.")
//        }
    }
    
    private func next() {
        prevButton.alpha = 1
        nextButton.alpha = 0
       
        
        let index = pageControl.currentPage + 1
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = index
    }
    
    @objc func prevClicked() {
        //        print("prev clicked", pageControl.currentPage)
        nextButton.alpha = 1
        prevButton.alpha = 0
      
        let index = pageControl.currentPage - 1
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = index
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.item == 1){
            return collectionView.dequeueReusableCell(withReuseIdentifier: page2Id, for: indexPath)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: page1Id, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    private func setupView(){
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView!.showsHorizontalScrollIndicator = false
        collectionView?.isScrollEnabled = false
        collectionView?.register(RegisterBusiness.self, forCellWithReuseIdentifier: page1Id)
        collectionView?.register(PaymentOptions.self, forCellWithReuseIdentifier: page2Id)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.isPagingEnabled = true
        
        view.addSubview(pageControl)
        pageControl.addSubview(nextButton)
        nextButton.addSubview(nextImage)
        pageControl.addSubview(prevButton)
        prevButton.addSubview(prevImage)
        
    }
    
    private func setupConstraints(){
        view.addConstraintsFormat(format: "H:|[v0]|", views: pageControl)
        view.addConstraintsFormat(format: "V:|-\(view.frame.height-110)-[v0(50)]|", views: pageControl)
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-100)-[v0(80)]|", views: nextButton)
        view.addConstraintsFormat(format: "V:|-15-[v0(25)]|", views: nextButton)
        view.addConstraintsFormat(format: "H:|-65-[v0(15)]|", views: nextImage)
        view.addConstraintsFormat(format: "V:|-5-[v0(15)]|", views: nextImage)
        
        view.addConstraintsFormat(format: "H:|-10-[v0(80)]|", views: prevButton)
        view.addConstraintsFormat(format: "V:|-15-[v0(25)]|", views: prevButton)
        view.addConstraintsFormat(format: "H:|[v0(15)]|", views: prevImage)
        view.addConstraintsFormat(format: "V:|-5-[v0(15)]|", views: prevImage)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        prevButton.alpha = 0
        
        setupView()
        setupConstraints()
    }
}
