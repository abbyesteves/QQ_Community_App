//
//  FeedCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 10/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import CouchbaseLiteSwift

class NewsTabCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var newsController: NewsViewController?
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    let cellId = "cellId"
    var newsCell: NewsCell?
    var Articles = Array<Any>()
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    //dummy data for news feed
//    var news: [News] = {
//        var announcements0 = News()
//        announcements0.thumbnailImage = "https://www.baliwag.gov.ph/wp-content/uploads/2017/05/Top15TaxPayerOfBaliwag.jpg"
//        announcements0.title = "Top 15 Business Taxpayers of Baliwag for 2017 represented by their officials."
//        announcements0.content = ""
//        announcements0.date = ""
//
//        var announcements1 = News()
//        announcements1.thumbnailImage = "https://www.baliwag.gov.ph/wp-content/uploads/2017/05/pension2.jpg"
//        announcements1.title = "Pensions for Indigent Senior Citizens"
//        announcements1.content = "Mga Lolo at Lola ng Baliwag Masayang Tinanggap Ang Kanilang Pension"
//        announcements1.date = "June 15, 2017"
//
//        var announcements2 = News()
//        announcements2.thumbnailImage = "https://www.baliwag.gov.ph/wp-content/uploads/2017/05/protect_mgabata3.jpg"
//        announcements2.title = "Proteksyon sa mga Bata: Mas Pinaigting sa Baliwag"
//        announcements2.content = "Malaking tulong po ang mga ganitong programa lalo na sa aming barangay"
//        announcements2.date = "May 26, 2017"
//        //        announcements2.description = "Ang Baliwag Mobile App ay naglalaman ng mga impormasyon ukol sa Baliwag at mga establisyimento nito."
//
//        var announcements3 = News()
//        announcements3.thumbnailImage = "https://www.baliwag.gov.ph/wp-content/uploads/2017/05/cleanUp2.jpg"
//        announcements3.title = "MENRO Leads Barangka Clean up"
//        announcements3.content = "Clean-up Drive is a part of the municipality’s endeavor to maintain the cleanliness"
//        announcements3.date = "May 17, 2017"
//        //        announcements3.description = "Baliwag set its best foot forward to achieve peaceful celebration of the upcoming Undas 2017 as Baliwagenyos commemorate with their departed love ones.
//
//        var announcements4 = News()
//        announcements4.thumbnailImage = "https://www.baliwag.gov.ph/wp-content/uploads/2017/01/BPLS-Baliwag-Cover-Photo-1024x777.jpg"
//        announcements4.title = "Paano mag-apply ng business license?"
//        announcements4.content = "Paalala: Hanggang January 20 na lamang po ang business permit application."
//        announcements4.date = "May 8, 2017"
//        //        announcements4.description = "Baliwag set its best foot forward to achieve peaceful celebration of the upcoming Undas 2017 as Baliwagenyos commemorate with their departed love ones.
//
//        return [announcements0, announcements1, announcements2, announcements3, announcements4]
//    }()
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let bannerHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let loadingView  : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        view.layer.cornerRadius = 5
        return view
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return 3
        return Articles.count //news.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewsCell
        
//        if(indexPath.item != 0){
            cell.backgroundColor = UIColor.white
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = CGSize(width: 0, height: 3)
            cell.layer.shadowRadius = 2
            cell.layer.cornerRadius = 2
//        }
//        cell.backgroundColor = UIColor.white
        cell.article = Articles[indexPath.item] as! Articles //news[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width-20, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = Articles[indexPath.item] as! Articles
        webDetail.openCell(url: data.url!, sourceName: data.source.name)
    }
    
    private func getApi(completion: @escaping () -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = "https://newsapi.org/v2/everything?q=bocaue&sortBy=publishedAt&apiKey=5b58e21f40ff43128da960349a77b934"
            guard let url = URL(string: urlString) else { return }

            URLSession.shared.dataTask(with: url) { (data, response, err) in
//                print("here")
                guard let data = data else { return }

                do {
                    let headlines = try JSONDecoder().decode(Headlines.self, from: data)
                    self.Articles = headlines.articles
                    completion()
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }

            }.resume()
        }
    }
    
    private func setupView(){
        self.isUserInteractionEnabled = false
        //light grayish background color
        backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 243, alpha: 1.0)
        loadingView.alpha = 0
        
        title.text = "News and Announcements"
        
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        
        addSubview(collectionView)
        collectionView.addSubview(title)
        insertSubview(bannerHeader, belowSubview: collectionView)
        addSubview(loadingView)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        addConstraintsFormat(format: "H:|-\(frame.width/2-50)-[v0(100)]|", views: loadingView)
        addConstraintsFormat(format: "V:|-\(frame.height/2-50)-[v0(100)]|", views: loadingView)
        
        addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsFormat(format: "V:|[v0]|", views: collectionView)
        
        addConstraintsFormat(format: "V:|-\(frame.height/2-10)-[v0(20)]|", views: activityIndicator)
        addConstraintsFormat(format: "H:|[v0]|", views: activityIndicator)
        
        addConstraintsFormat(format: "H:|-20-[v0]|", views: title)
        addConstraintsFormat(format: "V:|-(-100)-[v0(120)]", views: title)
        addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        addConstraintsFormat(format: "V:|-(-20)-[v0(200)]|", views: bannerHeader)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.loadingView.alpha = 1
        }, completion:  nil)
        
    }
    
    override func setupViews() {
        super.setupViews()
        setupView()
        
//        getApi(searchFor: "bocaue")
        getApi {
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.loadingView.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    self.collectionView.reloadData()
                    self.isUserInteractionEnabled = true
                }, completion:  { (Bool) in
                    
                })
            })
        }
        
        self.newsCell?.descriptionText.removeFromSuperview()
        collectionView.scrollsToTop = true
        collectionView.contentInset = UIEdgeInsetsMake(70, 0, 10, 0)
        collectionView.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 243, alpha: 0)
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: cellId)
    }
}






