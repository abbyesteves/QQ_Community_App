//
//  BfpCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 10/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BfpTabCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    //dummy data for news feed
    var safety: [Safety] = {
        
        var data1 = Safety()
        data1.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/130.jpg"
        data1.title = "Tip no. 1: Be fire-safety conscious"
        data1.content = "Everyone is encouraged to be fire-safety conscious as fire safety is everyone’s concern."
        
        var data2 = Safety()
        data2.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/227.jpg"
        data2.title = "Tip no. 2: Keep matches out of children’s reach"
        data2.content = "For their safety (and our homes’), matches and lighters should be placed in cupboards or drawers out of children’s reach."
        
        var data3 = Safety()
        data3.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/321.jpg"
        data3.title = "Tip no. 3: Keep lit candles away from combustible materials"
        data3.content = "When lighting candles, make sure that they placed away from combustible materials, such as curtains, newspapers, etc."
        
        var data4 = Safety()
        data4.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/419.jpg"
        data4.title = "Tip no. 4: Do not use substandard electrical wirings and equipment"
        data4.content = "Faulty electrical wiring is one of the main causes of fires, so make sure to use only the best type."
        
        var data5 = Safety()
        data5.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/514.jpg"
        data5.title = "Tip no. 5: Avoid using your electric fan continuously for 24 hours"
        data5.content = "To prevent them from overheating, electric fans should be switched off after several hours of continuous use."
        
        var data6 = Safety()
        data6.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/613.jpg"
        data6.title = "Tip no. 6: Do not smoke in bed"
        data6.content = "Lit cigarette butts can easily ignite inflammable materials, such as bed sheets and pillowcases, so avoid smoking in bed."
        
        
        var data7 = Safety()
        data7.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/79.jpg"
        data7.title = "Tip no. 7: Unplug and shut off electrical equipment and LPG tanks"
        data7.content = "When leaving the house, make sure that all electrical equipment and LPG tanks are properly disconnected and shut off."
        
        
        var data8 = Safety()
        data8.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/87.jpg"
        data8.title = "Tip no. 8: Never leave your kitchen while cooking"
        data8.content = "Do not leave cooking food unattended. If you have to leave the kitchen, turn off the stove and take the pans and pots off the heat."
        
        
        var data9 = Safety()
        data9.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/95.jpg"
        data9.title = "Tip no. 9: Keep them clean and grease-free"
        data9.content = "Your stove must be kept clean and grease-free at all times. In addition, use soapy water to check your LPG hose and connectors for any leaks."
        
        
        var data10 = Safety()
        data10.thumbnailImage = "http://www.lamudi.com.ph/journal/wp-content/uploads/2015/07/104.jpg"
        data10.title = "Tip no. 10: Avoid octopus connections"
        data10.content = "Overloading of electrical outlets and using octopus connections and worn-out cords are some of the leading causes of household fires."
        
        return [data1, data2, data3, data4, data5, data6, data7, data8, data9, data10]
    }()
    
    //view declarations
    let cellId = "cellId"
//    var safetyCell: SafetyCell?
    
    lazy var safetyCell: SafetyCell = {
        let mb = SafetyCell()
        return mb
    }()
    
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    lazy var highlightHeader: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.lightGray
        imageView.image = UIImage(named: "Top15TaxPayerOfBaliwag")
        return imageView;
    }()
    
    let bannerHeader: UIView = {
        let view = UIView()
        //green bg
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return safety.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SafetyCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 0.5
        cell.layer.cornerRadius = 5
        
        cell.safety = safety[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let height = (view.frame.width) / CGFloat(news.count)*118
        let titleLabel = safety[indexPath.item].title!
        let descriptionLabel = safety[indexPath.item].content!
        let heightName = heightEstimation(text: titleLabel, width: frame.width-40, size: 16, defaultHeight : 20)
        let heightDescription = (heightEstimation(text: descriptionLabel, width: frame.width-40, size: 13, defaultHeight : 5)/2)
        let height = 220+heightName+heightDescription+15
        return CGSize(width: frame.width-40, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func setupViews() {
        super.setupViews()
        overrideData()
        
        collectionView.contentInset = UIEdgeInsetsMake(70, 0, 10, 0)
        collectionView.backgroundColor = UIColor.Background(alpha: 0)
        collectionView.register(SafetyCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    private func setUpConstraints(){
        addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsFormat(format: "V:|[v0]|", views: collectionView)
        
        addConstraintsFormat(format: "H:|-20-[v0]|", views: title)
        addConstraintsFormat(format: "V:|-(-100)-[v0(120)]|", views: title)
        
        addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        addConstraintsFormat(format: "V:|-(-20)-[v0(200)]|", views: bannerHeader)
        
    }
    
    private func setUpAddView(){
        collectionView.addSubview(title)
        insertSubview(bannerHeader, belowSubview: collectionView)
        addSubview(collectionView)
    }
    
    func overrideData(){
        //light grayish background color
        
        backgroundColor = UIColor.Background(alpha: 1.0)
        title.text = "Fire Safety"
        
        setUpAddView()
        setUpConstraints()
        
    }
}
