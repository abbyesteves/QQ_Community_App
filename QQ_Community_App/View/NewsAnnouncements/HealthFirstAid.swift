//
//  FirstAid.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 21/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class HealthFirstAid: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    //dummy data for news feed
    var safety: [Safety] = {
        let data1 = Safety()
        data1.thumbnailImage = "http://pad1.whstatic.com/images/thumb/1/1c/Do-Basic-First-Aid-Step-4-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-4-Version-2.jpg"
        data1.title = "1. Determine responsiveness"
        data1.content = "If a person is unconscious, try to rouse them by speaking to them; do not be afraid to speak up. If they do not respond to activity, sound, touch, or other stimulation, determine whether they are breathing."
        
        let data2 = Safety()
        data2.thumbnailImage = "http://pad3.whstatic.com/images/thumb/a/a2/Do-Basic-First-Aid-Step-5-Version-2.jpg/728px-Do-Basic-First-Aid-Step-5-Version-2.jpg"
        data2.title = "2. Check for breathing and a pulse."
        data2.content = "If unconscious and unable to be roused, check for breathing: look for a rise in the chest area; listen for the sound of air coming in and out; feel for air using the side of your face. If no signs of breathing are apparent, place two fingers under the chin and gently guide the face pointing upwards to open up their airways. If any debris such as vomit can be seen, it is appropriate to move them onto their side to allow it to get out, which is achieved with the recovery position. Check for a pulse."
        
        let data3 = Safety()
        data3.thumbnailImage = "http://pad1.whstatic.com/images/thumb/c/c1/Do-Basic-First-Aid-Step-6-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-6-Version-2.jpg"
        data3.title = "3. If the person remains unresponsive, prep for CPR."
        data3.content = "Unless you suspect a spinal injury, carefully roll them onto their back and open their airway. If you suspect a spinal injury, leave the person where they are, provided they are breathing.\n - Keep the head and neck aligned.\n - Carefully roll them onto their back while holding their head.\n - Open the airway by lifting the chin."
        
        let data4 = Safety()
        data4.thumbnailImage = "http://pad2.whstatic.com/images/thumb/f/f9/Do-Basic-First-Aid-Step-7-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-7-Version-2.jpg"
        data4.title = "4. Perform 30 chest compressions and two rescue breaths as part of CPR."
        data4.content = "In the center of the chest, just below an imaginary line running between the nipples, put your two hands together and compress the chest down approximately 2 inches (5.1 cm) at a rate of 100 compressions per minute (or to the beat of \"Staying Alive\"). After 30 compressions, give two rescue breaths, done by opening the airways, closing the nose and fully covering the mouth hole. Then check vitals. If the breaths are blocked, reposition the airway. Make sure the head is tilted slightly back and the tongue is not obstructing it. Continue this cycle of 30 chest compressions and two rescue breaths until someone else relieves you."
        
        let data5 = Safety()
        data5.thumbnailImage = "http://pad3.whstatic.com/images/thumb/f/f2/Do-Basic-First-Aid-Step-8-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-8-Version-2.jpg"
        data5.title = "5. Remember your ABCs of CPR."
        data5.content = "The ABCs of CPR refer to the three critical things you need to look for. Check these three things frequently as you give the person first aid CPR. \n - Airway. Does the person have an unobstructed airway?\n - Breathing. Is the person breathing?\n - Circulation. Does the person show a pulse at major pulse points (wrist, carotid artery, groin)?"
        
        let data6 = Safety()
        data6.thumbnailImage = "http://pad3.whstatic.com/images/thumb/6/6c/Do-Basic-First-Aid-Step-9-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-9-Version-2.jpg"
        data6.title = "6. Make sure the person is warm as you wait for medical help"
        data6.content = "Drape a towel or a blanket over the person if you have one; if you don't, remove some of your own clothing (such as your coat or jacket) and use it as a cover until medical help arrives. However, if the person has a heatstroke, do not cover him or keep him warm. Instead try to cool him by fanning him and damping him."
        
        let data7 = Safety()
        data7.thumbnailImage = "http://pad1.whstatic.com/images/thumb/6/61/Do-Basic-First-Aid-Step-10-Version-2.jpg/aid544437-v4-728px-Do-Basic-First-Aid-Step-10-Version-2.jpg"
        data7.title = "7. Pay attention to a list of don'ts."
        data7.content = "As you administer first aid, be sure to be aware of these things that you should not do in any case: \n - Do not feed or hydrate an unconscious person. This could cause choking and possible asphyxiation.\n - Do not leave the person alone. Unless you absolutely need to signal or call for help, stay with the person at all times.\n - Do not prop up an unconscious person's head with a pillow.\n - Do not slap or splash with water an unconscious person's face. These are movie gimmicks.\n - If the person appears in danger due to an electric shock, you may attempt to move it, but only with a non-conductive object."

        return [data1, data2, data3, data4, data5, data6, data7]
    }()
    //view declarations
    var cellId = "cellId"
    
    let bannerView = UIView()
    let backView = UIView()
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let mainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    lazy var firsAidView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Basic First Aid"
        return label
    }()
    
    @objc func setup(){
        if let window = UIApplication.shared.keyWindow {
            bannerView.backgroundColor = UIColor.Theme(alpha: 1.0)
    
            window.addSubview(mainView)
            window.addSubview(bannerView)
            window.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(bannerView)
            mainView.addSubview(backView)
            firsAidView.addSubview(title)
            mainView.addSubview(firsAidView)
            
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Exit)))
            title.frame = CGRect(x: 20, y: -50, width: window.frame.width, height:  30)
            firsAidView.frame = CGRect(x: 0, y: 80, width: window.frame.width, height:  window.frame.height-80)
            backImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            mainView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha:1.0)
            backView.frame = CGRect(x: 10, y: 40, width: 30, height:  30)
            bannerView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height:  250)
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height:  window.frame.height)
            //animation appear view
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: self.mainView.frame.height)
                
            }, completion:  nil)
        }
    }
    
    @objc func Exit() {
//        safety.removeAll()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.mainView.frame = CGRect(x: 0, y: self.mainView.frame.height, width: self.mainView.frame.width, height: self.mainView.frame.height)
            
        }, completion:  nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return safety.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SafetyCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 0.5
        cell.layer.cornerRadius = 5
        
        cell.safety = safety[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let heightName = firsAidView.heightEstimation(text: safety[indexPath.item].title!, width: firsAidView.frame.width-40, size: 13, defaultHeight : 20)
        let heightDescription = (firsAidView.heightEstimation(text: safety[indexPath.item].content!, width: firsAidView.frame.width-40, size: 12, defaultHeight : 5)/2)
        return CGSize(width: firsAidView.frame.width-40, height: ((220+heightName)+heightDescription)+15)
//        return CGSize(width: firsAidView.frame.width-40, height: firsAidView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override init() {
        super.init()
        firsAidView.scrollsToTop = true
        firsAidView.contentInset = UIEdgeInsetsMake(50, 0, 10, 0)
        firsAidView.backgroundColor = UIColor.Theme(alpha: 1.0)
        firsAidView.register(SafetyCell.self, forCellWithReuseIdentifier: cellId)
        
    }

}
