//
//  TwitterTabCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 19/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterTabCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, TWTRTweetViewDelegate {

    let tweetView = TWTRTweetView()
    let tweetController = TWTRTimelineViewController()
    let tableView = UITableViewCell()

    static var tweets = Array<Any>()
    let cellId = "twitterId"
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()

    let profileView = UIView()
    
    let loadingView  : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        view.layer.cornerRadius = 5
        return view
    }()

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()

    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Twitter"
        return label
    }()

    let usernameText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "@username"
        return label
    }()

    let descText: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.font = textView.font?.withSize(13)
        textView.text = "description"
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()

    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 3
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 40
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.borderColor = UIColor.ThemeDarkSecond(alpha: 1.0).cgColor
        return imageView
    }()

    let errorText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "error"
        return label
    }()

    let bannerHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let loadLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(13)
        label.text = "We've desable all action. \n Let us load your data."
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()

    func getTwitterApi() {
        // Change url make sure url is on public
        let url = URL(string: "https://twitter.com/PhBocaue")

        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.sync {
                    if let errorMessage = error?.localizedDescription {
                        self.errorText.text = errorMessage
                    } else {
                        self.errorText.text = "Something went wrong. \n \n Try Again."
                    }
                }
            } else {
                let webContent : String = String(data: data!, encoding : String.Encoding.utf8)!
//                print("WEB CONTENT ",webContent)

                //name
                var array:Array<Any> = webContent.components(separatedBy: "<title>")
                array = (array[1] as AnyObject).components(separatedBy: " (")
                let name = array[0]
                //username
                array = (array[1] as AnyObject).components(separatedBy: ")")
                let userName = array[0]
                array.removeAll()
                //get description
                array = webContent.components(separatedBy: "<meta name=\"description\" content=\"The latest Tweets from \(name) (\(userName)). ")
                array = (array[1] as AnyObject).components(separatedBy: "\">")
                let description = array[0]
//                print("NAME : ", userName)

                // profile pic
                array.removeAll()
                array = webContent.components(separatedBy: "data-resolved-url-large=\"")
                array = (array[1] as AnyObject).components(separatedBy: "\"")
                let profilePicture = array[0]

                // Get tweets
                array.removeAll()
                array = webContent.components(separatedBy: "data-aria-label-part=\"0\">")
                array.remove(at: 0)
                let picOfTweeter = webContent.components(separatedBy: "js-action-profile-avatar\" src=\"")
                let nameOfTweeter = webContent.components(separatedBy: "<strong class=\"fullname show-popup-with-id u-textTruncate \" data-aria-label-part>")
                let userNameOfTweeter = nameOfTweeter[1].components(separatedBy: "data-aria-label-part>@<b>")[1].components(separatedBy: "</b>")[0]
                
                for i in 0..<array.count{
                    let addTweet = Tweets()
                    let tweet = (array[i] as AnyObject).components(separatedBy: "data-pre-embedded=\"true\" dir=\"ltr\" >")
                    let upimages = (array[i] as AnyObject).components(separatedBy: "<img data-aria-label-part src=\"")
                    let finaImage = upimages[0].components(separatedBy: "data-image-url=\"")
                    if finaImage.count == 2 {
                        addTweet.uploadedImage = finaImage[1].components(separatedBy: "\"")[0]
                    } else {
                        addTweet.uploadedImage = ""
                    }
                    addTweet.content = tweet[0]
                    addTweet.name = nameOfTweeter[1].components(separatedBy: "</strong>")[0]
                    addTweet.userName = "@\(userNameOfTweeter)"
                    addTweet.thumbnailImage = picOfTweeter[1].components(separatedBy: "\" alt=\"\">")[0]
                    array[i] = addTweet
        
                }
                TwitterTabCell.tweets = array
                DispatchQueue.main.sync {
                    self.title.text = name as! String
                    self.usernameText.text = userName as! String
                    self.descText.text = description as! String
                    self.collectionView.reloadData()

                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.title.alpha = 1
                        self.usernameText.alpha = 0.7
                        self.descText.alpha = 1
                        self.loadingView.alpha = 0
                        self.loadLabel.alpha = 0
                        self.collectionView.alpha = 1
                        self.getImageURL(url: profilePicture as! String)
                        self.activityIndicator.stopAnimating()
                        self.isUserInteractionEnabled = true
                    }, completion:  { (Bool) in })
                }
            }
        }.resume()
    }

    private func getImageURL(url: String){
        thumbnailView.alpha = 1
        let url = URL(string: url)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            DispatchQueue.main.sync {
                self.thumbnailView.image = UIImage(data: data!)
            }
        }.resume()
    }

    private func setupView() {
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        
        loadingView.backgroundColor = UIColor(white: 0, alpha: 0.6)
        loadingView.alpha = 0
        title.alpha = 0
        usernameText.alpha = 0
        descText.alpha = 0
        loadLabel.alpha = 0
        thumbnailView.alpha = 0
        collectionView.alpha = 0

        addSubview(collectionView)
//        addSubview(tweetView)
        insertSubview(bannerHeader, belowSubview: collectionView)
        collectionView.addSubview(profileView)
        profileView.addSubview(title)
        profileView.addSubview(thumbnailView)
        profileView.addSubview(usernameText)
        profileView.addSubview(descText)
        addSubview(loadingView)
        addSubview(loadLabel)
        addSubview(activityIndicator)
//        addSubview(tweetView)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.loadingView.alpha = 1
            self.activityIndicator.startAnimating()
        }, completion:  { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.loadLabel.alpha = 1
            }, completion:  { (Bool) in
//                UIView.animate(withDuration: 0.10, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.loadLabel.text = "It's longer than expected. \n Make sure you're connected in a fast connection."
//                }, completion: nil)
            })
        })
    }

    private func setupConstraints(){
//        addConstraintsFormat(format: "H:|[v0]|", views: tweetView)
//        addConstraintsFormat(format: "V:|[v0]|", views: tweetView)
        addConstraintsFormat(format: "H:|-\(frame.width/2-50)-[v0(100)]|", views: loadingView)
        addConstraintsFormat(format: "V:|-\(frame.height/2-50)-[v0(100)]|", views: loadingView)
        
        addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsFormat(format: "V:|[v0]|", views: collectionView)

        addConstraintsFormat(format: "H:|[v0]|", views: profileView)
        addConstraintsFormat(format: "V:|-(-180)-[v0(130)]|", views: profileView)
        
        addConstraintsFormat(format: "V:|-\(frame.height/2-10)-[v0(10)]-60-[v1]", views: activityIndicator, loadLabel)
        addConstraintsFormat(format: "H:|[v0]|", views: loadLabel)
        addConstraintsFormat(format: "H:|[v0]|", views: activityIndicator)

        addConstraintsFormat(format: "V:|-15-[v0(80)]|", views: thumbnailView)
        addConstraintsFormat(format: "H:|-20-[v0(80)]-10-[v1]-20-|", views: thumbnailView, title)
        addConstraintsFormat(format: "V:|-10-[v0(25)]-5-[v1(15)][v2(100)]", views: title, usernameText, descText)
        addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        addConstraintsFormat(format: "H:|-110-[v0(\(frame.width-140))]", views: descText)
        addConstraintsFormat(format: "H:|-110-[v0(\(frame.width-110))]-20-|", views: usernameText)
        addConstraintsFormat(format: "V:|-(-20)-[v0(200)]|", views: bannerHeader)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TwitterTabCell.tweets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TwitterCell
        cell.backgroundColor = UIColor.white
        cell.tweet = TwitterTabCell.tweets[indexPath.item] as! Tweets
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tweet = TwitterTabCell.tweets[indexPath.item] as! Tweets
        let heightTweet = heightEstimation(text: "\(tweet.content!.htmlToString) \n \(tweet.uploadedImage!)", width: frame.width-20, size: 13, defaultHeight: 20)
        if heightTweet < 80{
            return CGSize(width: frame.width-20, height: 80)
        }
        return CGSize(width: frame.width-20, height: heightTweet)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    override func setupViews() {
        super.setupViews()
        self.isUserInteractionEnabled = false
        backgroundColor = UIColor.Background(alpha: 1.0)

        setupView()
        setupConstraints()
        getTwitterApi()

        tweetView.showActionButtons = true

        collectionView.scrollsToTop = true
        collectionView.contentInset = UIEdgeInsetsMake(200, 0, 10, 0)
        collectionView.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 243, alpha: 0)
        collectionView.register(TwitterCell.self, forCellWithReuseIdentifier: cellId)
    }
}

class ListTimelineViewController: TWTRTimelineViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = TWTRUserTimelineDataSource(screenName: "PhBocaue", apiClient: TWTRAPIClient())
    }
}




