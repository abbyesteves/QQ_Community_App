//
//  HealthTabHeartCare.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class HealthHeartCare: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    //dummy data for news feed
    var safety: [Safety] = {
        let data1 = Safety()
        data1.thumbnailImage = "http://pad3.whstatic.com/images/thumb/7/75/Quit-Smoking-when-You-Don%27t-Really-Want-to-Step-17.jpg/aid1356644-v4-728px-Quit-Smoking-when-You-Don%27t-Really-Want-to-Step-17.jpg"
        data1.title = "1. Stop all forms of tobacco use."
        data1.content = "Using tobacco increases your risk of heart damage. Both tobacco and nicotine contain many chemicals that cause harm to your blood vessels and heart, and this results in atherosclerosis, which is a plaque build-up of cholesterol, fat, and calcium in your vessels that can cause your arteries to narrow, decreasing blood flow. \n The carbon monoxide in cigarette smoke has also been linked to mortality and morbidity. It interferes with oxygen, so your heart is pressured into supplying additional oxygen to compensate. The tightening of the blood vessels, partnered by the stress on the heart can both result in a heart attack. The only way to stop this burden on your heart and strive for a healthier heart is to stop smoking.\n Approximately 1 in 5 deaths in the United States is caused by cigarette smoking. According to the National Institute of Health, smoking is the main preventable cause of mortality in the United States."
        
        let data2 = Safety()
        data2.thumbnailImage = "http://pad3.whstatic.com/images/thumb/b/b6/Exercise-Step-9-Version-2.jpg/aid1356644-v4-728px-Exercise-Step-9-Version-2.jpg"
        data2.title = "2. Incorporate exercise into your daily routine."
        data2.content = "One way to make any muscle stronger is to exercise it. The same is true for your heart. Here is what the American Heart Association recommends:\n At least 30 minutes a day of moderate-intensity aerobic exercise to get your blood pumping and dramatically improve your heart's health. Ideally, you should do this 5 days a week for a total of 150 minutes of aerobic exercise.\n Alternatively, you can do 25 minutes of high-intensity aerobic activity for a minimum of 3 days a week, totaling 75 minutes in all.\n You should also incorporate resistance training (weight/strength training) into your workout at least 2 days every week in addition to the cardio.\n Always work up to a healthy routine! Only begin with what you are comfortable doing, and then systematically increase the difficulty as you are able to tolerate it. Too strenuous a routine too quickly can actually strain your heart and defeat the purpose. If you have any health conditions, discuss with your doctor before beginning any exercise routine."
        
        let data3 = Safety()
        data3.thumbnailImage = "http://pad2.whstatic.com/images/thumb/5/5d/Use-a-Scale-Step-23.jpg/aid1356644-v4-728px-Use-a-Scale-Step-23.jpg"
        data3.title = "3. Maintain a healthy weight."
        data3.content = "Increased weight causes your body to require more effort from your heart to maintain a baseline resting level. This continual strain on your heart can tax it and result in further issues in the future. Exercise and a healthy diet will help you lose the weight that is putting a strain on your heart. Dangerous cardiac issues that arise from being overweight include:\n Coronary heart disease - This condition arises from plaque build-up inside the arteries that serve your heart. Plaques can narrow your arteries as they grow and decrease the amount of blood flow, reducing the amount of oxygen that can be supplied to your body. In addition, your heart must work harder to push blood through the narrowed channels, which can cause angina (chest pain from oxygen deprivation) or even a heart attack.\n High blood pressure - If your heart has to pump harder to get the appropriate amount of oxygen and nutrients through your body, the vessels and your heart can become damaged over time. Your risk of high blood pressure is significantly greater when you are obese or overweight.\n Stroke - If a plaque that has developed in your arteries ruptures, the plaque can cause a blood clot to form. If the clot forms in close proximity to your brain, your brain can become deprived of blood and oxygen, resulting in a stroke."
        
        let data4 = Safety()
        data4.thumbnailImage = "http://pad1.whstatic.com/images/thumb/e/ef/Reduce-High-Blood-Pressure-Step-1-Version-2.jpg/aid1356644-v4-728px-Reduce-High-Blood-Pressure-Step-1-Version-2.jpg"
        data4.title = "4. Make a habit of getting regular screenings of your blood pressure and cholesterol levels."
        data4.content = "Doing so will keep you informed of your heart's health and allow you to take action before anything serious develops.\n - Blood pressure screenings - You should check your blood pressure every two years. If your blood pressure is above 120/80, then your doctor will likely recommend you have your pressure checked every year (or more depending on how high the reading is or if you have kidney problems, heart disease, etc.)[6] Your workplace or pharmacy may also offer free, automated blood pressure machines. Use these as often as you want to supplement actual visits to your doctor. If your blood pressure is above 140/90 and your doctor is not aware, it is important that you contact your doctor as soon as possible.\n - Cholesterol Screening - All men above the age of 34 should be screened every five years. Your doctor will draw blood samples and have them tested in the lab for cholesterol levels. He or she will go over the results and readings with you. If you have any risk factors that could make you more likely to have high cholesterol, it is recommended that you be screened as early as 20 years of age. Risk factors may include immediate family history, diabetes, or prior heart disease.[7] Depending on a regular workup, your doctor may request that you are screened more often."
        
        let data5 = Safety()
        data5.thumbnailImage = "http://pad2.whstatic.com/images/thumb/6/6d/Reduce-High-Blood-Pressure-Step-7-Version-2.jpg/aid1356644-v4-728px-Reduce-High-Blood-Pressure-Step-7-Version-2.jpg"
        data5.title = "5. Avoid too much stress."
        data5.content = "Stress can play a huge role in your heart health. Increased stress releases cortisol and adrenaline, which elevates blood pressure and cholesterol levels. Stress-related behaviors can also negatively affect your health, causing you to smoke more, drink more, overeat, and be physically inactive. All these behaviors will contribute negatively to your heart health.\n- Exercise, diet, and abstinence from smoking and coffee can help to reduce stress. You should consider these practices in your life particularly when you are stressed."
        
        let data6 = Safety()
        data6.thumbnailImage = "http://pad1.whstatic.com/images/thumb/e/ec/Overcome-the-Fear-of-Injections-Step-15.jpg/aid1356644-v4-728px-Overcome-the-Fear-of-Injections-Step-15.jpg"
        data6.title = "6. Manage your mental health."
        data6.content = "Certain mental health conditions can lead to detrimental behaviors for your heart health. These include depression and anxiety disorders, which include disorders such as bipolar disorder and OCD. These behaviors can present with symptoms of excessive eating, decreased eating, apathy, physical inactivity, stress, elevated blood pressure, and many other symptoms that negatively impact your heart.\n - If you are diagnosed with any mental health condition or believe that you may be suffering from one, be sure to visit your doctor as soon as possible. Only your doctor can effectively treat your mental health condition, as well as determine the effect it may have on the rest of your physical health."
        
        
        return [data1, data2, data3, data4, data5, data6]
    }()
    //view declarations
    var cellId = "cellId"
    
    let bannerView = UIView()
    let backView = UIView()
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let mainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    lazy var firsAidView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Maintain a Healthy Heart"
        return label
    }()
    
    @objc func setup(){
        if let window = UIApplication.shared.keyWindow {
            bannerView.backgroundColor = UIColor.Theme(alpha: 1.0)
            
            window.addSubview(mainView)
            window.addSubview(bannerView)
            window.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(bannerView)
            mainView.addSubview(backView)
            firsAidView.addSubview(title)
            mainView.addSubview(firsAidView)
            
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Exit)))
            title.frame = CGRect(x: 20, y: -50, width: window.frame.width, height:  30)
            firsAidView.frame = CGRect(x: 0, y: 80, width: window.frame.width, height:  window.frame.height-80)
            backImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            mainView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha:1.0)
            backView.frame = CGRect(x: 10, y: 40, width: 30, height:  30)
            bannerView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height:  250)
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height:  window.frame.height)
            //animation appear view
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: self.mainView.frame.height)
                
            }, completion:  nil)
        }
    }
    
    @objc func Exit() {
        //        safety.removeAll()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.mainView.frame = CGRect(x: 0, y: self.mainView.frame.height, width: self.mainView.frame.width, height: self.mainView.frame.height)
            
        }, completion:  nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return safety.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SafetyCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 0.5
        cell.layer.cornerRadius = 5
        
        cell.safety = safety[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let titleLabel = safety[indexPath.item].title!
        let descriptionLabel = safety[indexPath.item].content!
        let heightName = firsAidView.heightEstimation(text: titleLabel, width: firsAidView.frame.width-40, size: 16, defaultHeight : 20)
        let heightDescription = (firsAidView.heightEstimation(text: descriptionLabel, width: firsAidView.frame.width-40, size: 13, defaultHeight : 5)/2)
        return CGSize(width: firsAidView.frame.width-40, height: ((220+heightName)+heightDescription)+15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override init() {
        super.init()
        
        firsAidView.scrollsToTop = true
        firsAidView.contentInset = UIEdgeInsetsMake(50, 0, 10, 0)
        firsAidView.backgroundColor = UIColor.Theme(alpha: 1.0)
        firsAidView.register(SafetyCell.self, forCellWithReuseIdentifier: cellId)
        
    }
    
}
