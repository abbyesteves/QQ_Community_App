//
//  HealthTabGoodHealth.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class HealthGoodHealth: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    //dummy data for news feed
    var safety: [Safety] = {
        let data1 = Safety()
        data1.thumbnailImage = "http://pad3.whstatic.com/images/thumb/e/ec/Have-a-Good-General-Healthy-Body-Step-1-Version-4.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-1-Version-4.jpg"
        data1.title = "1. Monitor calories."
        data1.content = "To have a generally healthy body, you should try to maintain a healthy weight. If you're overweight, you are not maintaining a generally healthy body."
        
        let data2 = Safety()
        data2.thumbnailImage = "http://pad3.whstatic.com/images/thumb/f/f1/Have-a-Good-General-Healthy-Body-Step-2-Version-4.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-2-Version-4.jpg"
        data2.title = "2. Eat a well-balanced diet."
        data2.content = "Having a well-balanced diet is one of the most important parts of maintaining a generally healthy body. Without a balanced diet, it'll be very difficult to maintain proper health."
        
        let data3 = Safety()
        data3.thumbnailImage = "http://pad2.whstatic.com/images/thumb/3/32/Have-a-Good-General-Healthy-Body-Step-3-Version-4.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-3-Version-4.jpg"
        data3.title = "3. Go for lean protein sources."
        data3.content = "Protein is an essential nutrient in your diet. It will provide the building blocks for many functions of your body including maintaining lean muscle mass, rebuilding cells and supporting your immune system."
        
        let data4 = Safety()
        data4.thumbnailImage = "http://pad2.whstatic.com/images/thumb/e/e9/Have-a-Good-General-Healthy-Body-Step-4-Version-4.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-4-Version-4.jpg"
        data4.title = "4. Make half of your meals a fruit or vegetable."
        data4.content = "Fruits and vegetables are two of the most important food groups. These foods contain the highest amount of vitamins, minerals and antioxidants."
        
        let data5 = Safety()
        data5.thumbnailImage = "http://pad3.whstatic.com/images/thumb/6/6f/Have-a-Good-General-Healthy-Body-Step-5-Version-4.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-5-Version-4.jpg"
        data5.title = "5. Make your grain choices whole grain."
        data5.content = "Whole grain foods have been associated with a variety of beneficial health effects to help promote a general healthy body. Try making more of your grain choices whole."
        
        let data6 = Safety()
        data6.thumbnailImage = "http://pad3.whstatic.com/images/thumb/d/d5/Have-a-Good-General-Healthy-Body-Step-6-Version-2.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-6-Version-2.jpg"
        data6.title = "6. Limit junk foods and processed foods."
        data6.content = "Although there is a wide range of processed foods (including both nutritious and unhealthy options), many are higher in calories, fat, sugar and preservatives."
        
        let data7 = Safety()
        data7.thumbnailImage = "http://pad1.whstatic.com/images/thumb/8/8c/Have-a-Good-General-Healthy-Body-Step-7-Version-2.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-7-Version-2.jpg"
        data7.title = "7. Drink adequate amounts of water."
        data7.content = "Water is a vital nutrient in your diet and plays many important roles in your body. If you do not consume enough hydrating fluids each day, you run the risk of being dehydrated. \n Most adults need at least eight 8-oz glasses of hydrating fluids daily. However some experts recommend consuming up to 10–13 glasses daily."
        
        let data8 = Safety()
        data8.thumbnailImage = "http://pad3.whstatic.com/images/thumb/5/5d/Have-a-Good-General-Healthy-Body-Step-8-Version-2.jpg/aid817331-v4-728px-Have-a-Good-General-Healthy-Body-Step-8-Version-2.jpg"
        data8.title = "8. Take a vitamin and mineral supplements."
        data8.content = "Some health and nutrition experts may recommend taking a daily multivitamin. These \"all-in-one\" supplements can serve as a back up to the days you do not eat a healthy diet or are unable to meet all of your nutrient needs through foods."
        
        
        return [data1, data2, data3, data4, data5, data6, data7, data8]
    }()
    //view declarations
    var cellId = "cellId"
    
    let bannerView = UIView()
    let backView = UIView()
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let mainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    lazy var firsAidView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(20)
        label.text = "Have a Good General Healthy Body"
        return label
    }()
    
    @objc func setup(){
        if let window = UIApplication.shared.keyWindow {
            bannerView.backgroundColor = UIColor.Theme(alpha: 1.0)
            
            window.addSubview(mainView)
            window.addSubview(bannerView)
            window.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(bannerView)
            mainView.addSubview(backView)
            firsAidView.addSubview(title)
            mainView.addSubview(firsAidView)
            
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Exit)))
            title.frame = CGRect(x: 20, y: -50, width: window.frame.width-20, height:  30)
            firsAidView.frame = CGRect(x: 0, y: 80, width: window.frame.width, height:  window.frame.height-80)
            backImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            mainView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha:1.0)
            backView.frame = CGRect(x: 10, y: 40, width: 30, height:  30)
            bannerView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height:  250)
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height:  window.frame.height)
            //animation appear view
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: self.mainView.frame.height)
                
            }, completion:  nil)
        }
    }
    
    @objc func Exit() {
        //        safety.removeAll()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.mainView.frame = CGRect(x: 0, y: self.mainView.frame.height, width: self.mainView.frame.width, height: self.mainView.frame.height)
            
        }, completion:  nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return safety.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SafetyCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 0.5
        cell.layer.cornerRadius = 5
        
        cell.safety = safety[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let height = (view.frame.width) / CGFloat(news.count)*118
        let titleLabel = safety[indexPath.item].title!
        let descriptionLabel = safety[indexPath.item].content!
        let heightName = firsAidView.heightEstimation(text: titleLabel, width: firsAidView.frame.width-40, size: 16, defaultHeight : 20)
        let heightDescription = (firsAidView.heightEstimation(text: descriptionLabel, width: firsAidView.frame.width-40, size: 13, defaultHeight : 5)/2)
        return CGSize(width: firsAidView.frame.width-40, height: ((220+heightName)+heightDescription)+15)
//        return CGSize(width: firsAidView.frame.width-40, height: firsAidView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override init() {
        super.init()
        
        firsAidView.scrollsToTop = true
        firsAidView.contentInset = UIEdgeInsetsMake(60, 0, 10, 0)
        firsAidView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha: 0)
        firsAidView.register(SafetyCell.self, forCellWithReuseIdentifier: cellId)
        
    }
    
}
