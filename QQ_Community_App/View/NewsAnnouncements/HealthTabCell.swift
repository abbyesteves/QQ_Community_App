//
//  HealthCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 10/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class HealthTabCell: NewsTabCell{
    
    //Images
    let hospitalImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "hospital")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let aidImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "doctors_bag")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let heartImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "heart_health")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let healthImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "heart_monitor")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    //Labels
    let hospitalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = label.font.withSize(14)
        label.text = "Hospitals"
        label.textAlignment = .center;
        return label
    }()
    
    let aidLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = label.font.withSize(14)
        label.textAlignment = .center;
        label.text = "First Aid"
        return label
    }()
    
    let heartLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = label.font.withSize(14)
        label.text = "Heart Care"
        label.textAlignment = .center;
        return label
    }()
    
    let healthLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = label.font.withSize(14)
        label.text = "Good Health"
        label.textAlignment = .center;
        return label
    }()
    
    
    //Views
    let hospital: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    let aid: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        return view
    }()
    
    let heart: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        return view
    }()
    
    let health: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        return view
    }()
    
    lazy var hospitalAid: HealthHospital = {
        let launcher = HealthHospital()
        return launcher
    }()
    
    @objc func Hospitals() {
        hospitalAid.setup()
    }
    
    lazy var firstAid: HealthFirstAid = {
        let launcher = HealthFirstAid()
        return launcher
    }()
    
    @objc func FirstAid() {
       firstAid.setup()
    }
    
    lazy var heartCare: HealthHeartCare = {
        let launcher = HealthHeartCare()
        return launcher
    }()
    
    @objc func HeartCare() {
        heartCare.setup()
    }
    
    lazy var goodHealth: HealthGoodHealth = {
        let launcher = HealthGoodHealth()
        return launcher
    }()
    
    @objc func GoodHealth() {
        goodHealth.setup()
    }
    
    private func setUpConstraints(){
        addConstraintsFormat(format: "H:|-20-[v0]|", views: title)
        addConstraintsFormat(format: "V:|-(-30)-[v0(120)]|", views: title)
        
        //computation for the width
        let width = (frame.width-320)/2
        let width2 = width+160
        
        // Hospital
        addConstraintsFormat(format: "H:|-\(width)-[v0(160)]|", views: hospital)
        addConstraintsFormat(format: "V:|-70-[v0(160)]|", views: hospital)
        addConstraintsFormat(format: "H:|[v0(160)]|", views: hospitalLabel)
        addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: hospitalLabel)
        
        addConstraintsFormat(format: "H:|-45-[v0(70)]|", views: hospitalImage)
        addConstraintsFormat(format: "V:|-40-[v0(70)]|", views: hospitalImage)
        
        // Fist Aid
        addConstraintsFormat(format: "H:|-\(width2)-[v0(160)]|", views: aid)
        addConstraintsFormat(format: "V:|-70-[v0(160)]|", views: aid)
        addConstraintsFormat(format: "H:|[v0(160)]|", views: aidLabel)
        addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: aidLabel)
        
        addConstraintsFormat(format: "H:|-45-[v0(70)]|", views: aidImage)
        addConstraintsFormat(format: "V:|-40-[v0(70)]|", views: aidImage)
        
        // heart
        addConstraintsFormat(format: "H:|-\(width)-[v0(160)]|", views: heart)
        addConstraintsFormat(format: "V:|-230-[v0(160)]|", views: heart)
        
        addConstraintsFormat(format: "H:|[v0(160)]|", views: heartLabel)
        addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: heartLabel)
        
        addConstraintsFormat(format: "H:|-45-[v0(70)]|", views: heartImage)
        addConstraintsFormat(format: "V:|-40-[v0(70)]|", views: heartImage)
        
        // health
        addConstraintsFormat(format: "H:|-\(width2)-[v0(160)]|", views: health)
        addConstraintsFormat(format: "V:|-230-[v0(160)]|", views: health)
        
        addConstraintsFormat(format: "H:|[v0(160)]|", views: healthLabel)
        addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: healthLabel)
        
        addConstraintsFormat(format: "H:|-45-[v0(70)]|", views: healthImage)
        addConstraintsFormat(format: "V:|-40-[v0(70)]|", views: healthImage)
    }

    private func setUpAddView(){
        addSubview(title)
        addSubview(hospital)
        addSubview(aid)
        addSubview(heart)
        addSubview(health)
        //hospital
        hospital.addSubview(hospitalImage)
        hospital.addSubview(hospitalLabel)
        //first aid
        aid.addSubview(aidLabel)
        aid.addSubview(aidImage)
        //heart
        heart.addSubview(heartImage)
        heart.addSubview(heartLabel)
        //health
        health.addSubview(healthImage)
        health.addSubview(healthLabel)
    }

    private func setUpGestures() {
        hospital.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Hospitals)))
        aid.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FirstAid)))
        heart.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HeartCare)))
        health.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GoodHealth)))
    }

    override func setupViews() {
        super.setupViews()
        self.title.text = "Health Care"
        collectionView.removeFromSuperview()
        
        setUpGestures()
        setUpAddView()
        setUpConstraints()
        
    }
}
