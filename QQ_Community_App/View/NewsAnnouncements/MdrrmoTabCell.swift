//
//  MdrrmoCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 10/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import AVFoundation

class MdrrmoTabCell: NewsTabCell {
    
    var player : AVAudioPlayer = AVAudioPlayer()
    
    let audioPath = Bundle.main.path(forResource: "siren", ofType: "mp3")
    
    //Views
    let flashlight: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        view.layer.cornerRadius = 2
        return view
    }()
    
    let alarm: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        view.layer.cornerRadius = 2
        return view
    }()
    
    //FLASH LIGHT
    let lightOn: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let lightOnImage: UIImageView = {
        let image = UIImage(named: "ic_flashlight_on")?.withRenderingMode(.alwaysTemplate)as UIImage?
        let imageView = UIImageView(image: image)
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.tintColor = UIColor.ThemeLight(alpha: 1.0)
        return imageView
    }()
    
    let flashlightOnLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeLight(alpha: 1.0)
        label.text = "ON"
        label.textAlignment = .center;
        return label
    }()
    
    let lightOff: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let lightOffImage: UIImageView = {
        let image = UIImage(named: "ic_flashlight_off")?.withRenderingMode(.alwaysTemplate)as UIImage?
        let imageView = UIImageView(image: image)
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let flashlightOffLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "OFF"
        label.textAlignment = .center;
        return label
    }()
    
    //
    
    //RESCUE ALARM
    let rescueOn: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let rescueOnImage: UIImageView = {
        let image = UIImage(named: "ic_volume_up")?.withRenderingMode(.alwaysTemplate)as UIImage?
        let imageView = UIImageView(image: image)
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.tintColor = UIColor.ThemeLight(alpha: 1.0)
        return imageView
    }()
    
    let alarmOnLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeLight(alpha: 1.0)
        label.text = "ON"
        label.textAlignment = .center;
        return label
    }()
    
    let rescueOff: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let rescueOffImage: UIImageView = {
        let image = UIImage(named: "ic_volume_off")?.withRenderingMode(.alwaysTemplate)as UIImage?
        let imageView = UIImageView(image: image)
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let alarmOffLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "OFF"
        label.textAlignment = .center;
        return label
    }()
    
    //
    
    let flashlightLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.text = "Flashlight"
        label.textAlignment = .center;
        return label
    }()
    
    let rescueLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.text = "Rescue Alarm"
        label.textAlignment = .center;
        return label
    }()
    
    //tapped gesture
    @objc func flashlightOn(){
        lightOn.backgroundColor = UIColor.Theme(alpha: 1.0)
        lightOnImage.tintColor = UIColor.white
        flashlightOnLabel.textColor = UIColor.white
        
        lightOff.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        lightOffImage.tintColor = UIColor.ThemeLight(alpha: 1.0)
        flashlightOffLabel.textColor = UIColor.ThemeLight(alpha: 1.0)
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video), device.hasTorch {
            do {
                try device.lockForConfiguration()
                try device.setTorchModeOn(level: 1.0)
                device.torchMode = .on
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    @objc func flashlightOff(){
        lightOff.backgroundColor = UIColor.Theme(alpha: 1.0)
        lightOffImage.tintColor = UIColor.white
        flashlightOffLabel.textColor = UIColor.white
        
        lightOn.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        lightOnImage.tintColor = UIColor.ThemeLight(alpha: 1.0)
        flashlightOnLabel.textColor = UIColor.ThemeLight(alpha: 1.0)
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video), device.hasTorch {
            do {
                try device.lockForConfiguration()
                try device.setTorchModeOn(level: 1.0)
                device.torchMode = .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    @objc func rescueAlarmOn(){
        rescueOn.backgroundColor = UIColor.Theme(alpha: 1.0)
        rescueOnImage.tintColor = UIColor.white
        alarmOnLabel.textColor = UIColor.white
        
        rescueOff.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        rescueOffImage.tintColor = UIColor.ThemeLight(alpha: 1.0)
        alarmOffLabel.textColor = UIColor.ThemeLight(alpha: 1.0)
        
       
        player.play()
        player.numberOfLoops = -1
    }
    
    @objc func rescueAlarmOff(){
        rescueOff.backgroundColor = UIColor.Theme(alpha: 1.0)
        rescueOffImage.tintColor = UIColor.white
        alarmOffLabel.textColor = UIColor.white
        
        rescueOn.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 231, alpha: 1)
        rescueOnImage.tintColor = UIColor.ThemeLight(alpha: 1.0)
        alarmOnLabel.textColor = UIColor.ThemeLight(alpha: 1.0)
        
        player.pause()
    }
    
    func setUpAlarm(){
        do {
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPath!) as URL)
        } catch  {
            print("error")
        }
    }
    
    private func setUpConstraints(){
        let width = frame.width-30
        
        addConstraintsFormat(format: "H:|-20-[v0]|", views: title)
        addConstraintsFormat(format: "V:|-(-30)-[v0(120)]|", views: title)
        
        
        addConstraintsFormat(format: "H:|-\((frame.width/2)-(width/2))-[v0(\(width))]", views: flashlight)
        addConstraintsFormat(format: "V:|-70-[v0(160)]|", views: flashlight)
        
        
        addConstraintsFormat(format: "H:|-\((frame.width/2)-(width/2))-[v0(\(width))]", views: alarm)
        addConstraintsFormat(format: "V:|-240-[v0(160)]|", views: alarm)
        
        //flashlight
        addConstraintsFormat(format: "H:|-\((width/2)-155)-[v0(150)]-10-[v1(150)]", views: lightOn, lightOff)
        addConstraintsFormat(format: "V:|-40-[v0(110)]|", views: lightOn)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: lightOnImage)
        addConstraintsFormat(format: "V:|[v0]-20-|", views: lightOnImage)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: flashlightOnLabel)
        addConstraintsFormat(format: "V:|-80-[v0(20)]|", views: flashlightOnLabel)
        
        addConstraintsFormat(format: "V:|-40-[v0(110)]|", views: lightOff)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: lightOffImage)
        addConstraintsFormat(format: "V:|[v0]-20-|", views: lightOffImage)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: flashlightOffLabel)
        addConstraintsFormat(format: "V:|-80-[v0(20)]|", views: flashlightOffLabel)
        //
        
        //rescue alarm
        addConstraintsFormat(format: "H:|-\((width/2)-155)-[v0(150)]-10-[v1(150)]", views: rescueOn, rescueOff)
        addConstraintsFormat(format: "V:|-40-[v0(110)]|", views: rescueOn)
        
        
        addConstraintsFormat(format: "H:|-35-[v0]-35-|", views: rescueOnImage)
        addConstraintsFormat(format: "V:|-15-[v0]-30-|", views: rescueOnImage)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: alarmOnLabel)
        addConstraintsFormat(format: "V:|-80-[v0(20)]|", views: alarmOnLabel)
        
        addConstraintsFormat(format: "V:|-40-[v0(110)]|", views: rescueOff)
        
        addConstraintsFormat(format: "H:|-35-[v0]-35-|", views: rescueOffImage)
        addConstraintsFormat(format: "V:|-15-[v0]-30-|", views: rescueOffImage)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: alarmOffLabel)
        addConstraintsFormat(format: "V:|-80-[v0(20)]|", views: alarmOffLabel)
        
        //Label
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: flashlightLabel)
        addConstraintsFormat(format: "V:|-80-[v0(20)]|", views: flashlightLabel)
        
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: rescueLabel)
        addConstraintsFormat(format: "V:|-250-[v0(20)]|", views: rescueLabel)
    }
    
    private func setUpAddView(){
        addSubview(title)
        addSubview(flashlight)
        addSubview(alarm)
        //flashlight
        flashlight.addSubview(lightOn)
        flashlight.addSubview(lightOff)
        lightOn.addSubview(lightOnImage)
        lightOn.addSubview(flashlightOnLabel)
        lightOff.addSubview(lightOffImage)
        lightOff.addSubview(flashlightOffLabel)
        //rescue alarm
        alarm.addSubview(rescueOn)
        alarm.addSubview(rescueOff)
        rescueOn.addSubview(rescueOnImage)
        rescueOn.addSubview(alarmOnLabel)
        rescueOff.addSubview(rescueOffImage)
        rescueOff.addSubview(alarmOffLabel)
        //label
        addSubview(flashlightLabel)
        addSubview(rescueLabel)
    }
    
    private func setUpGestures() {
        lightOn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(flashlightOn)))
        lightOff.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(flashlightOff)))
    
        rescueOn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rescueAlarmOn)))
        rescueOff.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rescueAlarmOff)))
    }

    override func setupViews() {
        super.setupViews()
        loadingView.removeFromSuperview()
        self.title.text = "Emergency Tools"
        collectionView.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        
        setUpAlarm()
        setUpGestures()
        setUpAddView()
        setUpConstraints()
    }
}










