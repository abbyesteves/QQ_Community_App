//
//  PnpCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 10/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class PnpTabCell: BfpTabCell {
    
    var pnpArray: [Safety] = {
        
        var data1 = Safety()
        data1.thumbnailImage = "https://i.pinimg.com/736x/35/4b/87/354b87883682db57dd579d7223a808bf--flat-illustration-illustrations.jpg"
        data1.title = "Tip no. 1: Be wary of isolated spots"
        data1.content = "(e.g. apartment laundry rooms, underground garages, parking lots, dark streets). Walk with a friend or coworker, especially at night."
        
        var data2 = Safety()
        data2.thumbnailImage = "http://www.tenable.com/brand/img/imagery/tenable-imagery-good-3.png"
        data2.title = "Tip no. 2: Be aware of your surroundings when using the ATM machine"
        data2.content = "Look around before conducting a transaction. If you see anyone or anything suspicious, cancel your transaction and go to another ATM. If you must use an ATM after hours, make sure it's well lit."
        
        var data3 = Safety()
        data3.thumbnailImage = "https://images.vexels.com/media/users/3/78631/raw/7acad042008eaa4cf8426daed9950097-flat-house-illustration.jpg"
        data3.title = "Tip no. 3: Never open your door to strangers"
        data3.content = "Offer to make an emergency call while someone waits outside. Check the identification of sales or service people before letting them in. Don't be embarrassed to phone for verification."
        
        var data4 = Safety()
        data4.thumbnailImage = "http://i2.wp.com/static.freepsdfiles.net/uploads/2015/02/small11.jpg"
        data4.title = "Tip no. 4: Walk facing traffic whenever possible"
        data4.content = "This increases awareness of potential traffic hazards and also reduces the possibility of being followed by someone in a vehicle. Avoid walking by the curb or near buildings or shrubbery. Walk in the middle of the sidewalk with confidence."
        
        var data5 = Safety()
        data5.thumbnailImage = "http://media.gettyimages.com/vectors/flat-identification-card-icon-vector-id518097024"
        data5.title = "Tip no. 5: Bring an I.D"
        data5.content = "Always carry a form of personal identification with you."
        
        return [data1, data2, data3, data4, data5]
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pnpArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SafetyCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 0.5
        cell.layer.cornerRadius = 5

        cell.safety = pnpArray[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let titleLabel = pnpArray[indexPath.item].title!
        let descriptionLabel = pnpArray[indexPath.item].content!
        let heightName = heightEstimation(text: titleLabel, width: frame.width-40, size: 16, defaultHeight : 20)
        let heightDescription = (heightEstimation(text: descriptionLabel, width: frame.width-40, size: 13, defaultHeight : 5)/2)
        let height = 220+heightName+heightDescription+15
        return CGSize(width: frame.width-40, height: height)
    }
    
    override func setupViews() {
        super.setupViews()
        self.title.text = "Safety Tips"
    }
}
