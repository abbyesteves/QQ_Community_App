//
//  HealthHospitals.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 04/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class HealthHospital: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    //dummy data for news feed
    var hospitals: [Hospital] = {
        let data1 = Hospital()
        data1.name = "St. Paul Hospital Bulacan, Inc"
        data1.address = "187 JP Rizal St., Biñang 2nd., Bocaue Town Proper Biňang 1st Raod, Bocaue, Bulacan"
        data1.contact = "(044) 692 5964"
        
        let data2 = Hospital()
        data2.name = "Dr Yanga's Hospital"
        data2.address = "252 Bocaue Town Proper Rd, Bocaue, Bulacan"
        data2.contact = "(044) 692 5307"
        
        let data3 = Hospital()
        data3.name = "Insular Health Care, Inc. - Mt. Carmel Hospital"
        data3.address = "Mc Arthur Highway, Bunlo, Bocaue, Bulacan"
        data3.contact = "(044) 692 1619"
        
        let data4 = Hospital()
        data4.name = "Bocaue Specialists Medical Center"
        data4.address = "Bocaue, Bulacan"
        data4.contact = "0905 462 0904"
        
        let data5 = Hospital()
        data5.name = "BMMG Cooperative Hospital"
        data5.address = "Gov Fortunato Halili Ave, Bocaue, Bulacan"
        data5.contact = "(044) 278-6560"
        
        let data6 = Hospital()
        data6.name = "Mt. Carmel Hospital"
        data6.address = "Mt. Carmel Bocaue Bulacan"
        data6.contact = "(044) 692 1619"
        
        let data7 = Hospital()
        data7.name = "San Lorenzo Ruiz Home Care Hospital"
        data7.address = "Bunducan, Bocaue, Bulacan"
        data7.contact = "(044) 692 2907"
        
        return [data1, data2, data3, data4, data5, data6]
    }()
    //view declarations
    var cellId = "cellId"
    
    let bannerView = UIView()
    let backView = UIView()
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let mainView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.Theme(alpha: 1.0)
        return cv
    }()
    
    lazy var hospitalView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Hospitals"
        return label
    }()
    
    @objc func setup(){
        if let window = UIApplication.shared.keyWindow {
            bannerView.backgroundColor = UIColor.Theme(alpha: 1.0)
            
            window.addSubview(mainView)
            window.addSubview(bannerView)
            window.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(bannerView)
            mainView.addSubview(backView)
            hospitalView.addSubview(title)
            mainView.addSubview(hospitalView)
            
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Exit)))
            title.frame = CGRect(x: 20, y: -50, width: window.frame.width, height:  30)
            hospitalView.frame = CGRect(x: 0, y: 80, width: window.frame.width, height:  window.frame.height-80)
            backImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            mainView.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha:1.0)
            backView.frame = CGRect(x: 10, y: 40, width: 30, height:  30)
            bannerView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height:  250)
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height:  window.frame.height)
            //animation appear view
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: self.mainView.frame.height)
                
            }, completion:  nil)
        }
    }
    
    @objc func Exit() {
        //        safety.removeAll()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.mainView.frame = CGRect(x: 0, y: self.mainView.frame.height, width: self.mainView.frame.width, height: self.mainView.frame.height)
            
        }, completion:  nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hospitals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HospitalCell
        cell.backgroundColor = UIColor.white        
        cell.hospital = hospitals[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let name = hospitals[indexPath.item].name
        let address  = hospitals[indexPath.item].address
        let nameHeight = hospitalView.heightEstimation(text: name!, width: hospitalView.frame.width, size: 16, defaultHeight: 20)
        let addressHeight = hospitalView.heightEstimation(text: address!, width: hospitalView.frame.width, size: 13, defaultHeight: 20)
        let height = nameHeight+addressHeight+30
        if height < 120 {
            return CGSize(width: hospitalView.frame.width, height: 120)
        }
        return CGSize(width: hospitalView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override init() {
        super.init()
        
        hospitalView.scrollsToTop = true
        hospitalView.contentInset = UIEdgeInsetsMake(50, 0, 10, 0)
        hospitalView.backgroundColor = UIColor.Theme(alpha: 1.0)
        hospitalView.register(HospitalCell.self, forCellWithReuseIdentifier: cellId)
        
    }
}
