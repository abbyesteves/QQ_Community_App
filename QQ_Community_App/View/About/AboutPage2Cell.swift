//
//  Page2Cell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class AboutPage2Cell: BaseCell {
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.textAlignment = .center
        label.text = "RiverTown Wonders"
        return label
    }()
    
    let baliwagLogo: UIImageView = {
        let image = UIImage(named: "logo")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "Among its tourist attractions are a town museum located near the municipality's center and the town's river festival celebrated on the first Sunday of every July. The river festival is in commemoration of the Holy Cross of Wawa, believed to be miraculous by the town's predominating Roman Catholic populace."
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.Theme(alpha: 1.0)
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    private func setupView(){
        addSubview(title)
        addSubview(baliwagLogo)
        addSubview(descriptionLabel)
    }
    
    private func setupConstraints(){
        let width = (frame.width-320)/2
        let height = (frame.height-100)/2
        
        addConstraintsFormat(format: "H:|[v0]|", views: title)
        addConstraintsFormat(format: "V:|-10-[v0(50)]", views: title)
        
        addConstraintsFormat(format: "H:|-\(width)-[v0(320)]|", views: baliwagLogo)
        addConstraintsFormat(format: "V:|-\(height)-[v0(100)]-20-[v1]-50-|", views: baliwagLogo, descriptionLabel)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: descriptionLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = UIColor.Theme(alpha: 1.0)
        
        setupView()
        setupConstraints()

    }
}
