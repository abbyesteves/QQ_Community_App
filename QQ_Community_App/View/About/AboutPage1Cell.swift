//
//  AboutCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class AboutPage1Cell: BaseCell {
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.textAlignment = .center
        label.text = "Bocaue"
        return label
    }()
    
    let baliwagImage: UIImageView = {
        let image = UIImage(named: "map")?.withRenderingMode(.alwaysTemplate)as UIImage?
        let imageView = UIImageView(image: image)
        imageView.tintColor = UIColor.ThemeDark(alpha: 1.0)
        return imageView
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "The Bocaue River river holds as much charm as the lights that form in the sky from the fireworks. It is the history written in its waters, the heroism of its children, the merriness of its people and the richness of its flora and fauna that make Bocaue a RiverTown Wonder."
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.Theme(alpha: 1.0)
        textView.textAlignment = .center
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    

    private func setupView() {
        backgroundColor = UIColor.Theme(alpha: 1.0)
        
        addSubview(title)
        addSubview(baliwagImage)
        addSubview(descriptionLabel)
    }
    
    private func setupConstraints(){
        // NEW CONSTRAINTS
//        let yellowView = UIView()
//        yellowView.backgroundColor = UIColor.clear
//
////        let blueView = UIView()
////        blueView.backgroundColor = UIColor.blue
//
////        let redView = UIView()
////        redView.backgroundColor = UIColor.red
//
//        let bottomControlStackView = UIStackView(arrangedSubviews: [yellowView, pageControl, nextView])
//        bottomControlStackView.translatesAutoresizingMaskIntoConstraints = false
//        bottomControlStackView.distribution = .fillEqually
////        bottomControlStackView.axis = .vertical // .translatesAutoresizingMaskIntoConstraints = false
//
//        addSubview(bottomControlStackView)
//
//        NSLayoutConstraint.activate([
//            bottomControlStackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
//            bottomControlStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
//            bottomControlStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
//            bottomControlStackView.heightAnchor.constraint(equalToConstant: 50)
//            ])
        
        //
        
        
        let width = (frame.width-190)/2
        let height = (frame.height-280)/2
        
        addConstraintsFormat(format: "H:|[v0]|", views: title)
        addConstraintsFormat(format: "V:|-10-[v0(50)]", views: title)
        
        addConstraintsFormat(format: "V:|-\(height)-[v0(280)]-20-[v1]-50-|", views: baliwagImage, descriptionLabel)
        addConstraintsFormat(format: "H:|-\(width)-[v0(190)]|", views: baliwagImage)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: descriptionLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}









