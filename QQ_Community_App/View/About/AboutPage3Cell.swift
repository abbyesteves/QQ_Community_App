//
//  Page3Cell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class AboutPage3Cell: BaseCell {
    
    let councilors = ["Norielito E. Guzman", "Alvin Paul Cotaco", "Donnabel Mendoza-Celestino", "Yboyh del Rosario, Sr.", "Agapito Salonga", "Josef Andrew Mendoza", "Dennis Carpio", "Emmanual Cruz"]
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.rgba(red: 202, green: 223, blue: 232, alpha: 1.0)
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "MUNICIPAL OFFICIALS"
        return label
    }()
    
    let mayorView = UIView()
    let vmayorView = UIView()
    let councilView = UIView()
    let bottomView: UIImageView = {
        let image = UIImage(named: "officials_bg")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "Mayor \n \n \n Mayor Eleanor J. Villanueva-Tugan was sworn into office as the 27th elected local cheif executive of Baliwag, alongside other new and re-elected officials of the province of Bulacan."
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.Theme(alpha: 1.0)
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = textView.font?.withSize(14)
        return textView
    }()
    
    let viceMayorLabel: UITextView = {
        let textView = UITextView()
        textView.text = "Vice Mayor"
        textView.textColor = UIColor.white
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let viceNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(16)
        label.text = "Adrin B. Sta. Ana"
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.text = "Eleanor J. Villanueva-Tugan"
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let mayorImage: UIImageView = {
        let image = UIImage(named: "mayor")
        let imageView = UIImageView(image: image)
        imageView.layer.borderWidth = 5
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 60
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.borderColor = UIColor.ThemeDarkSecond(alpha: 1.0).cgColor
        return imageView
    }()
    
    let viceMayorImage: UIImageView = {
        let image = UIImage(named: "vmayor")
        let imageView = UIImageView(image: image)
        imageView.layer.borderWidth = 5
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 60
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.borderColor = UIColor.Theme(alpha: 1.0).cgColor
        return imageView
    }()
    
    let councilLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        label.font = label.font.withSize(16)
        label.text = "COUNCILORS"
        label.textAlignment = .center
        return label
    }()
    
    var height = 10
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.white
        mayorView.backgroundColor = UIColor.Theme(alpha: 1.0)
        vmayorView.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        
        addSubview(scrollView)
        scrollView.addSubview(title)
        scrollView.addSubview(mayorView)
        scrollView.addSubview(vmayorView)
        scrollView.addSubview(bottomView)
        scrollView.addSubview(mayorImage)
        scrollView.addSubview(viceMayorImage)
        mayorView.addSubview(descriptionLabel)
        mayorView.addSubview(nameLabel)
        vmayorView.addSubview(viceMayorLabel)
        vmayorView.addSubview(viceNameLabel)
        bottomView.addSubview(councilLabel)
        bottomView.addSubview(councilView)
        
        let width = (frame.width-120)/2
        

        scrollView.contentSize = CGSize(width: frame.width, height: 800)
        
        addConstraintsFormat(format: "H:|[v0]|", views: scrollView)
        addConstraintsFormat(format: "V:|[v0]|", views: scrollView)
        
        title.frame = CGRect(x: 0, y: 0, width: frame.width, height: 30)
        mayorView.frame = CGRect(x: 0, y: 100, width: frame.width, height: 230)
        vmayorView.frame = CGRect(x: 60, y: 300, width: frame.width, height: 80)
        mayorImage.frame = CGRect(x: width, y: 40, width: 120, height: 120)
        viceMayorImage.frame = CGRect(x: 0, y: 270, width: 120, height: 120)
        bottomView.frame = CGRect(x: 0, y: 380, width: frame.width, height: 650)
        
        addConstraintsFormat(format: "H:|[v0]|", views: councilLabel)
        addConstraintsFormat(format: "H:|[v0]|", views: councilView)
        addConstraintsFormat(format: "V:|-30-[v0(20)]|", views: councilLabel)
        addConstraintsFormat(format: "V:|-60-[v0]|", views: councilView)

        addConstraintsFormat(format: "H:|-65-[v0]|", views: viceMayorLabel)
        addConstraintsFormat(format: "V:|[v0(30)]|", views: viceMayorLabel)

        addConstraintsFormat(format: "H:|-70-[v0]|", views: viceNameLabel)
        addConstraintsFormat(format: "V:|-30-[v0(30)]|", views: viceNameLabel)

        addConstraintsFormat(format: "H:|[v0]|", views: nameLabel)
        addConstraintsFormat(format: "V:|-85-[v0(20)]|", views: nameLabel)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: descriptionLabel)
        addConstraintsFormat(format: "V:|-50-[v0]|", views: descriptionLabel)
        
        for council in councilors {
            
            let councilNameLabel: UILabel = {
                let label = UILabel()
                label.textColor = UIColor.darkGray
                label.font = label.font.withSize(14)
                label.textAlignment = .center
                label.attributedText = imbedIcon(string : "Coun. \(council)", image : "ic_person_colored")
                return label
            }()
            
            councilView.addSubview(councilNameLabel)
            addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: councilNameLabel)
            addConstraintsFormat(format: "V:|-\(height)-[v0(20)]|", views: councilNameLabel)
            height = height+30
        }
    }
}
