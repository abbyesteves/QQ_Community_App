//
//  NewsCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class NewsCell: BaseCell {
    var dataIndex = 0
    static var heightName = CGFloat()
    var article: Articles? { //News?
        didSet {
            
            let date = (article?.publishedAt)!
            let newDateFormat = dateFormat(date: date)
            //remove elements if header
//            if (newDateFormat == dateToday()){
//                removeForHeader()
//            }

            titleLabel.text = article?.title
            dateLabel.text = newDateFormat
            descriptionText.text = article?.description
            
            if let urlImage = article?.urlToImage {
                ApiService().get_image(url_str: urlImage, thumbnailView: self.thumbnailView)
            }
            
            
            //measure title height by text
            if let title = article?.title {
//                titleLabelHeight?.constant = heightEstimation(text: titleLabel.text!, width: frame.width-20, size: 16, defaultHeight : 20)
                let size = CGSize(width: frame.width-40, height: 1000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimated = NSString(string: title).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
                titleLabelHeight?.constant = round(estimated.size.height)
            }
        }
    }
    
    func dateToday() -> String {
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let todaysDate = dateFormatter.string(from: date)
        return todaysDate
    }
    
    func dateFormat(date: String) -> String {
        let formatter = ISO8601DateFormatter()
        let date = formatter.date(from: date)
        return date!.iso8601
    }
    
    func removeForHeader(){
        addSubview(headerView)
        addConstraintsFormat(format: "H:|[v0]|", views: headerView)
        addConstraintsFormat(format: "V:|[v0]|", views: headerView)
        
        addSubview(HeaderLabel)
        HeaderLabel.text = article?.title
        addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: HeaderLabel)
        addConstraintsFormat(format: "V:|-150-[v0(50)]|", views: HeaderLabel)
        
        titleLabel.removeFromSuperview()
        dateLabel.removeFromSuperview()
        iconView.removeFromSuperview()
        descriptionText.removeFromSuperview()
    }
    
    let headerView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.brown
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //fix image view aspect ratio
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 2
        return imageView
    }()
    
    let HeaderLabel: UILabel = {
        let label = UILabel()
        label.text = "Title of the news and announcement"
        label.numberOfLines = 2
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(17)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.lightGray
        imageView.image = UIImage(named: "no-image")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_today")
        return imageView
    }()
    
    let boarderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha:1.0)
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
//         label.backgroundColor = UIColor.lightGray
        label.text = "No Title"
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionText: UITextView = {
        let textView = UITextView()
        textView.text = "No Description"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let dateLabel: UITextView = {
        let textView = UITextView()
        textView.text = "No Date"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor(red: 2/255, green: 58/255, blue: 93/255, alpha: 1.0)
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    var titleLabelHeight: NSLayoutConstraint?
    
    private func setupView() {
        addSubview(boarderView)
        addSubview(thumbnailView)
        addSubview(titleLabel)
        addSubview(descriptionText)
        addSubview(iconView)
        addSubview(dateLabel)
    }
    
    private func setupConstraints(){
        //vertical constraint
        
        addConstraintsFormat(format: "V:|-5-[v0(100)]-5-[v1(0)]|", views: thumbnailView, boarderView)
        
        addConstraintsFormat(format: "H:|[v0]|", views: boarderView, iconView)
        addConstraintsFormat(format: "H:|-5-[v0(150)]", views: thumbnailView)
        
        //        addConstraintsFormat(format: "H:|-130-[v0(20)]|", views: iconView)
        //        addConstraintsFormat(format: "V:|-110-[v0(20)]", views: iconView)
        
        //top constraints for title label
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        //left constraint for title label
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: thumbnailView, attribute: .right, multiplier: 1, constant: 10))
        //right constraint fot title label
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
        //height constraint for title label
        titleLabelHeight = NSLayoutConstraint(item: titleLabel, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 50)
        addConstraint(titleLabelHeight!)
        
        
        //top constraints for description label
        addConstraint(NSLayoutConstraint(item: descriptionText, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 1))
        //left constraint for description label
        addConstraint(NSLayoutConstraint(item: descriptionText, attribute: .left, relatedBy: .equal, toItem: thumbnailView, attribute: .right, multiplier: 1, constant: 10))
        //right constraint fot description label
        addConstraint(NSLayoutConstraint(item: descriptionText, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
        //height constraint for description label
        addConstraint(NSLayoutConstraint(item: descriptionText, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 50))
        
        
        //top constraints for date label
        addConstraint(NSLayoutConstraint(item: dateLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
        //left constraint for date label
        addConstraint(NSLayoutConstraint(item: dateLabel, attribute: .left, relatedBy: .equal, toItem: iconView, attribute: .right, multiplier: 1, constant: 5))
        //right constraint fot date label
        addConstraint(NSLayoutConstraint(item: dateLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
        //height constraint for date label
        addConstraint(NSLayoutConstraint(item: dateLabel, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 20))
        
        //top constraints for date image
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -5))
        //left constraint for date image
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .left, relatedBy: .equal, toItem: thumbnailView, attribute: .right, multiplier: 1, constant: 5))
        //right constraint fot date image
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .right, relatedBy: .equal, toItem: thumbnailView, attribute: .right, multiplier: 1, constant: 20))
        //height constraint for date image
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0, constant: 16))
    }
    
    override func setupViews(){
        
        setupView()
        setupConstraints()
        
    }
}

