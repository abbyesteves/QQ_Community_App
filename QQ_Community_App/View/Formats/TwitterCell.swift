//
//  TwitterCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 26/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class TwitterCell: BaseCell {
    static var heightName = CGFloat()
    var tweet: Tweets? {
        didSet{
            
            tweetText.text = "\((tweet?.content!.htmlToString)!) \n \((tweet?.uploadedImage!)!)"
            nameLabel.text = tweet?.name
            userNameLabel.text = tweet?.userName
            if let urlImage = tweet?.thumbnailImage{
                ApiService().get_image(url_str : urlImage, thumbnailView : self.thumbnailView)
            }
            addConstraintsFormat(format: "H:|-5-[v0(50)]-10-[v1(\(widthEstimation(text: (tweet?.name)!, size: 13)))][v2(5)]-5-[v3][v4(20)]-5-|", views: thumbnailView, nameLabel,seperateView, userNameLabel, imageView)
        }
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.text = "Twitter Name"
        return label
    }()
    
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
//        label.backgroundColor = UIColor.blue
        label.font = label.font.withSize(13)
//        label.textAlignment = .center
        label.text = "@username"
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(13)
        label.text = "Month day, Year"
        label.textAlignment = .center
        return label
    }()
    
    let tweetText: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.black
        textView.font = textView.font?.withSize(13)
        textView.text = "Tweet message goes here"
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.dataDetectorTypes = .all
        return textView
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "no_image")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let seperateView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_dot")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.lightGray
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "logo_twitter")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let boarderView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 230, green: 236, blue: 240, alpha: 1.0)
        return view
    }()
    
    private func setupView(){
        addSubview(nameLabel)
        addSubview(thumbnailView)
        addSubview(userNameLabel)
        addSubview(tweetText)
        addSubview(boarderView)
        addSubview(imageView)
        addSubview(seperateView)
    }
    
    private func setupConstraints(){
        addConstraintsFormat(format: "V:|-5-[v0(50)]-5-|", views: thumbnailView)
        addConstraintsFormat(format: "V:|-5-[v0(20)]-5-|", views: userNameLabel)
        addConstraintsFormat(format: "H:|-60-[v0]-5-|", views: tweetText)
        addConstraintsFormat(format: "V:|-5-[v0(20)][v1][v2(1)]|", views: nameLabel, tweetText, boarderView)
        addConstraintsFormat(format: "H:|[v0]|", views: boarderView)
        addConstraintsFormat(format: "V:|-12.5-[v0(5)]|", views: seperateView)
        addConstraintsFormat(format: "V:|-5-[v0(20)]|", views: imageView)
    }
    
    override func setupViews(){
        setupView()
        setupConstraints()
    }
}
