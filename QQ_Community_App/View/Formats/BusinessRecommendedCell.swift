//
//  BusinessCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 27/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BusinessRecommendedCell: BaseCell {
    static var widthCategory = CGFloat()
    static var heightName = CGFloat()
    var businessFeed: BusinessFeed? {
        didSet {
            if let urlImage = businessFeed?.thumbnailImage {
                ApiService().get_image(url_str : urlImage, thumbnailView : self.thumbnailView)
            }
            if let rate = businessFeed?.rating {
                rating(rate: CGFloat(rate), horizontal: frame.width-120, vertical : 210)
            }
            NameLabel.text = businessFeed?.name
            ContactLabel.text = businessFeed?.contact
            CategoryLabel.text = businessFeed?.category
            
            BusinessRecommendedCell.widthCategory = widthEstimation(text : CategoryLabel.text, size : 13)
            addConstraintsFormat(format: "H:|-10-[v0(\(BusinessRecommendedCell.widthCategory))]-10-|", views: CategoryLabel)
            
            BusinessRecommendedCell.heightName = heightEstimation(text: NameLabel.text!, width: 250, size: 16, defaultHeight: 20)
            addConstraintsFormat(format: "V:|[v0(150)]-3-[v1(\(BusinessRecommendedCell.heightName))]-5-[v2(20)]-5-[v3(20)]-5-|", views: thumbnailView, NameLabel, ContactLabel, CategoryLabel)
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.image = UIImage(named: "no_image")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let NameLabel: UILabel = {
        let label = UILabel()
        label.text = "No Title"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let ContactLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "Not Available"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(13)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let CategoryLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "No Category"
        textView.textContainerInset = UIEdgeInsetsMake(3, 5, 5, 5)
        textView.font = textView.font?.withSize(13)
        textView.textAlignment = .center
        textView.textColor = UIColor.rgba(red: 2, green: 58, blue: 93, alpha: 1.0)
        textView.backgroundColor = UIColor.rgba(red: 202, green: 223, blue: 232, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    override func setupViews(){
        
        addSubview(thumbnailView)
        addSubview(NameLabel)
        addSubview(ContactLabel)
        addSubview(CategoryLabel)
        
        addConstraintsFormat(format: "H:|[v0]|", views: thumbnailView)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: NameLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: ContactLabel)
        
    }
}
