//
//  BusinessDetail.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 12/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BusinessDetail: BaseCell{
    
    var addressSelected = ""
    var bookMarkedDidTapped = 0
    let uiView = UIView()
    let backView = UIView()
    let bookmarkView = UIView()
    
    let floatImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 11.5
        return image
    }()
    
    let floatMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 27.5
        return view
    }()
    
    let contactUsButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.backgroundColor = UIColor.Theme(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("CONTACT US", for: .normal)
        return button
    }()
    
    lazy var mapDetail: MapDetail = {
        let launcher = MapDetail()
        return launcher
    }()
    
    lazy var contactLuncher: ContactLauncher = {
        let launcher = ContactLauncher()
        return launcher
    }()
    
    let locationView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 1, height: 2)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 1
        return view
    }()

    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name of restaurant"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.font = label.font.withSize(18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let categoryLabel: UILabel = {
        let label = UILabel()
        label.text = "FILIPINO"
        label.textAlignment = .center
        label.numberOfLines = 1
        label.textColor = UIColor.rgba(red: 2, green: 58, blue: 93, alpha: 1.0)
        label.backgroundColor = UIColor.rgba(red: 202, green: 223, blue: 232, alpha: 1.0)
        label.font = label.font.withSize(13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let contactLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "(0917) 302 9221"
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(16)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let mapLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "ADDRESS"
        textView.backgroundColor = .clear
        textView.font = UIFont.boldSystemFont(ofSize: 15)
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(12)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let addressLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.black
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(14)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let viewMapLabel: UITextView = {
        let textView = UITextView()
        textView.text = "VIEW ON MAP"
        textView.textColor = UIColor.gray
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(11)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let businessLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.alpha = 0.8
        textView.text = "BUSINESS DESCRIPTION"
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(14)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.black
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(14)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let hoursLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(16)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_clock")
        return imageView
    }()
    
    let mapPinView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_pin_drop")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let bookmarkImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "ic_bookmark")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let rateUsImage: UIImageView = {
        let imageView = UIImageView()
        imageView.safeAreaInsetsDidChange()
        imageView.image = UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    func openCell(name: String, about: String, address: String, hours: String, category: String, image: String, rating: CGFloat, contact: String) {
//        print(name)
//        print(about)
//        print(address)
//        print(hours)
//        print(category)
//        print(image)
//        print(rating)
//        print(contact)
        addressSelected = address
        addressLabel.text = address
        descriptionLabel.text = about
        hoursLabel.text = hours
        contactLabel.text = contact
        categoryLabel.text = category
        nameLabel.text = name
        
        let widthLabel = widthEstimation(text : categoryLabel.text!, size : 13)
        
        if let window = UIApplication.shared.keyWindow {
            let heigtImage = (window.frame.height/2)-100
            
            contactUsButton.backgroundColor = UIColor.Theme(alpha: 1.0)
            uiView.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeCell)))
            bookmarkView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BookMarked)))
            locationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openMap)))
            contactUsButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(contactTapped)))
            floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rateTapped)))
            
            window.addSubview(uiView)
            uiView.addSubview(thumbnailView)
            uiView.addSubview(nameLabel)
            uiView.addSubview(contactLabel)
            uiView.addSubview(categoryLabel)
            uiView.addSubview(hoursLabel)
            uiView.addSubview(iconView)
            uiView.addSubview(locationView)
            locationView.addSubview(mapLabel)
            locationView.addSubview(addressLabel)
            locationView.addSubview(viewMapLabel)
            locationView.addSubview(mapPinView)
            uiView.addSubview(businessLabel)
            uiView.addSubview(descriptionLabel)
            uiView.addSubview(contactUsButton)
            uiView.addSubview(bookmarkView)
            bookmarkView.addSubview(bookmarkImage)
            uiView.addSubview(backView)
            backView.addSubview(backImage)
            uiView.addSubview(floatMenu)
            floatMenu.addSubview(floatImage)
            
            //get image url
            ApiService().get_image(url_str : image, thumbnailView : self.thumbnailView)
            //rating display
            uiView.rating(rate: rating, horizontal: window.frame.width-150, vertical: heigtImage+85)
            
            bookmarkView.frame = CGRect(x: window.frame.width-50, y: 40, width: 80, height:  80)
            bookmarkImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            backImage.frame = CGRect(x: 0, y: 0, width: 30, height:  30)
            backView.frame = CGRect(x: 10, y: 40, width: 80, height:  80)
            floatMenu.frame = CGRect(x: window.frame.width-80, y: window.frame.height-130, width: 55, height:  55)
            
            uiView.addConstraintsFormat(format: "H:|-17.5-[v0(20)]|", views: floatImage)
            uiView.addConstraintsFormat(format: "V:|-17.5-[v0(20)]|", views: floatImage)
            uiView.addConstraintsFormat(format: "H:|[v0]|", views: thumbnailView)
            uiView.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: nameLabel)
            uiView.addConstraintsFormat(format: "H:|-20-[v0][v1(\(widthLabel))]-20-|", views: contactLabel, categoryLabel)
            uiView.addConstraintsFormat(format: "V:|-\(heigtImage+50)-[v0(20)]-20-|", views: contactLabel)
            uiView.addConstraintsFormat(format: "V:|-\(heigtImage+50)-[v0(20)]-20-|", views: categoryLabel)
            uiView.addConstraintsFormat(format: "V:|-\(heigtImage+90)-[v0(20)]-20-|", views: hoursLabel)
            uiView.addConstraintsFormat(format: "H:|-30-[v0(35)]-15-[v1]|", views: iconView, hoursLabel)
            uiView.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: locationView)
            uiView.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: businessLabel)
            uiView.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: descriptionLabel)
            uiView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: contactUsButton)
            uiView.addConstraintsFormat(format: "V:|[v0(\(heigtImage))]-20-[v1(20)]-40-[v2(35)]-10-[v3(60)]-20-[v4(20)]-5-[v5]-5-[v6(50)]-10-|", views: thumbnailView, nameLabel, iconView, locationView, businessLabel, descriptionLabel, contactUsButton)
            
            uiView.addConstraintsFormat(format: "H:|-5-[v0]-5-[v1(90)][v2(20)]-5-|", views: mapLabel, viewMapLabel, mapPinView)
            uiView.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: addressLabel)
            uiView.addConstraintsFormat(format: "V:|-5-[v0(20)]-5-|", views: viewMapLabel)
            uiView.addConstraintsFormat(format: "V:|-5-[v0(20)]-5-|", views: mapPinView)
            uiView.addConstraintsFormat(format: "V:|-5-[v0(15)]-15-[v1]-5-|", views: mapLabel, addressLabel)
            
            
            uiView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
                
                self.uiView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                
            }, completion:  nil)
        }
        
    }
    
//    var Star = UIImageView()
    
    @objc private func rateTapped() {
        
        let alertController = UIAlertController(title: "Tell us what you think", message: "How would you rate your experience? \n\n\n", preferredStyle: UIAlertControllerStyle.alert)
        
        var count = 1
        var x = 50
        while count <= 5 {
            let Star: UIImageView = {
                let imageView = UIImageView()
                imageView.image = UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate)as UIImage?
                imageView.tintColor = UIColor.lightGray.withAlphaComponent(0.5)
                imageView.tag = count
                return imageView
            }()
            
            let halfButton: UIButton = {
                let button = UIButton(type: .custom) as UIButton
                button.backgroundColor = .clear//UIColor.gray
                return button
            }()
            
            halfButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ratingTapped)))
            Star.frame = CGRect(x: x, y: 80, width: 30, height: 30)
            halfButton.frame = CGRect(x: x, y: 80, width: 15, height: 30)
            alertController.view.addSubview(Star)
            alertController.view.addSubview(halfButton)
            x = x+35
            count = count+1
        }
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Rate", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in

        }))

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in

        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    @objc private func ratingTapped(){
        print(" IT WORKS!!! ")
    }
    
    @objc private func contactTapped(){
        let number = self.contactLabel.text
        contactLuncher.showMenu(number: number!, email: "business@example.com")
    }
    
    @objc private func BookMarked() {
        if bookMarkedDidTapped == 0 {
            BookMarkedSet(image : "ic_bookmark_filled")
            bookMarkedDidTapped = bookMarkedDidTapped + 1
        } else {
            BookMarkedSet(image : "ic_bookmark")
            bookMarkedDidTapped = 0
        }
    }
    
    private func BookMarkedSet(image: String){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            if let window = UIApplication.shared.keyWindow {
                self.bookmarkImage.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)as UIImage?
                self.bookmarkImage.tintColor = UIColor.white
                self.bookmarkView.frame = CGRect(x: window.frame.width-50, y: 40, width: 0, height:  0)
            }
            
        }, completion:  { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
                if let window = UIApplication.shared.keyWindow {
                    self.bookmarkView.frame = CGRect(x: window.frame.width-50, y: 40, width: 80, height:  80)
                }
            }, completion:  { (Bool) in })
        })
    }
    
    @objc func openMap() {
        mapDetail.openCell(address: addressSelected)
    }
    
    @objc func closeCell() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            if let window = UIApplication.shared.keyWindow {
                self.uiView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            }
            
        }, completion:  nil)
    }
}
