//
//  SeeAllDetail.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 22/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SeeAllDetail: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    
    var personalCareNewlyOpened = PersonalCareTabController().newlyOpened
    var personalCareRecommended = RecommendedPersonalCell().recommended
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SeeAllCell
        cell.backgroundColor = UIColor.white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2-15, height: 180)
    }
    
    private func setupCollectionView(){
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.contentInset = UIEdgeInsetsMake(6, 6, 6, 6)
        collectionView?.register(SeeAllCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("arrays ",personalCareNewlyOpened, personalCareRecommended)
        
        setupCollectionView()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
}
