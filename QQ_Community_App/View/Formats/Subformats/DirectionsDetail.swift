//
//  DirectionsDetail.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 17/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class DirectionsDetail: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    static var destination = String()
    static var origin = String()
    private let header: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Going Somewhere"
        return label
    }()
    
    private func getApi(completion: @escaping () -> Void){
        let destinationMarker = DirectionsDetail.destination.replacingOccurrences(of: " ", with: "")
        let originMarker = DirectionsDetail.origin.replacingOccurrences(of: " ", with: "")
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(originMarker)&destination=%22+\(destinationMarker)+%22&mode=transit&key=AIzaSyCkYzVIUwQwsQOPmtYgjRhtByd4nyBrAnU"
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                
                do {
                    let steps = try JSONDecoder().decode(Routes.self, from: data)
                    print(" steps : ", steps)
                    completion()
                } catch let jsonErr {
                    print("ERROR Serializing: ", jsonErr)
                }
                
            }.resume()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.Theme(alpha: 1.0)
        
        DirectionsDetail.destination = TransportationController.destination
        DirectionsDetail.origin = TransportationController.origin
        
        print(" Destination : ",DirectionsDetail.destination.replacingOccurrences(of: " ", with: ""), DirectionsDetail.origin)
        
        getApi {
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.collectionView?.reloadData()
                }, completion:  { (Bool) in
                    
                })
            })
        }
    }
}
