//
//  MapDetail.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 12/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class MapDetail: BaseCell, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var googleMapsView : GMSMapView!
    var region = MKCoordinateRegion()
    let manager = CLLocationManager()
    let mapView: MKMapView = {
        let map = MKMapView()
        return map
    }()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let regionUpdate : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        region = regionUpdate
    }
    
    func openCell(address: String){
        print(address)
        addAnnotations(region: region, searchFor: address)
        
        mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeCell)))
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(mapView)
            
            mapView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
                
                self.mapView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                
            }, completion:  nil)
        }
    }
    
    @objc func closeCell() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            if let window = UIApplication.shared.keyWindow {
                self.mapView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            }
            
        }, completion:  nil)
    }
    
    func addAnnotations(region: MKCoordinateRegion, searchFor: String){
        //remove all annotations
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchFor
        request.region = region
        
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: {
            response, error in
            
            guard let response = response else {
                return
            }
            print("RESPONSE",response)
            
            if response == nil {
                self.showToast(message: "Be sure you are connected to the internet")
            } else {
                print("MY LOCATIONS RESULTS")
                print(response.mapItems)
                
                for item in response.mapItems {
                    let annotation = MKPointAnnotation()
                    let address = String(describing: item.placemark)
                    
                    annotation.coordinate = item.placemark.coordinate
                    annotation.subtitle = address.components(separatedBy: " @")[0]
                    annotation.title = item.name
                    
                    DispatchQueue.main.async {
                        self.mapView.addAnnotation(annotation)
                    }
                }
            }
        })
    }
    
}
