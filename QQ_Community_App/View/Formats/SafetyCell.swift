//
//  HealthCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 16/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class SafetyCell: BaseCell {
    var nameHeight: NSLayoutConstraint?
    static var heightName = CGFloat()
    static var heightDescription = CGFloat()
    var safety: Safety? {
        didSet {
            if let urlImage = safety?.thumbnailImage {
                ApiService().get_image(url_str : urlImage, thumbnailView : self.thumbnailView)
            }
            titleLabel.text = safety?.title
            descriptionLabel.text = safety?.content
            
            SafetyCell.heightName = heightEstimation(text: titleLabel.text!, width: frame.width-40, size: 16, defaultHeight : 20)
            SafetyCell.heightDescription = (heightEstimation(text: descriptionLabel.text!, width: frame.width-40, size: 13, defaultHeight : 5)/2)
            print("height collection cell :", 220+SafetyCell.heightName+SafetyCell.heightDescription+15)
            addConstraintsFormat(format: "V:|[v0(220)]-3-[v1(\(SafetyCell.heightName))]-3-[v2(\(SafetyCell.heightDescription))]", views: thumbnailView, titleLabel, descriptionLabel)
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.image = UIImage(named: "no_image")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "No Title"
//        label.backgroundColor = UIColor.Background(alpha: 1.0)
        label.textColor = UIColor.rgba(red: 123, green: 123, blue: 123, alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.backgroundColor = .clear//UIColor.lightGray
        textView.text = "No Description"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(13)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.layer.cornerRadius = 5
        return textView
    }()
    
    private func setupConstraints(){
        addConstraintsFormat(format: "H:|[v0]|", views: thumbnailView, titleLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: descriptionLabel)
    }
    
    override func setupViews(){
        addSubview(thumbnailView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        
        setupConstraints()
    }
}
