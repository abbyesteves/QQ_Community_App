//
//  SeeAllCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 22/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SeeAllCell: BaseCell {
    
    var businessFeed: BusinessFeed? {
        didSet {
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.image = UIImage(named: "no_image")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let NameLabel: UILabel = {
        let label = UILabel()
        label.text = "No Title"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let ContactLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "Not Available"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(12)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let CategoryLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.text = "No Category"
        textView.textContainerInset = UIEdgeInsetsMake(3, 5, 5, 5)
        textView.font = textView.font?.withSize(13)
        textView.textAlignment = .center
        textView.textColor = UIColor.rgba(red: 2, green: 58, blue: 93, alpha: 1.0)
        textView.backgroundColor = UIColor.rgba(red: 202, green: 223, blue: 232, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        let widthCategory = widthEstimation(text : CategoryLabel.text, size : 13)
        
        addSubview(thumbnailView)
        addSubview(NameLabel)
        addSubview(ContactLabel)
        addSubview(CategoryLabel)
        
        addConstraintsFormat(format: "H:|[v0]|", views: thumbnailView)
        addConstraintsFormat(format: "V:|[v0(100)]-3-[v1(20)]-5-[v2(20)]-5-[v3(20)]-8-|", views: thumbnailView, NameLabel, ContactLabel, CategoryLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: NameLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: ContactLabel)
        addConstraintsFormat(format: "H:|-10-[v0(\(widthCategory))]-10-|", views: CategoryLabel)
        
    }
}
