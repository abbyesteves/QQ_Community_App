//
//  SurveyCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 29/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class SurveyCell: BaseCell {
    static var heightName = CGFloat()
    var survey: Survey? {
        didSet {
            questionLabel.text = survey?.question
            
            SafetyCell.heightName = heightEstimation(text: questionLabel.text!, width: frame.width-20, size: 7.5, defaultHeight : 5)
            addConstraintsFormat(format: "V:|-10-[v0(\(SafetyCell.heightName))]-10-[v1(130)]", views: questionLabel, answer)
        }
    }
    
    let questionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "This is a dummy description. This could be anything that the user set from the configurator."
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        return textView
    }()
    
    let answer: UITextView = {
        let textField = UITextView()
        textField.backgroundColor = UIColor.white
        textField.layer.shadowColor = UIColor.gray.cgColor
        textField.layer.shadowOpacity = 0.5
        textField.layer.shadowOffset = CGSize(width: 0, height: 5)
        textField.layer.shadowRadius = 0.6
        textField.isEditable = true
        textField.isSelectable = true
        textField.isScrollEnabled = true
        return textField
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(questionLabel)
        addSubview(answer)
        
//        addConstraintsFormat(format: "V:|-10-[v0(\(height))]-10-[v1(130)]", views: questionLabel, answer)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: questionLabel)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: answer)
    }
}
