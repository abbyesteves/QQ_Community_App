//
//  BusinessNewCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import Foundation
class BusinessNewCell: BaseCell {
    static var widthCategory = CGFloat()
    var businessFeed: BusinessFeed? {
        didSet {
            if let urlImage = businessFeed?.thumbnailImage {
                ApiService().get_image(url_str : urlImage, thumbnailView : self.thumbnailView)
            }
            NameLabel.text = businessFeed?.name
            AboutLabel.text = businessFeed?.about
            CategoryLabel.text = businessFeed?.category
            BusinessNewCell.widthCategory = widthEstimation(text : CategoryLabel.text!, size : 13)
            addConstraintsFormat(format: "H:|-160-[v0(\(BusinessNewCell.widthCategory))]-10-|", views: CategoryLabel)
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let NameLabel: UILabel = {
        let label = UILabel()
        label.text = "Title of the news and announcement"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let AboutLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.rgba(red: 123, green: 123, blue: 123, alpha: 1.0)
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.font = textView.font?.withSize(14)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    let CategoryLabel: UILabel = {
        let label = UILabel()
        label.text = "No Category"
        label.textAlignment = .center
        label.numberOfLines = 1
        label.textColor = UIColor.rgba(red: 2, green: 58, blue: 93, alpha: 1.0)
        label.backgroundColor = UIColor.rgba(red: 202, green: 223, blue: 232, alpha: 1.0)
        label.font = label.font.withSize(13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func setupViews(){
        addSubview(thumbnailView)
        addSubview(NameLabel)
        addSubview(AboutLabel)
        addSubview(CategoryLabel)
        
        addConstraintsFormat(format: "H:|-5-[v0(150)]-10-[v1]", views: thumbnailView, NameLabel)
        addConstraintsFormat(format: "V:|-5-[v0(100)]", views: thumbnailView)
        addConstraintsFormat(format: "V:|-10-[v0(20)]-5-[v1]-5-[v2(20)]-5-|", views: NameLabel, AboutLabel, CategoryLabel)
        addConstraintsFormat(format: "H:|-165-[v0]-5-|", views: AboutLabel)
        
    }
}
