//
//  HospitalCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 04/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class HospitalCell: BaseCell {
    static var heightName = CGFloat()
    var hospital: Hospital? {
        didSet {
            nameLabel.text = hospital?.name?.uppercased()
            addressText.text = hospital?.address
            contactText.text = hospital?.contact
            
            HospitalCell.heightName = heightEstimation(text: nameLabel.text!, width: frame.width-150, size: 16, defaultHeight : 20)
            addConstraintsFormat(format: "V:|-10-[v0(\(HospitalCell.heightName))]-2-[v1]-5-[v2(20)]-10-|", views: nameLabel, addressText, contactText)
        }
    }
    
    let textView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        return view
    }()
    
    let textImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "ic_sms_compose")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.darkGray
        return imageView;
    }()
    
    let callView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        return view
    }()
    
    let callImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "ic_phone")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView;
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name of hospital name of hospital"
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        return label
    }()
    
    let addressText: UITextView = {
        let textView = UITextView()
        textView.text = "This is a dummy description. This could be anything that the user set from the configurator."
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor.gray
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    let contactText: UITextView = {
        let textView = UITextView()
        textView.text = "0917 306 9221"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor.gray
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    lazy var contactLauncher : ContactLauncher = {
        let launcher = ContactLauncher()
        return launcher
    }()
    
    private func setupConstraints(){
        addConstraintsFormat(format: "H:|-12.5-[v0(25)]-12.5-|", views: callImage)
        addConstraintsFormat(format: "V:|-12.5-[v0(25)]-12.5-|", views: callImage)
        addConstraintsFormat(format: "H:|-12.5-[v0(25)]-12.5-|", views: textImage)
        addConstraintsFormat(format: "V:|-12.5-[v0(25)]-12.5-|", views: textImage)
        
        addConstraintsFormat(format: "H:|-\(frame.width-130)-[v0(50)]-10-[v1(50)]", views: textView, callView)
        addConstraintsFormat(format: "V:|-30-[v0(50)]-30-|", views: textView)
        addConstraintsFormat(format: "V:|-30-[v0(50)]-30-|", views: callView)
        addConstraintsFormat(format: "H:|-10-[v0(\(frame.width-150))]-10-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-10-[v0(\(frame.width-150))]-10-|", views: addressText)
        addConstraintsFormat(format: "H:|-10-[v0(\(frame.width-150))]-10-|", views: contactText)
    }
    
    private func setupGestures() {
        textView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textMessage)))
        callView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(call)))
    }
    
    @objc func textMessage() {
        contactLauncher.sms(contactNumber: contactText.text!)
    }
    
    @objc func call() {
        contactLauncher.call(contactNumber: contactText.text!)
    }
    
    override func setupViews(){
        addSubview(nameLabel)
        addSubview(addressText)
        addSubview(contactText)
        addSubview(textView)
        textView.addSubview(textImage)
        addSubview(callView)
        callView.addSubview(callImage)
        
        setupConstraints()
        setupGestures()
    }
}
