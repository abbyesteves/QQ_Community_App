//
//  SurveyCategoryCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 26/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SurveyCategoryCell: BaseCell {
    var surveyCategory: SurveyCategory? {
        didSet {
            titleText.text = surveyCategory?.title
            thumbnailImage.image = UIImage(named: (surveyCategory?.thumbnailImage)!)
            subTitleText.text = surveyCategory?.subtitle
        }
    }
    
    let boarder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        return view
    }()
    
    let titleText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(13)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "General"
        return label
    }()
    
    let subTitleText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(11)
        label.text = "General Community Survey"
        return label
    }()
    
    let thumbnailImage: UIImageView = {
        let image = UIImage(named: "ic_survey_community")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    private func setupView() {
        addSubview(thumbnailImage)
        addSubview(boarder)
        addSubview(titleText)
        addSubview(subTitleText)
    }
    
    private func setupConstraints() {
        addConstraintsFormat(format: "H:|-10-[v0(60)]", views: thumbnailImage)
        addConstraintsFormat(format: "V:|-10-[v0(60)]|", views: thumbnailImage)
        addConstraintsFormat(format: "H:|-80-[v0(1.5)]", views: boarder)
        addConstraintsFormat(format: "V:|-15-[v0]-15-|", views: boarder)
        addConstraintsFormat(format: "H:|-90-[v0]", views: titleText)
        addConstraintsFormat(format: "V:|-30-[v0(10)]|", views: titleText)
        addConstraintsFormat(format: "H:|-90-[v0]", views: subTitleText)
        addConstraintsFormat(format: "V:|-45-[v0(10)]|", views: subTitleText)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}
