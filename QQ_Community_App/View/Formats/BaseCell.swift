//
//  BaseCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 16/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
