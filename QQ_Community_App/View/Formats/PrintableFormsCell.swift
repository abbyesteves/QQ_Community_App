//
//  PrintableFormsCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class PrintableFormsCell: BaseCell {
    static var heightName = CGFloat()
    static var heightDescription = CGFloat()
    var form: Forms? {
        didSet {
            nameLabel.text = form?.title?.uppercased()
            bodyText.text = form?.body
            
            PrintableFormsCell.heightName = heightEstimation(text: nameLabel.text!, width: frame.width-20, size: 16, defaultHeight : 20)
            addConstraintsFormat(format: "V:|-5-[v0(\(PrintableFormsCell.heightName))]-5-[v1]-5-|", views: nameLabel, bodyText)
        }
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name of form to be printed"
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        return label
    }()
    
    let bodyText: UITextView = {
        let textView = UITextView()
        textView.text = "This is a dummy description. This could be anything that the user set from the configurator."
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor.gray
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.isSelectable = false
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    private func setupConstraints(){
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: bodyText)
//        addConstraintsFormat(format: "V:|-10-[v0(35)]-10-[v1]-10-|", views: nameLabel, bodyText)
    }
    
    private func setupView(){
        addSubview(nameLabel)
        addSubview(bodyText)
    }
    
    override func setupViews(){
        
        setupView()
        setupConstraints()
        
    }
    
}
