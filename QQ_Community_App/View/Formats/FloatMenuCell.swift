//
//  FloatMenuCell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 22/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class FloatMenuCell: BaseCell {
    var menu: Menu? {
        didSet {
            if (menu?.icon == "") {
                menuLabel.textColor = UIColor.gray
                menuLabel.font = menuLabel.font.withSize(14)
                iconImage.removeFromSuperview()
                addConstraintsFormat(format: "H:|-15-[v0]|", views: menuLabel)
            }
            menuLabel.text = menu?.label
            iconImage.image = UIImage(named: (menu?.icon)!)?.withRenderingMode(.alwaysTemplate)
        }
    }
    
    let menuLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.text = "menu"
        label.font = label.font.withSize(12)
        return label
    }()
    
    let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.black
        imageView.image = UIImage(named: "ic_action_news")
        return imageView;
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(iconImage)
        addSubview(menuLabel)
        
        addConstraintsFormat(format: "H:|-15-[v0(23)]-30-[v1]|", views: iconImage, menuLabel)
        addConstraintsFormat(format: "V:|-15-[v0(23)]", views: iconImage)
        addConstraintsFormat(format: "V:|-20-[v0]", views: menuLabel)
        
    }
}
