//
//  MenuBar.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit


class MenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate,
                 UICollectionViewDelegateFlowLayout{
    
    var widthBarView: NSLayoutConstraint?
    var newsController: NewsViewController?
    var barLeftAnchor: NSLayoutConstraint?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let indicatorBarView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 2/255, green: 58/255, blue: 93/255, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let cellId = "cellId"
    let menuLabels = ["ANNOUNCEMENTS ", "TWITTER", "SOCIAL VOICES", "MEDICAL ASSISTANCE", "PNP", "BFP", "MDRRMO"]
    
    private func setupCollectionView() {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
//        collectionView.isPagingEnabled = true
    }
    
    private func setupView() {
        addSubview(collectionView)
    }
    
    private func setupConstraints() {
        addConstraintsFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsFormat(format: "V:|[v0]|", views: collectionView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCollectionView()
        setupView()
        setupConstraints()
        
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)

        let selectedIndexPath = NSIndexPath(item: 0, section: 0)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.selectItem(at: selectedIndexPath as IndexPath, animated: false, scrollPosition: [])
        
        setupIndicatorBar()
    }
    
    func setupIndicatorBar(){
        addSubview(indicatorBarView)
        
        barLeftAnchor = indicatorBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        barLeftAnchor?.isActive = true
        
        indicatorBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        indicatorBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/7).isActive = true
//        indicatorBarView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        widthBarView = indicatorBarView.widthAnchor.constraint(equalToConstant: 120)
        widthBarView?.isActive = true
        indicatorBarView.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //change width of var view
        if indexPath.item == 0 {
            widthBarView?.constant = 120
        } else  if indexPath.item == 1 {
            widthBarView?.constant = 90
        } else  if indexPath.item == 2 {
            widthBarView?.constant = 110
        } else  if indexPath.item == 3 {
            widthBarView?.constant = 150
        } else  if indexPath.item == 6 {
            widthBarView?.constant = 70
        } else {
            widthBarView?.constant = 50
        }
        
        //scroll navbar index to center
        let indexPath = NSIndexPath(item: indexPath.item, section: 0)
        collectionView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
        //scroll to page
        newsController?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
        cell.menuLabel.text = menuLabels[indexPath.item]
        cell.backgroundColor = UIColor.Theme(alpha: 1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: 120, height: frame.height)
        } else if indexPath.item == 1 {
            return CGSize(width: 90, height: frame.height)
        } else if indexPath.item == 2 {
            return CGSize(width: 110, height: frame.height)
        } else if indexPath.item == 3 {
            return CGSize(width: 150, height: frame.height)
        } else if indexPath.item == 6 {
            return CGSize(width: 70, height: frame.height)
        }
        return CGSize(width: 50, height: frame.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MenuCell: BaseCell {
    let menuLabel: UILabel = {
        let label = UILabel()
        label.text = "News"
        label.textColor = UIColor.white
        label.font = UIFont(name: "Helvetica", size: 10)
        label.numberOfLines = 1
        label.textAlignment = .center;
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
//        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override var isHighlighted: Bool {
        didSet {
            menuLabel.textColor = UIColor.white
//            menuLabel.textColor = isHighlighted ? UIColor(red: 2/255, green: 58/255, blue: 93/255, alpha: 1.0) : UIColor.white
        }
    }
    
    override var isSelected: Bool {
        didSet {
            menuLabel.textColor = UIColor.white
//            menuLabel.textColor = isSelected ? UIColor(red: 2/255, green: 58/255, blue: 93/255, alpha: 1.0) : UIColor.white
        }
    }
    
    private func setupView(){
        addSubview(menuLabel)
    }
    
    private func setupConstraints(){
        addConstraintsFormat(format: "H:[v0]", views: menuLabel)
        addConstraintsFormat(format: "V:[v0]", views: menuLabel)
        
        addConstraint(NSLayoutConstraint(item: menuLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: menuLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    override func setupViews() {
        super.setupViews()
        setupView()
        setupConstraints()
    }
    
}















