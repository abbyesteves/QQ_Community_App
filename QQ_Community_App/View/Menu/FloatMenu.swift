//
//  FloatingMenuLauncher.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 22/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class FloatMenu: BaseCell{
    
    var menuOpened = "no"
    
    lazy var menuView = UIView()
    
    lazy var floatImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_phone")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    lazy var floatMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 28
        return view
    }()
    
    //Medical Assistance
    let medicalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Medical Assistance"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let medicalImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "hospital_filled")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let medicalMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 20
        return view
    }()
    
    //MDRRMO
    let mdrrmoLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "MDRRMO"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let mdrrmoImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "error_filled")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let mdrrmoMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 20
        return view
    }()
    
    //PNP
    let pnpLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "PNP"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let pnpImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "taxi_filled")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let pnpMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 20
        return view
    }()
    
    //BFP
    let bfpLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "BFP"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let bfpImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "attention")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let bfpMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 20
        return view
    }()
    
    //Mayor's hotline
    let hotlineLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Mayor's Hotline"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let hotlineImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "government")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let hotlineMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 20
        return view
    }()
    
    func Tapped() {
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(menuView)
            menuView.frame = window.frame
            
            menuView.addSubview(medicalLabel)
            menuView.addSubview(medicalMenu)
            medicalMenu.addSubview(medicalImage)
            
            menuView.addSubview(mdrrmoLabel)
            menuView.addSubview(mdrrmoMenu)
            mdrrmoMenu.addSubview(mdrrmoImage)
            
            menuView.addSubview(pnpLabel)
            menuView.addSubview(pnpMenu)
            pnpMenu.addSubview(pnpImage)
            
            menuView.addSubview(bfpLabel)
            menuView.addSubview(bfpMenu)
            bfpMenu.addSubview(bfpImage)
            
            menuView.addSubview(hotlineLabel)
            menuView.addSubview(hotlineMenu)
            hotlineMenu.addSubview(hotlineImage)
            
            
            medicalImage.alpha = 0
            medicalImage.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
            medicalMenu.frame = CGRect(x: menuView.frame.width-70, y: menuView.frame.height-300, width: 0, height: 0)
            medicalLabel.frame = CGRect(x: menuView.frame.width-50, y: menuView.frame.height-295, width: 0, height: 0)
            
            
            mdrrmoImage.alpha = 0
            mdrrmoImage.frame = CGRect(x: 10, y: 5, width: 20, height: 20)
            mdrrmoMenu.frame = CGRect(x: menuView.frame.width-70, y: menuView.frame.height-250, width: 0, height: 0)
            mdrrmoLabel.frame = CGRect(x: menuView.frame.width-50, y: menuView.frame.height-240, width: 0, height: 0)
            
            
            pnpImage.alpha = 0
            pnpImage.frame = CGRect(x: 10, y: 5, width: 20, height: 20)
            pnpMenu.frame = CGRect(x: menuView.frame.width-70, y: menuView.frame.height-200, width: 0, height: 0)
            pnpLabel.frame = CGRect(x: menuView.frame.width-50, y: menuView.frame.height-195, width: 0, height: 0)
            
            
            bfpImage.alpha = 0
            bfpImage.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
            bfpMenu.frame = CGRect(x: menuView.frame.width-70, y: menuView.frame.height-150, width: 0, height: 0)
            bfpLabel.frame = CGRect(x: menuView.frame.width-50, y: menuView.frame.height-185, width: 0, height: 0)
            
            
            hotlineImage.alpha = 0
            hotlineImage.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
            hotlineMenu.frame = CGRect(x: menuView.frame.width-70, y: menuView.frame.height-80, width: 0, height: 0)
            hotlineLabel.frame = CGRect(x: menuView.frame.width-50, y: menuView.frame.height-145, width: 0, height: 0)
            
            medicalMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(medicalTapped)))
            mdrrmoMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mdrrmoTapped)))
            pnpMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pnpTapped)))
            hotlineMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hotlineTapped)))
            bfpMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bfpTapped)))
            menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
            
            if ( menuOpened == "no"){
                open()
            } else {
                close()
            }
        }
    }
    
    lazy var contactLuncher: ContactLauncher = {
        let launcher = ContactLauncher()
        return launcher
    }()
    
    @objc func medicalTapped(){
        let number = "09436018271"
        close()
        contactLuncher.showMenu(number: number, email : "medical@example.com")
    }
    
    @objc func mdrrmoTapped(){
        let number = "09175057827"
        close()
        contactLuncher.showMenu(number: number, email : "mddrmo@example.com")
    }
    
    @objc func pnpTapped(){
        let number = "09227888221"
        close()
        contactLuncher.showMenu(number: number, email : "pnp@example.com")
    }
    
    @objc func hotlineTapped(){
        let number = "09399997827"
        close()
        contactLuncher.showMenu(number: number, email : "hotline@example.com")
    }
    
    @objc func bfpTapped(){
        let number = "09423527440"
        close()
        contactLuncher.showMenu(number: number, email : "bfp@example.com")
    }
    
    @objc func open(){
        menuOpened = "yes"
        //animation
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.menuView.alpha = 1
            self.floatImage.transform = CGAffineTransform(rotationAngle: 180)
            self.hotlineMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-150, width: 40, height: 40)
            
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.hotlineImage.alpha = 1
            self.hotlineLabel.frame = CGRect(x: self.menuView.frame.width-180, y: self.menuView.frame.height-145, width: 100, height: 30)
            
            self.bfpMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-200, width: 40, height: 40)
            
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.bfpImage.alpha = 1
            self.bfpLabel.frame = CGRect(x: self.menuView.frame.width-130, y: self.menuView.frame.height-195, width: 50, height: 30)
            
            self.pnpMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-250, width: 40, height: 40)
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.pnpImage.alpha = 1
            self.pnpLabel.frame = CGRect(x: self.menuView.frame.width-130, y: self.menuView.frame.height-245, width: 50, height: 30)
            
            self.mdrrmoMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-300, width: 40, height: 40)
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.4, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.mdrrmoImage.alpha = 1
            self.mdrrmoLabel.frame = CGRect(x: self.menuView.frame.width-160, y: self.menuView.frame.height-295, width: 80, height: 30)
            
            self.medicalMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-350, width: 40, height: 40)
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.medicalImage.alpha = 1
            self.medicalLabel.frame = CGRect(x: self.menuView.frame.width-200, y: self.menuView.frame.height-345, width: 120, height: 30)
            
        }, completion:  nil)
        
    }
    
    @objc func close(){
        menuOpened = "no"
        UIView.animate(withDuration: 0.2, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.menuView.alpha = 0
            self.floatImage.transform = CGAffineTransform(rotationAngle: 0)
            self.hotlineMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-80, width: 0, height: 0)
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.4, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.hotlineLabel.frame = CGRect(x: self.menuView.frame.width-50, y: self.menuView.frame.height-145, width: 0, height: 0)
            
            self.bfpImage.alpha = 0
            self.bfpMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-150, width: 0, height: 0)
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.bfpLabel.frame = CGRect(x: self.menuView.frame.width-50, y: self.menuView.frame.height-185, width: 0, height: 0)
            
            self.pnpImage.alpha = 0
            self.pnpLabel.frame = CGRect(x: self.menuView.frame.width-50, y: self.menuView.frame.height-195, width: 0, height: 0)
            
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.pnpMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-200, width: 0, height: 0)
            
            self.mdrrmoImage.alpha = 0
            self.mdrrmoLabel.frame = CGRect(x: self.menuView.frame.width-50, y: self.menuView.frame.height-240, width: 0, height: 0)
            
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            
            self.mdrrmoMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-250, width: 0, height: 0)
            
            self.medicalImage.alpha = 0
            self.medicalLabel.frame = CGRect(x: self.menuView.frame.width-50, y: self.menuView.frame.height-295, width: 0, height: 0)
            
            
        }, completion:  nil)
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            self.medicalMenu.frame = CGRect(x: self.menuView.frame.width-70, y: self.menuView.frame.height-300, width: 0, height: 0)
            
            
        }, completion:  nil)
        
    }
}
