//
//  SurveyBasicInfo.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 14/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
class SurveyBasicInfo: BaseCell, UIPickerViewDataSource, UIPickerViewDelegate {
    //values variables need to set
    static var gender = ""
    static var occupation = ""
    static var status = ""
    static var age = "17 & below"
    //
    let ageRange = ["17 & below", "18 - 24", "25 - 30", "31 - 40", "41 - 60", "61 & up"]
    let statuses = ["None", "Housewife", "Student", "Unemployed", "Employed", "Entrepreneur", "Senior Citizen"]
    let pickerView = UIPickerView()
    let dropPressView = UIView()

    let titleheader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(18)
        label.text = "Basic Information"
        return label
    }()

    let descText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        label.text = "For us to provide better analytics"
        return label
    }()

    let genderText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "GENDER"
        return label
    }()

    let ageText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "AGE GROUP"
        return label
    }()

    let occupationText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "OCCUPATIONAL STATUS"
        return label
    }()

    let statusText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "CIVIL STATUS"
        return label
    }()

    @objc func titlHeader(title: String) -> UILabel?{
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = title
        return label
    }

    let maleView: UIView = {
        let view = UIView()
        return view
    }()

    let maleImage: UIImageView = {
        let image = UIImage(named: "ic_male")
        let imageView = UIImageView(image: image)
        return imageView
    }()

    let maleText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Male"
        return label
    }()

    let femaleView: UIView = {
        let view = UIView()
        return view
    }()

    let femaleImage: UIImageView = {
        let image = UIImage(named: "ic_female")
        let imageView = UIImageView(image: image)
        return imageView
    }()

    let femaleText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Female"
        return label
    }()

    //
    let dropView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 0.5
        return view
    }()

    let dropOptionView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: 5)
        view.layer.shadowRadius = 8
        return view
    }()

    let dropSelectedText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.text = "None"
        return label
    }()

    let dropImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_down")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    //
    let genderView: UIView = {
        let view = UIView()
        return view
    }()
    let singleButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Single", for: .normal)
        return button
    }()

    let singleStatusSelectView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1.5
        view.layer.cornerRadius = 9
        return view
    }()
    
    let separatedButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Separated", for: .normal)
        button.tag = 6
        return button
    }()

    let separateStatusSelectView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1.5
        view.layer.cornerRadius = 9
        return view
    }()
    
    let marriedButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Married", for: .normal)
        button.tag = 6
        return button
    }()

    let marriedStatusSelectView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1.5
        view.layer.cornerRadius = 9
        return view
    }()

    let widowedStatusSelectView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1.5
        view.layer.cornerRadius = 9
        return view
    }()
    
    let widowedButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Widowed", for: .normal)
        button.tag = 6
        return button
    }()
    
    let noneButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("None", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let housewifeButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("House Wife", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let studentButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Student", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let unemployedButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Unemployed", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let employedButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Employed", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let entrepreneurButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Entrepreneur", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()
    
    let seniorButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.setTitle("Senior Citizen", for: .normal)
        button.contentHorizontalAlignment = .left
        button.tag = 6
        return button
    }()

    // Picker pre-req
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ageRange.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var label: UILabel
        if let view = view as? UILabel { label = view }
        else { label = UILabel() }

        label.textColor = UIColor.white
        label.backgroundColor = UIColor.ThemeDark(alpha: 0.5)
        label.textAlignment = .center
        label.font = UIFont(name: "", size: 20)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.text = ageRange[row]

        return label
    }

    @objc func dropTapped(){

        if let window = UIApplication.shared.keyWindow {

            dropPressView.backgroundColor = UIColor(white: 0, alpha: 0)

            dropPressView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            noneButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(noneTapped)))
            housewifeButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(housewifeTapped)))
            studentButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(studentTapped)))
            unemployedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(unemployedTapped)))
            employedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(employedTapped)))
            entrepreneurButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(entrepreneurTapped)))
            seniorButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(seniorTapped)))
            
            window.addSubview(dropPressView)
            window.addSubview(dropOptionView)
            dropOptionView.addSubview(noneButton)
            dropOptionView.addSubview(housewifeButton)
            dropOptionView.addSubview(studentButton)
            dropOptionView.addSubview(unemployedButton)
            dropOptionView.addSubview(employedButton)
            dropOptionView.addSubview(entrepreneurButton)
            dropOptionView.addSubview(seniorButton)
            

            dropOptionView.frame = CGRect(x: 30, y: dropView.frame.maxY+35, width: frame.width-60, height: 0)
            dropPressView.alpha = 0
            dropPressView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)

            noneButton.alpha = 0
            housewifeButton.alpha = 0
            studentButton.alpha = 0
            unemployedButton.alpha = 0
            employedButton.alpha = 0
            entrepreneurButton.alpha = 0
            seniorButton.alpha = 0
            
            dropOptionView.addConstraintsFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(20)]-5-[v5(20)]-5-[v6(20)]-10-|", views: noneButton, housewifeButton, studentButton, unemployedButton, employedButton, entrepreneurButton, seniorButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: noneButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: housewifeButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: studentButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: unemployedButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: employedButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: entrepreneurButton)
            dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: seniorButton)

            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.dropPressView.alpha = 1
                self.dropOptionView.frame = CGRect(x: 30, y: self.dropView.frame.maxY+35, width: self.frame.width-60, height: 190)
                
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.noneButton.alpha = 1
                    self.housewifeButton.alpha = 1
                    self.studentButton.alpha = 1
                    self.unemployedButton.alpha = 1
                    self.employedButton.alpha = 1
                    self.entrepreneurButton.alpha = 1
                    self.seniorButton.alpha = 1
//                    var height = 10
//                    for status in self.statuses {
//                        let statusButton: UIButton = {
//                            let button = UIButton()
//                            button.backgroundColor = .clear
//                            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
//                            button.setTitleColor(UIColor.gray, for: .normal)
//                            button.setTitle(status, for: .normal)
//                            button.contentHorizontalAlignment = .left
//                            button.tag = 6
//                            return button
//                        }()
//
//                        self.dropOptionView.addSubview(statusButton)
//                        self.dropOptionView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: statusButton)
//                        self.dropOptionView.addConstraintsFormat(format: "V:|-\(height)-[v0(20)]|", views: statusButton)
//                        height = height+10
//                    }
                }, completion: { (Bool) in })
            })
        }
    }

    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.noneButton.alpha = 0
            self.housewifeButton.alpha = 0
            self.studentButton.alpha = 0
            self.unemployedButton.alpha = 0
            self.employedButton.alpha = 0
            self.entrepreneurButton.alpha = 0
            self.seniorButton.alpha = 0
           
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.dropPressView.alpha = 0
                self.dropOptionView.frame = CGRect(x: 30, y: self.dropView.frame.maxY+35, width: self.frame.width-60, height: 0)
            }, completion: { (Bool) in
                self.dropOptionView.removeFromSuperview()
            })
        })
    }
    
    func ifAnswered() -> Bool {
        if SurveyBasicInfo.occupation != "None" &&
           SurveyBasicInfo.gender != "" &&
           SurveyBasicInfo.status != "" {
            return true
        }
        return false
    }
    
    @objc private func noneTapped() {
        SurveyBasicInfo.occupation = "None"
        dropSelectedText.text = "None"
        bgDismiss()
    }
    
    @objc private func housewifeTapped() {
        SurveyBasicInfo.occupation = "House Wife"
        dropSelectedText.text = "House Wife"
        bgDismiss()
    }
    
    @objc private func studentTapped() {
        SurveyBasicInfo.occupation = "Student"
        dropSelectedText.text = "Student"
        bgDismiss()
    }
    
    @objc private func unemployedTapped() {
        SurveyBasicInfo.occupation = "Unemployed"
        dropSelectedText.text = "Unemployed"
        bgDismiss()
    }
    
    @objc private func employedTapped() {
        SurveyBasicInfo.occupation = "Employed"
        dropSelectedText.text = "Employed"
        bgDismiss()
    }
    
    @objc private func entrepreneurTapped() {
        SurveyBasicInfo.occupation = "Entrepreneur"
        dropSelectedText.text = "Entrepreneur"
        bgDismiss()
    }
    
    @objc private func seniorTapped() {
        SurveyBasicInfo.occupation = "Senior Citizen"
        dropSelectedText.text = "Senior Citizen"
        bgDismiss()
    }

    @objc private func maleTapped(){
        SurveyBasicInfo.gender = "male"
        maleView.alpha = 1
        femaleView.alpha = 0.5
    }

    @objc private func femaleTapped(){
        SurveyBasicInfo.gender = "female"
        maleView.alpha = 0.5
        femaleView.alpha = 1
    }

    @objc private func separatedTapped(){
        SurveyBasicInfo.status = "separated"
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.singleStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.separateStatusSelectView.backgroundColor = UIColor.white
            self.marriedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.widowedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
        }, completion:  nil)
    }

    @objc private func widowedTapped(){
        SurveyBasicInfo.status = "widowed"

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.singleStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.separateStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.marriedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.widowedStatusSelectView.backgroundColor = UIColor.white
        }, completion:  nil)
    }

    @objc private func singleTapped(){
        SurveyBasicInfo.status = "single"

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.singleStatusSelectView.backgroundColor = UIColor.white
            self.separateStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.marriedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.widowedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
        }, completion:  nil)

    }

    @objc private func marriedTapped(){
        SurveyBasicInfo.status = "married"

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.singleStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.separateStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
            self.marriedStatusSelectView.backgroundColor = UIColor.white
            self.widowedStatusSelectView.backgroundColor = UIColor.Theme(alpha: 1.0)
        }, completion:  nil)

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        SurveyBasicInfo.age = ageRange[row]
    }

    private func setupConstraints(){
        addConstraintsFormat(format: "H:|-20-[v0]", views: titleheader)
        // Vertical
        addConstraintsFormat(format: "V:|-20-[v0(20)]-5-[v1(20)]-20-[v2(20)]-80-[v3(20)][v4(150)]-10-[v5(20)]-10-[v6(30)]-20-[v7(20)]-10-[v8(50)]", views: titleheader, descText, genderText, ageText, pickerView, occupationText, dropView, statusText, genderView)
        addConstraintsFormat(format: "H:|-20-[v0]", views: descText)
        addConstraintsFormat(format: "H:|-30-[v0]", views: genderText)
        //
        addConstraintsFormat(format: "H:|-\((frame.width/2)/2)-[v0(80)]-\((frame.width/4)/2)-[v1(80)]", views: maleView, femaleView)
        addConstraintsFormat(format: "V:|-100-[v0(80)]|", views: maleView)
        addConstraintsFormat(format: "V:|-100-[v0(80)]|", views: femaleView)
        addConstraintsFormat(format: "H:|-15-[v0(50)]", views: maleImage)
        addConstraintsFormat(format: "V:|-5-[v0(50)]|", views: maleImage)
        addConstraintsFormat(format: "H:|[v0]|", views: maleText)
        addConstraintsFormat(format: "V:|-60-[v0(10)]|", views: maleText)
        addConstraintsFormat(format: "H:|-15-[v0(50)]", views: femaleImage)
        addConstraintsFormat(format: "V:|-5-[v0(50)]|", views: femaleImage)
        addConstraintsFormat(format: "H:|[v0]|", views: femaleText)
        addConstraintsFormat(format: "V:|-60-[v0(10)]|", views: femaleText)
        //
        addConstraintsFormat(format: "H:|-30-[v0]", views: ageText)
        addConstraintsFormat(format: "H:|[v0]|", views: pickerView)
        //
        addConstraintsFormat(format: "H:|-30-[v0]", views: occupationText)
        addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: dropView)
        addConstraintsFormat(format: "H:|-20-[v0]-100-[v1(15)]-10-|", views: dropSelectedText, dropImage)
        addConstraintsFormat(format: "V:|-5-[v0(20)]|", views: dropSelectedText)
        addConstraintsFormat(format: "V:|-8-[v0(15)]|", views: dropImage)
        //
        addConstraintsFormat(format: "H:|-30-[v0]", views: statusText)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: genderView)
        
        addConstraintsFormat(format: "H:|[v0(18)]", views: singleStatusSelectView)
        addConstraintsFormat(format: "V:|[v0(18)]", views: singleStatusSelectView)
        addConstraintsFormat(format: "H:|[v0(18)]", views: marriedStatusSelectView)
        addConstraintsFormat(format: "V:|[v0(18)]", views: marriedStatusSelectView)
        addConstraintsFormat(format: "H:|[v0(18)]", views: separateStatusSelectView)
        addConstraintsFormat(format: "V:|[v0(18)]", views: separateStatusSelectView)
        addConstraintsFormat(format: "H:|[v0(18)]", views: widowedStatusSelectView)
        addConstraintsFormat(format: "V:|[v0(18)]", views: widowedStatusSelectView)
        addConstraintsFormat(format: "H:|-50-[v0(100)]-20-[v1(100)]-50-|", views: singleButton, marriedButton)
        addConstraintsFormat(format: "V:|[v0(20)]|", views: singleButton)
        addConstraintsFormat(format: "V:|[v0(20)]|", views: marriedButton)
        addConstraintsFormat(format: "H:|-50-[v0(110)]-20-[v1(100)]-50-|", views: separatedButton, widowedButton)
        addConstraintsFormat(format: "V:|-30-[v0(20)]|", views: separatedButton)
        addConstraintsFormat(format: "V:|-30-[v0(20)]|", views: widowedButton)
        
    }

    private func setupGestures(){
        dropView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dropTapped)))
        maleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(maleTapped)))
        femaleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(femaleTapped)))
        
        marriedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(marriedTapped)))
        singleButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(singleTapped)))
        separatedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(separatedTapped)))
        widowedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(widowedTapped)))
    }

    private func setupView(){
        
        addSubview(titleheader)
        addSubview(descText)
        addSubview(genderText)
        addSubview(ageText)
        addSubview(occupationText)
        addSubview(statusText)
        addSubview(genderView)
        
        genderView.addSubview(singleButton)
        singleButton.addSubview(singleStatusSelectView)
        genderView.addSubview(separatedButton)
        separatedButton.addSubview(separateStatusSelectView)
        genderView.addSubview(marriedButton)
        marriedButton.addSubview(marriedStatusSelectView)
        genderView.addSubview(widowedButton)
        widowedButton.addSubview(widowedStatusSelectView)
        
        addSubview(maleView)
        maleView.addSubview(maleImage)
        maleView.addSubview(maleText)
        addSubview(femaleView)
        femaleView.addSubview(femaleImage)
        femaleView.addSubview(femaleText)

        addSubview(pickerView)
        addSubview(dropView)
        dropView.addSubview(dropSelectedText)
        dropView.addSubview(dropImage)
    }

    override func setupViews() {
        super.setupViews()

        backgroundColor = UIColor.Theme(alpha: 1.0)
        //set values for age group picker
        pickerView.delegate = self
        pickerView.dataSource = self
        //set opacity for gender picker
        maleView.alpha = 0.5
        femaleView.alpha = 0.5

        setupGestures()
        setupView()
        setupConstraints()
    }
}
