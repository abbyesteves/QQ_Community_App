//
//  SurveyPage2Cell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 28/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class SurveySelect: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    static var select = ""
    
    var surveyCategory : [SurveyCategory] = {
        var category1 = SurveyCategory()
        category1.thumbnailImage = "ic_survey_community"
        category1.title = "General"
        category1.type = "general"
        category1.subtitle = "General Community Survey"
        
        var category2 = SurveyCategory()
        category2.thumbnailImage = "ic_survey_senior"
        category2.title = "Senior Citizens"
        category2.type = "senior"
        category2.subtitle = "Survey Questions for the eldery"
        
        var category3 = SurveyCategory()
        category3.thumbnailImage = "ic_survey_youth"
        category3.title = "Youth"
        category3.type = "youth"
        category3.subtitle = "Survey Questions for the youth"
        
        var category4 = SurveyCategory()
        category4.thumbnailImage = "ic_survey_working"
        category4.title = "Working Class"
        category4.type = "working"
        category4.subtitle = "Survey Questions for the working class"
        
        var category5 = SurveyCategory()
        category5.thumbnailImage = "ic_survey_single"
        category5.title = "Single Parents"
        category5.type = "singleParents"
        category5.subtitle = "Survey Questions for the single parents"
        
        return [category1, category2, category3, category4, category5]
    }()
    
//    var surveyCategory : [SurveyCategory] = {
//        var category1 = SurveyCategory()
//        category1.thumbnailImage = "ic_survey_community"
//        category1.title = "General"
//        category1.subtitle = "General Community Survey"
//
//        var category2 = SurveyCategory()
//        category2.thumbnailImage = "ic_survey_senior"
//        category2.title = "Senior Citizens"
//        category2.subtitle = "Survey Questions for the eldery"
//
//        var category3 = SurveyCategory()
//        category3.thumbnailImage = "ic_survey_youth"
//        category3.title = "Youth"
//        category3.subtitle = "Survey Questions for the youth"
//
//        var category4 = SurveyCategory()
//        category4.thumbnailImage = "ic_survey_working"
//        category4.title = "Working Class"
//        category4.subtitle = "Survey Questions for the working class"
//
//        var category5 = SurveyCategory()
//        category5.thumbnailImage = "ic_survey_single"
//        category5.title = "Single Parents"
//        category5.subtitle = "Survey Questions for the single parents"
//
//        var category6 = SurveyCategory()
//        category6.thumbnailImage = "ic_survey_community"
//        category6.title = "General"
//        category6.subtitle = "General Community Survey"
//
//        var category7 = SurveyCategory()
//        category7.thumbnailImage = "ic_survey_senior"
//        category7.title = "Senior Citizens"
//        category7.subtitle = "Survey Questions for the eldery"
//
//        var category8 = SurveyCategory()
//        category8.thumbnailImage = "ic_survey_youth"
//        category8.title = "Youth"
//        category8.subtitle = "Survey Questions for the youth"
//
//        var category9 = SurveyCategory()
//        category9.thumbnailImage = "ic_survey_working"
//        category9.title = "Working Class"
//        category9.subtitle = "Survey Questions for the working class"
//
//        var category10 = SurveyCategory()
//        category10.thumbnailImage = "ic_survey_single"
//        category10.title = "Single Parents"
//        category10.subtitle = "Survey Questions for the single parents"
//
//        return [category1, category2, category3, category4, category5, category6, category7, category8, category9, category10]
//    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let bannerHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let bgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        view.layer.cornerRadius = 15
        return view
    }()
    
    let titleHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(20)
        label.text = "Survey"
        return label
    }()
    
    lazy var surveyController: SurveyController = {
        let launcher = SurveyController()
        return launcher
    }()
    

    private func setupView(){
        addSubview(collectionView)
        collectionView.addSubview(titleHeader)
        insertSubview(bannerHeader, belowSubview: collectionView)
        insertSubview(bgView, belowSubview: collectionView)
        
    }
    
    private func setupConstraints() {
        let height = (surveyCategory.count*100)+(surveyCategory.count*5)
        collectionView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height-50)
        addConstraintsFormat(format: "H:|-20-[v0]|", views: titleHeader)
        addConstraintsFormat(format: "V:|-(-100)-[v0(120)]", views: titleHeader)
        addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        addConstraintsFormat(format: "V:|[v0(200)]|", views: bannerHeader)
        
        addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: bgView)
        addConstraintsFormat(format: "V:|-60-[v0(\(height+15))]|", views: bgView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return surveyCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SurveyCategoryCell
        cell.backgroundColor = UIColor.white
        cell.surveyCategory = surveyCategory[indexPath.item]
        if (cell.selectedBackgroundView != nil) {
            cell.backgroundColor = UIColor.lightGray
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width-50, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = surveyCategory[indexPath.item]
//        print(data.title, data.subtitle, surveyController.pageControl.currentPage)
        SurveySelect.select = data.type!
//        DispatchQueue.main.async(execute: {
//            let index = self.surveyController.pageControl.currentPage + 1
//            self.surveyController.pageControl.currentPage = index
//            let indexPath = IndexPath(item: 2, section: 0)
//            self.surveyController.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//        })
    }
    
    func ifSelected() -> String{
        return SurveySelect.select
    }
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.Background(alpha: 1.0)
        
        setupView()
        setupConstraints()
        
        collectionView.scrollsToTop = true
        collectionView.contentInset = UIEdgeInsetsMake(70, 0, 10, 0)
//        collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 50, 0)
        collectionView.backgroundColor = UIColor.rgba(red: 231, green: 231, blue: 243, alpha: 0)
        collectionView.register(SurveyCategoryCell.self, forCellWithReuseIdentifier: cellId)
    }
}
