//
//  SurveyPage3Cell.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 28/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class SurveyQuestions: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    var catergory = String()
    let cellId = "cellId"
    static var categorySelected = "singleParent"
    let experience = UIView()
    let terrible = UIView()
    let bad = UIView()
    let okay = UIView()
    let good = UIView()
    let great = UIView()
    let selectedView = UIView()
    lazy var surveySelect: SurveySelect = {
        let launcher = SurveySelect()
        return launcher
    }()
    
    let feelLabel: UITextView = {
        let textView = UITextView()
        textView.text = "How would you rate our municipal government?"
        textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0)
        textView.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1.0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.textAlignment = .center
        textView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        return textView
    }()
    
//    var categorySelected = self.surveySelect.ifSelected()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    // Images

    let terribleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_terrible")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        return imageView
    }()
    
    let badImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_bad")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        return imageView
    }()
    
    let okayImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_okay")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        return imageView
    }()
    
    let goodImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_good")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        return imageView
    }()
    
    let greatImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_great")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        return imageView
    }()
    
    let selectedImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_terrible")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 235, green: 124, blue: 104, alpha: 1.0)
        return imageView
    }()
    //labels
    let terribleText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "Terrible"
        return label
    }()
    
    let badText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "Bad"
        return label
    }()
    
    let okayText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "Okay"
        return label
    }()
    
    let goodText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "Good"
        return label
    }()
    
    let greatText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "Great"
        return label
    }()
    
    let selectedText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    var general: [Survey] = {
        var survey1 = Survey()
        survey1.question = "Is there anything the current Municipal Government can change to improve itself? If so, please explain below:"
        
        var survey2 = Survey()
        survey2.question = "We would like to know if you have any comments about our current Municipal Government that you think they should be aware of. If so, please explain below:"
        
        var survey3 = Survey()
        survey3.question = "Do you have any project suggestion to our Municipal Government If so, please explain below:"
        
        return [survey1, survey2, survey3]
    }()
    
    var senior: [Survey] = {
        var survey1 = Survey()
        survey1.question = "What problems, if any, do you experience with the home or community where you live?"
        
        var survey2 = Survey()
        survey2.question = "Are there any special programs you would like to see the Governmant develop for older members of the community?"
        
        var survey3 = Survey()
        survey3.question = "What issues do you feel older adults in your congregation struggle with the most?"
        
        return [survey1, survey2, survey3]
    }()
    
    var youth: [Survey] = {
        var survey1 = Survey()
        survey1.question = "As a youth, what problems, if any, do you experience with the home or community where you live?"
        
        var survey2 = Survey()
        survey2.question =  "Do you have any youth project suggestion to our Municipal Government If so, please explain below:"
        
        return [survey1, survey2]
    }()
    
    var working: [Survey] = {
        var survey1 = Survey()
        survey1.question = "How  do you think the Government could help parents balance work and family life?"
        
        var survey2 = Survey()
        survey2.question = "What problems, if any, do you experience at work?"
        
        return [survey1, survey2]
    }()
    
    var singleParent: [Survey] = {
        var survey1 = Survey()
        survey1.question = "How  do you think the Government could help parents balance work and family life?"
        
        var survey2 = Survey()
        survey2.question = "What are or were the most significant difficulties you have experienced as a single-parent?"
        
        var survey3 = Survey()
        survey3.question = "Do you experience some challenges with regard to your psychological and emotional needs? If yes, what are those?"
        
        var survey4 = Survey()
        survey4.question = "What do you think are the things that a single parent can do to overcome this problems that may affect the children involved?"
        
        return [survey1, survey2, survey3, survey4]
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if(SurveyQuestions.categorySelected == "general"){
            count = general.count
        } else if (SurveyQuestions.categorySelected == "senior"){
            count = senior.count
        } else if (SurveyQuestions.categorySelected == "youth"){
            count = youth.count
        } else if (SurveyQuestions.categorySelected == "working"){
            count = working.count
        } else if (SurveyQuestions.categorySelected == "singleParent"){
             count = singleParent.count
        }
        return count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SurveyCell
//        cell.backgroundColor = UIColor.gray
        if(SurveyQuestions.categorySelected == "general"){
            cell.survey = general[indexPath.item]
        } else if (SurveyQuestions.categorySelected == "senior"){
            cell.survey = senior[indexPath.item]
        } else if (SurveyQuestions.categorySelected == "youth"){
            cell.survey = youth[indexPath.item]
        } else if (SurveyQuestions.categorySelected == "working"){
            cell.survey = working[indexPath.item]
        } else if (SurveyQuestions.categorySelected == "singleParent"){
            cell.survey = singleParent[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var question = String()
        if(SurveyQuestions.categorySelected == "general"){
            question = general[indexPath.item].question!
        } else if (SurveyQuestions.categorySelected == "senior"){
            question = senior[indexPath.item].question!
        } else if (SurveyQuestions.categorySelected == "youth"){
            question = youth[indexPath.item].question!
        } else if (SurveyQuestions.categorySelected == "working"){
            question = working[indexPath.item].question!
        } else if (SurveyQuestions.categorySelected == "singleParent"){
            question = singleParent[indexPath.item].question!
        }
        let heightName = heightEstimation(text: question, width: frame.width-20, size: 7.5, defaultHeight : 5)
       return CGSize(width: frame.width, height: 130+heightName+20)
    }
    
    private func setupView(){
        experience.backgroundColor = UIColor.rgba(red: 216, green: 216, blue: 216, alpha: 1.0)
        selectedView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        terrible.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        bad.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        okay.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        good.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        great.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        
        addSubview(collectionView)
        collectionView.addSubview(experience)
        collectionView.addSubview(terrible)
        terrible.addSubview(terribleImage)
        terrible.addSubview(terribleText)
        collectionView.addSubview(bad)
        bad.addSubview(badImage)
        bad.addSubview(badText)
        collectionView.addSubview(okay)
        okay.addSubview(okayImage)
        okay.addSubview(okayText)
        collectionView.addSubview(good)
        good.addSubview(goodImage)
        good.addSubview(goodText)
        collectionView.addSubview(great)
        great.addSubview(greatText)
        great.addSubview(greatImage)
        experience.addSubview(selectedView)
        selectedView.addSubview(selectedImage)
        selectedView.addSubview(selectedText)
        experience.addSubview(feelLabel)
//        insertSubview(experience, belowSubview: collectionView)
    }
    
    private func setupConstraints() {
        collectionView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        let y = collectionView.frame.height+(frame.height/3)
        experience.frame = CGRect(x: 0, y: y, width: frame.width, height: 3)
        let goodY = experience.frame.width/3
        terrible.frame = CGRect(x: 20, y: y-25, width: 50, height: 50)
        okay.frame = CGRect(x: experience.frame.width/2-25, y: y-25, width: 50, height: 50)
        bad.frame = CGRect(x: okay.frame.maxX/2-15, y: y-25, width: 50, height: 50)
        
        great.frame = CGRect(x: experience.frame.width-70, y: y-25, width: 50, height: 50)
        good.frame = CGRect(x: okay.frame.maxX+(goodY/2)-35, y: y-25, width: 50, height: 50)
        
        selectedView.frame = CGRect(x: 20, y: experience.frame.height/2-30, width: 0, height: 0)
        
        addConstraintsFormat(format: "H:|[v0]|", views: feelLabel)
        addConstraintsFormat(format: "V:|-(-50)-[v0(20)]|", views: feelLabel)
        addConstraintsFormat(format: "H:|[v0]|", views: terribleImage)
        addConstraintsFormat(format: "V:|[v0]|", views: terribleImage)
        addConstraintsFormat(format: "H:|[v0]|", views: badImage)
        addConstraintsFormat(format: "V:|[v0]|", views: badImage)
        addConstraintsFormat(format: "H:|[v0]|", views: okayImage)
        addConstraintsFormat(format: "V:|[v0]|", views: okayImage)
        addConstraintsFormat(format: "H:|[v0]|", views: goodImage)
        addConstraintsFormat(format: "V:|[v0]|", views: goodImage)
        addConstraintsFormat(format: "H:|[v0]|", views: greatImage)
        addConstraintsFormat(format: "V:|[v0]|", views: greatImage)
        addConstraintsFormat(format: "H:|[v0]|", views: selectedImage)
        addConstraintsFormat(format: "V:|[v0]|", views: selectedImage)
        
        addConstraintsFormat(format: "H:|[v0]|", views: terribleText)
        addConstraintsFormat(format: "V:|-(60)-[v0(20)]|", views: terribleText)
        addConstraintsFormat(format: "H:|[v0]|", views: badText)
        addConstraintsFormat(format: "V:|-(60)-[v0(20)]|", views: badText)
        addConstraintsFormat(format: "H:|[v0]|", views: okayText)
        addConstraintsFormat(format: "V:|-(60)-[v0(20)]|", views: okayText)
        addConstraintsFormat(format: "H:|[v0]|", views: goodText)
        addConstraintsFormat(format: "V:|-(60)-[v0(20)]|", views: goodText)
        addConstraintsFormat(format: "H:|[v0]|", views: greatText)
        addConstraintsFormat(format: "V:|-(60)-[v0(20)]|", views: greatText)
        addConstraintsFormat(format: "H:|[v0]|", views: selectedText)
        addConstraintsFormat(format: "V:|-(70)-[v0(20)]|", views: selectedText)
    }
    
    private func setupGestures() {
        terrible.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(terribleTapped)))
        bad.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(badTapped)))
        okay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(okayTapped)))
        good.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goodTapped)))
        great.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(greatTapped)))
    }
    
    @objc private func terribleTapped() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.terrible.alpha = 0
            self.bad.alpha = 1
            self.okay.alpha = 1
            self.good.alpha = 1
            self.great.alpha = 1
        }, completion:  nil)
        self.changeSelect(selected: "Terrible", color: UIColor.rgba(red: 235, green: 124, blue: 104, alpha: 1.0), x: 20)
    }
    
    @objc private func badTapped() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.terrible.alpha = 1
            self.bad.alpha = 0
            self.okay.alpha = 1
            self.good.alpha = 1
            self.great.alpha = 1
        }, completion:  nil)
        self.changeSelect(selected: "Bad", color: UIColor.rgba(red: 235, green: 178, blue: 104, alpha: 1.0), x : okay.frame.maxX/2-15)
    }
    
    @objc private func okayTapped() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.terrible.alpha = 1
            self.bad.alpha = 1
            self.okay.alpha = 0
            self.good.alpha = 1
            self.great.alpha = 1
        }, completion:  nil)
        self.changeSelect(selected: "Okay", color: UIColor.rgba(red: 235, green: 207, blue: 104, alpha: 1.0), x : experience.frame.width/2-25)
    }
    
    @objc private func goodTapped() {
        let goodY = experience.frame.width/3
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.terrible.alpha = 1
            self.bad.alpha = 1
            self.okay.alpha = 1
            self.good.alpha = 0
            self.great.alpha = 1
        }, completion:  nil)
        self.changeSelect(selected: "Good", color: UIColor.rgba(red: 219, green: 222, blue: 136, alpha: 1.0), x : okay.frame.maxX+(goodY/2)-35)
    }
    
    @objc private func greatTapped() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.terrible.alpha = 1
            self.bad.alpha = 1
            self.okay.alpha = 1
            self.good.alpha = 1
            self.great.alpha = 0
        }, completion:  nil)
        self.changeSelect(selected: "Great", color: UIColor.rgba(red: 118, green: 165, blue: 76, alpha: 1.0), x : experience.frame.width-70)
    }
    
    private func changeSelect(selected : String, color : UIColor, x : CGFloat) {
        let image = selected.lowercased()
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.selectedView.frame = CGRect(x: x-5, y: self.experience.frame.height/2-30, width: 60, height: 60)
                self.selectedImage.image = UIImage(named: "ic_\(image)")?.withRenderingMode(.alwaysTemplate)as UIImage?
                self.selectedImage.tintColor = color
                self.selectedText.text = selected
                self.selectedText.textColor = UIColor.darkGray
                self.selectedText.font = self.selectedText.font.withSize(13)
            }, completion:  nil)
        })
    }
    
    private func load(completion: @escaping () -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now()) {
//            SurveyQuestions.categorySelected = self.surveySelect.ifSelected()
            completion()
        }
    }
    override func setupViews() {
        super.setupViews()
        
        load {
            DispatchQueue.main.async(execute: {
                self.collectionView.reloadData()
            })
        }
        
//
//        print("survey : ", surveySelect.ifSelected())
//        print("CATEGORY ", catergory)
//        print("categorySelected " , SurveyQuestions.categorySelected)
        
        
        setupView()
        setupConstraints()
        setupGestures()
        
        collectionView.scrollsToTop = true
        collectionView.contentInset = UIEdgeInsetsMake(10, 0, (frame.height/3), 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 50, 0)
        collectionView.backgroundColor = UIColor.rgba(red: 238, green: 238, blue: 242, alpha: 1.0)
        collectionView.register(SurveyCell.self, forCellWithReuseIdentifier: cellId)
        
        DispatchQueue.main.async(execute: {
            self.collectionView.reloadData()
        })
        
    }
}
