//
//  SettingsLauncher.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 08/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class MenuLauncher: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let blackView = UIView()
//    let blurView = UIBlurEffect(style: UIBlurEffectStyle.dark)
    let blurEffectView = UIVisualEffectView(effect: (UIBlurEffect(style: UIBlurEffectStyle.dark)))
    
    lazy var menuView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let menuCellId = "menuCellId"
    let menuLabels: [Menu] = {
        var menu1 = Menu()
        menu1.icon = "side_nav_bar"
        menu1.label = "header"
        
        var menu2 = Menu()
        menu2.icon = "ic_action_news"
        menu2.label = "Bocaue Cares"
        
        var menu3 = Menu()
        menu3.icon = "ic_business"
        menu3.label = "Business Centre"
        
        var menu4 = Menu()
        menu4.icon = "ic_directions_transit"
        menu4.label = "Transportation"
        
        var menu5 = Menu()
        menu5.icon = "ic_stars"
        menu5.label = "Badges"
        
        var menu6 = Menu()
        menu6.icon = ""
        menu6.label = ""
        
        var menu7 = Menu()
        menu7.icon = ""
        menu7.label = "Government"
        
        var menu8 = Menu()
        menu8.icon = "ic_survey"
        menu8.label = "Survey"
        
        var menu9 = Menu()
        menu9.icon = "ic_forms"
        menu9.label = "Printable Forms"
        
        var menu10 = Menu()
        menu10.icon = "ic_pin_drop"
        menu10.label = "Bocaue Traffic"
        
        var menu11 = Menu()
        menu11.icon = "ic_info_filled"
        menu11.label = "About Bocaue"
        
        var menu12 = Menu()
        menu12.icon = ""
        menu12.label = ""
        
        var menu13 = Menu()
        menu13.icon = ""
        menu13.label = "Options"
        
        var menu14 = Menu()
        menu14.icon = "ic_power"
        menu14.label = "Sign in"
        return [menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8, menu9, menu10, menu11, menu12, menu13, menu14]
    }()

    
    var newsController: NewsViewController?
    var businessCenterController : BusinessCenterController?//showController
    
    
    @objc func showMenu(){
        //show menu
        //show dim bg when options are clicked
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            window.addSubview(blackView)
            //add view for the options
            window.addSubview(menuView)
            let width: CGFloat = 260
            let x = -width
            menuView.frame = CGRect(x: x, y: 0, width: width, height: window.frame.height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            //animation
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                self.menuView.frame = CGRect(x: 0, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
                
            }, completion:  nil)
        }
    }
    // function to dismiss menu and option view
    // also check if which controller to load
    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(x: -window.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
            }
            
        }, completion: { (Bool) in
            
            
        })
    }
    
    @objc func menuDismiss(menu: Menu){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(x: -window.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
            }
            
        }, completion: { (Bool) in
            
            self.newsController?.showController(menu: menu)
//            self.newsController?.showNews()
            
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuLabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! SideMenuCell
        let menuLabel = menuLabels[indexPath.item]
        cell.menu = menuLabel
        if(indexPath.item == 5 || indexPath.item == 11){
            cell.backgroundColor = UIColor.rgba(red: 232, green: 232, blue: 232, alpha: 1.0)
        } else if(indexPath.item == 0){
            cell.backgroundColor = UIColor.rgba(red: 12, green: 47, blue: 67, alpha: 1.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.item == 0){
            return CGSize(width: menuView.frame.width, height: 130)
        } else if(indexPath.item == 5 || indexPath.item == 11){
            return CGSize(width: menuView.frame.width, height: 1.5)
        }
        return CGSize(width: menuView.frame.width, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menu = menuLabels[indexPath.item]
        if((menu.label == "Government") || (menu.label == "Options") || (menu.label == "")){
//            bgDismiss()
        } else {
            menuDismiss(menu: menu)
        }
        
    }

    
    override init() {
        super.init()
        
        menuView.register(SideMenuCell.self, forCellWithReuseIdentifier: menuCellId)
        
    }
}
