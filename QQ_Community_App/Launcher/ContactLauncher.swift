//
//  FloatOption.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 22/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import MessageUI

class ContactLauncher: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    static var numberSelected = ""
    static var emailSelected = ""
    static var smsController = MFMessageComposeViewController()
    let sms = MFMessageComposeViewController()
    static var mailController = MFMailComposeViewController()
    let blackView = UIView()
    let blurEffectView = UIVisualEffectView(effect: (UIBlurEffect(style: UIBlurEffectStyle.dark)))
    
    lazy var menuView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let menuCellId = "menuCellId"
    let menuLabels: [Menu] = {
        var menu1 = Menu()
        menu1.icon = ""
        menu1.label = "Choose an action"
        
        var menu2 = Menu()
        menu2.icon = "ic_call"
        menu2.label = "Call"
        
        var menu3 = Menu()
        menu3.icon = "ic_text_message"
        menu3.label = "Text"
        
        var menu4 = Menu()
        menu4.icon = "ic_email"
        menu4.label = "Direct Message"
        
        return [menu1, menu2, menu3, menu4]
    }()
    
    var newsController: NewsViewController?
    
    
    @objc func showMenu(number: String, email : String){
        //show menu
        ContactLauncher.numberSelected = number
        ContactLauncher.emailSelected = email
        //show dim bg when options are clicked
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.isUserInteractionEnabled = true
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            window.addSubview(blackView)
            //add view for the options
            window.addSubview(menuView)
            menuView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: 200)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            //animation
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                self.menuView.frame = CGRect(x: 0, y: window.frame.height-200, width: window.frame.width, height: 200)
                
            }, completion: { (Bool) in
                
            })
        }
    }
    // function to dismiss menu and option view
    // also check if which controller to load
    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow {
                self.blackView.alpha = 0
                self.menuView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: 200)
            }
            
        }, completion: { (Bool) in
            
            
        })
    }
    
    func sms(contactNumber :String) {
//        let sms = MFMessageComposeViewController()
        
        let alertWindow : UIWindow = {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.windowLevel = UIWindowLevelAlert + 2;
            window.makeKeyAndVisible()
            return window
        }()
        
        if MFMessageComposeViewController.canSendText() {
            //                smsController = MFMessageComposeViewController()
            sms.body = ""
            sms.recipients = [contactNumber]
            sms.messageComposeDelegate = self
            
            alertWindow.rootViewController?.present(sms, animated: true, completion: nil)
            //                UIApplication.shared.keyWindow?.rootViewController?.present(ContactLauncher.smsController, animated: true, completion: nil)
            
        } else {
            print("Message option not available")
            UIApplication.shared.keyWindow?.alertView(title : "Message option not available", message : "")
        }
    }
    
    func call(contactNumber :String) {
        let contactNumber = contactNumber.replacingOccurrences(of: " ", with: "")
        if let url : NSURL = URL(string: "TEL://\(contactNumber)")! as NSURL {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            print("Call option not available")
            UIApplication.shared.keyWindow?.alertView(title : "Call option not available", message : "")
        }
    }
    
    @objc func menuDismiss(menu: Menu){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(x: -window.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
            }
            
        }, completion: { (Bool) in
            
//            self.newsController?.showController(menu: menu)
//            self.newsController?.showNews()
            
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuLabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! FloatMenuCell
        cell.menu = menuLabels[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: menuView.frame.width, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        //call
        if indexPath.item == 1 {
            let contactNumber = ContactLauncher.numberSelected.replacingOccurrences(of: " ", with: "")
            if let url : NSURL = URL(string: "TEL://\(contactNumber)")! as NSURL {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                print("Call option not available")
                UIApplication.shared.keyWindow?.alertView(title : "Call option not available", message : "")
            }
        
        //text
        } else if indexPath.item == 2 {
            if MFMessageComposeViewController.canSendText() {
//                smsController = MFMessageComposeViewController()
                ContactLauncher.smsController.body = ""
                ContactLauncher.smsController.recipients = [ContactLauncher.numberSelected]
                ContactLauncher.smsController.messageComposeDelegate = self
                
                alertWindow.rootViewController?.present(ContactLauncher.smsController, animated: true, completion: nil)
//                UIApplication.shared.keyWindow?.rootViewController?.present(ContactLauncher.smsController, animated: true, completion: nil)
                
            } else {
                print("Message option not available")
                UIApplication.shared.keyWindow?.alertView(title : "Message option not available", message : "")
            }
        //direct message
        } else if indexPath.item == 3 {
            if MFMailComposeViewController.canSendMail(){
//                mailController = MFMailComposeViewController()
                ContactLauncher.mailController.setSubject("")
                ContactLauncher.mailController.setMessageBody("", isHTML: false)
                ContactLauncher.mailController.setToRecipients([ContactLauncher.emailSelected])
                
                alertWindow.rootViewController?.present(ContactLauncher.mailController, animated: true, completion: nil)
                UIApplication.shared.keyWindow?.rootViewController?.present(ContactLauncher.mailController, animated: true, completion: nil)
            } else {
                print("Mail option not available")
                UIApplication.shared.keyWindow?.alertView(title : "Mail option not available", message : "")
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        switch result.rawValue {
//        case MFMailComposeResult.cancelled.rawValue :
//            print("Cancelled Mail")
//
//        case MFMailComposeResult.failed.rawValue :
//            print("Failed Mail")
//
//        case MFMailComposeResult.saved.rawValue :
//            print("Saved Mail")
//
//        case MFMailComposeResult.sent.rawValue :
//            print("Sent Mail")

//        default: break
//
//        }
        
        ContactLauncher.mailController.dismiss(animated: true, completion: {
            ContactLauncher.mailController.removeFromParentViewController()
        })
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        ContactLauncher.smsController.dismiss(animated: true, completion: {
            ContactLauncher.smsController.removeFromParentViewController()
        })
        
        sms.dismiss(animated: true, completion: {
            self.sms.removeFromParentViewController()
        })
        
    }

    override init() {
        super.init()
        
        menuView.register(FloatMenuCell.self, forCellWithReuseIdentifier: menuCellId)
        
    }
}
