//
//  news.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class Menu: NSObject {
    var icon = String()
    var label = String()
    
//    init(icon: String, label: String) {
//        self.icon = icon
//        self.label = label
//    }
}

struct Routes: Decodable {
    var rows : [Rows]
}

struct Rows: Decodable {
    var elements : [Elements]
}

struct Elements: Decodable {
    var distance : Distance
    var duration : Duration
}

struct Distance: Decodable {
    var text : String
}

struct Duration: Decodable {
    var text : String
}

struct Headlines: Decodable {
    var status: String?
    var totalResults: Int?
    var articles : [Articles]
}

struct Articles: Decodable {
    var source : Source
    var title: String!
    var description: String!
    var url: String!
    var urlToImage : String!
    var publishedAt: String!
    var author: String!
}

struct Source : Decodable {
    var id : String!
    var name : String!
}

class BusinessFeed: NSObject {
    var thumbnailImage: String?
    var name: String?
    var contact: String?
    var category: String?
    var rating: CFloat?
    var about: String?
    var hours: String?
    var address: String?
}

class News: NSObject {
    var thumbnailImage: String?
    var title: String?
    var content: String?
    var date: String?
}

class Tweets: NSObject {
    var thumbnailImage: String?
    var userName: String?
    var name: String?
    var content: String?
    var date: String?
    var uploadedImage : String?
}

class Survey: NSObject {
    var question: String?
}

class SurveyCategory : NSObject {
    var thumbnailImage: String?
    var title: String?
    var subtitle: String?
    var type: String?
}


class Safety: NSObject {
    var thumbnailImage: String?
    var title: String?
    var content: String?
}

class Hospital: NSObject {
    var name: String?
    var address: String?
    var contact: String?
}

class Content: NSObject {
    var title: String?
    var body: String?
}

class Forms: NSObject {
    var title: String?
    var body: String?
    var url: String?
}


