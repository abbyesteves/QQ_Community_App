//
//  BadgesController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 27/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BadgesController: UICollectionViewController {
    
    let mainView = UIView()
    let backView = UIView()
    let popView = UIView()
    let blackView = UIView()
    let boarder = UIView()
    
    let businessView = UIView()
    let sharedView = UIView()
    let businessCentreView = UIView()
    let surveyAnswerView = UIView()
    let rateAppView = UIView()
    
    let popHeaderText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(18)
        label.textAlignment = .center
        return label
    }()
    
    let popDescText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 1)
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let popDescCurrentText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 1)
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let popDescPointsText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let popDescValueText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let popImgage: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let popBronze: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_badge_bronze")
        return imageView
    }()
    
    let popGold: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_badge_gold")
        return imageView
    }()
    
    let popSilver: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_badge_silver")
        return imageView
    }()
    
    let titleHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Badges"
        return label
    }()
    
    let infoImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_info")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let infoView: UIView = {
        let view = UIView()
        return view
    }()
    
    @objc func badgeTapped(){
        PopOver(header: "Add Business", image: "ic_badge_business")
    }
    
    @objc func shareTapped(){
        PopOver(header: "Share Community App", image: "ic_badge_invite")
    }
    
    @objc func rateTapped(){
        PopOver(header: "Rate Business", image: "ic_badge_rating")
    }
    
    @objc func surveyTapped(){
        PopOver(header: "Take Survey", image: "ic_badge_survey")
    }
    
    @objc func appStoreTapped(){
        PopOver(header: "Rate on Apps Store", image: "ic_apps_store")
    }
    
    @objc func infoTapped(){
        PopOver(header: "Badges", image: "ic_badge")
    }
    
    func PopOver(header: String, image: String){
        if let window = UIApplication.shared.keyWindow {
            boarder.backgroundColor = UIColor.lightGray
            popView.backgroundColor = UIColor.white
            popView.layer.cornerRadius = 5
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            popHeaderText.text = header
            popImgage.image = UIImage(named: image)
            popDescText.text = "Collect badges through completed activities"
            popDescValueText.text = "0"
            let text:String = "Current point/s: " + popDescValueText.text!
            popDescCurrentText.text = text
            popDescPointsText.text = "20 pts    10 pts    5 pts"
           
            
            window.addSubview(blackView)
            window.addSubview(popView)
            popView.addSubview(popHeaderText)
            popView.addSubview(popDescText)
            popView.addSubview(popDescCurrentText)
            popView.addSubview(popDescValueText)
            popView.addSubview(popDescPointsText)
            popView.addSubview(popImgage)
            popView.addSubview(boarder)
            popView.addSubview(popGold)
            popView.addSubview(popSilver)
            popView.addSubview(popBronze)
            
            blackView.alpha = 0
            popHeaderText.alpha = 0
            popDescText.alpha = 0
            popDescPointsText.alpha = 0
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            popView.frame = CGRect(x: (window.frame.width)/2, y: (window.frame.height)/2, width: 0, height: 0)
            
            popView.addConstraintsFormat(format: "H:|[v0]|", views: popHeaderText)
            popView.addConstraintsFormat(format: "V:|-20-[v0(20)]|", views: popHeaderText)
            popView.addConstraintsFormat(format: "H:|[v0]|", views: popDescText)
            popView.addConstraintsFormat(format: "V:|-50-[v0(20)]|", views: popDescText)
            popView.addConstraintsFormat(format: "H:|[v0]|", views: popDescCurrentText)
            popView.addConstraintsFormat(format: "V:|-70-[v0(20)]|", views: popDescCurrentText)
            popView.addConstraintsFormat(format: "H:|[v0]|", views: popDescPointsText)
            popView.addConstraintsFormat(format: "V:|-230-[v0(20)]|", views: popDescPointsText)
            
            
            popImgage.frame = CGRect(x: (popView.frame.width/2), y: (popView.frame.height/2)-20, width: 0, height: 0)
            
            popGold.frame = CGRect(x: (window.frame.width)/2-120, y: (popView.frame.height), width: 0, height: 0)
            popSilver.frame = CGRect(x: window.frame.width/2-70, y: (popView.frame.height), width: 0, height: 0)
            popBronze.frame = CGRect(x: (window.frame.width)/2-20, y: (popView.frame.height), width: 0, height: 0)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                
                self.popView.frame = CGRect(x: (window.frame.width-320)/2, y: (window.frame.height-270)/2, width: 320, height: 270)
                self.popImgage.frame = CGRect(x: (self.popView.frame.width-60)/2, y: (self.popView.frame.height-60)/2, width: 60, height: 60)
                self.popGold.frame = CGRect(x: (window.frame.width)/2-120, y: (self.popView.frame.height)-90, width: 50, height: 50)
                self.popSilver.frame = CGRect(x: self.view.frame.width/2-70, y: (self.popView.frame.height)-90, width: 50, height: 50)
                self.popBronze.frame = CGRect(x: (window.frame.width)/2-20, y: (self.popView.frame.height)-90, width: 50, height: 50)
                self.blackView.alpha = 1
                self.popHeaderText.alpha = 1
                self.popDescText.alpha = 1
                self.popDescPointsText.alpha = 1
                
                if(header == "Badges"){
                    self.popDescValueText.removeFromSuperview()
                    self.popDescCurrentText.removeFromSuperview()
                    self.popDescPointsText.removeFromSuperview()
                    self.popGold.removeFromSuperview()
                    self.popSilver.removeFromSuperview()
                    self.popBronze.removeFromSuperview()
                }
                
            }, completion:  nil)
        }
    }
    
    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            self.popHeaderText.alpha = 0
            self.popDescText.alpha = 0
            self.popDescPointsText.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.popView.frame = CGRect(x: (window.frame.width)/2, y: (window.frame.height)/2, width: 0, height: 0)
                self.popImgage.frame = CGRect(x: (self.popView.frame.width)/2, y: (self.popView.frame.height)/2, width: 0, height: 0)
                self.popGold.frame = CGRect(x: self.popGold.frame.maxX-50, y: self.popGold.frame.maxY-50, width: 0, height: 0)
                self.popSilver.frame = CGRect(x: self.popSilver.frame.maxX-50, y: self.popSilver.frame.maxY-50, width: 0, height: 0)
                self.popBronze.frame = CGRect(x: self.popBronze.frame.maxX-50, y: self.popBronze.frame.maxY-50, width: 0, height: 0)
            }
            
        }, completion:  nil)
    }
    
    private func setupViews() {
        view.addSubview(titleHeader)
        view.addSubview(infoView)
        infoView.addSubview(infoImage)
        view.addSubview(businessView)
        view.addSubview(sharedView)
        view.addSubview(businessCentreView)
        view.addSubview(surveyAnswerView)
        view.addSubview(rateAppView)
    }
    
    private func setupConstraints() {
        let y = (view.frame.height/2)
        
        businessView.frame = CGRect(x: 0, y: y-40-20-80-20-80, width: view.frame.width, height: 80)
        sharedView.frame = CGRect(x: 0, y: y-40-20-80, width: view.frame.width, height: 80)
        businessCentreView.frame = CGRect(x: 0, y: y-40, width: view.frame.width, height: 80)
        surveyAnswerView.frame = CGRect(x: 0, y: y+60, width: view.frame.width, height: 80)
        rateAppView.frame = CGRect(x: 0, y: y+60+20+80, width: view.frame.width, height: 80)
        
        view.addConstraintsFormat(format: "H:|-20-[v0][v1(30)]-20-|", views: titleHeader, infoView)
        view.addConstraintsFormat(format: "V:|-20-[v0(30)]|", views: titleHeader)
        view.addConstraintsFormat(format: "V:|-20-[v0(30)]-20-|", views: infoView)
        view.addConstraintsFormat(format: "H:|[v0]|", views: infoImage)
        view.addConstraintsFormat(format: "V:|[v0]|", views: infoImage)
        
        
        //business layout
        getConstraint(mainView: businessView, badgeImage: "ic_badge_business", labelText: "Add a business on Baliwag Business Center")
        //
        
        //share layout
        getConstraint(mainView: sharedView, badgeImage: "ic_badge_invite", labelText: "Share QQ Community App to friends")
        //
        
        //rate layout
        getConstraint(mainView: businessCentreView, badgeImage: "ic_badge_rating", labelText: "Rate businesses on Baliuag Business Centre")
        //

        //survey layout
        getConstraint(mainView: surveyAnswerView, badgeImage: "ic_badge_survey", labelText: "Answer survey forms")
        //

        //appStore layout
        getConstraint(mainView: rateAppView, badgeImage: "ic_apps_store", labelText: "Rate Community App on Apps Store")
        //
        
        businessView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(badgeTapped)))
        sharedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shareTapped)))
        businessCentreView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rateTapped)))
        surveyAnswerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(surveyTapped)))
        rateAppView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(appStoreTapped)))
        infoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(infoTapped)))
    }
    
    private func getConstraint(mainView : UIView, badgeImage : String, labelText : String) {
 
        let badgeView: UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 5
            return view
        }()
        
        let badge: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: badgeImage)
            return imageView
        }()
        
        let bronze: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_badge_bronze")
            return imageView
        }()
        
        let gold: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_badge_gold")
            return imageView
        }()
        
        let silver: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "ic_badge_silver")
            return imageView
        }()
        
        let label: UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
            label.font = label.font.withSize(12)
            label.textAlignment = .center
            label.text = labelText
            return label
        }()
        
        view.grayScale(image : bronze)
        view.grayScale(image : gold)
        view.grayScale(image : silver)
        
        mainView.addSubview(badgeView)
        mainView.addSubview(badge)
        mainView.addSubview(label)
        badgeView.addSubview(gold)
        badgeView.addSubview(silver)
        badgeView.addSubview(bronze)
        
        badge.frame = CGRect(x: (mainView.frame.width/2)-(mainView.frame.height-20)-((mainView.frame.width/2)/2), y: 0, width: mainView.frame.height-20, height: mainView.frame.height-20)
        badgeView.frame = CGRect(x:  (mainView.frame.width/2)-((mainView.frame.width/2)/2)+(badge.frame.width/2), y:0, width: mainView.frame.width/2+20, height: mainView.frame.height-20)
        label.frame = CGRect(x: 0, y: mainView.frame.height-20, width : mainView.frame.width, height: 20)
        gold.frame = CGRect(x: 5, y: 8.75, width: 45, height: 45)
        silver.frame = CGRect(x: (badgeView.frame.width/2)-22.5, y: 8.75, width: 45, height: 45)
        bronze.frame = CGRect(x: badgeView.frame.width-50, y: 8.75, width: 45, height: 45)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.Theme(alpha: 1.0)
        
        setupViews()
        setupConstraints()
    }
}
