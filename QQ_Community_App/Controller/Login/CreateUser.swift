//
//  CreateUser.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 13/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class CreateUser: BaseCell, UITextFieldDelegate {
    
    let backView = UIView()
    let mainView = UIView()
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "back_narrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let createNewView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "signup")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let googleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "google_icon")
        return imageView;
    }()
    
    let titleHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Sign Up"
        return label
    }()
    
    let signUpNowButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
        button.setTitle("SIGN UP NOW", for: .normal)
        return button
    }()
    //
    
    let firstNameLabel : UILabel = {
        let label = UILabel()
        label.text = "First Name"
        label.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        label.font = label.font.withSize(10)
        return label
    }()
    
    let firstNameBoarder : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    let firstNameText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
//        var placeholder = NSMutableAttributedString()
//        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "First Name", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.8)]))
//        text.setLeftPadding(space: 8)
//        text.attributedPlaceholder = placeholder
        return text
    }()
    //
    let lastNameBoarder : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let LastNameLabel : UILabel = {
        let label = UILabel()
        label.text = "Last Name"
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        return label
    }()
    
    let lastNameText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
        return text
    }()
    //
    let emailBoarder : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let emailLabel : UILabel = {
        let label = UILabel()
        label.text = "E-mail Address"
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        return label
    }()
    
    let emailText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
        return text
    }()
    //
    let passwordBoarder : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let passwordLabel : UILabel = {
        let label = UILabel()
        label.text = "Password"
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        return label
    }()
    
    let passwordText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
        text.isSecureTextEntry = true
        return text
    }()
    //
    let repasswordBoarder : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let repasswordLabel : UILabel = {
        let label = UILabel()
        label.text = "Re-enter Password"
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        return label
    }()
    
    let repasswordText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
        text.isSecureTextEntry = true
        return text
    }()
    
    func openCell() {
        resignResponder()
        resetAttributes()
        firstNameLabel.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        firstNameText.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        if let window = UIApplication.shared.keyWindow {
            firstNameBoarder.backgroundColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            mainView.backgroundColor = UIColor.white
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeCell)))
            signUpNowButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signUpClicked)))
            firstNameText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(firstNameTapped)))
            lastNameText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(lastNameTapped)))
            emailText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emailTapped)))
            passwordText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(passwordTapped)))
            repasswordText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(repasswordTapped)))
            
            window.addSubview(mainView)
            mainView.addSubview(createNewView)
            mainView.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(titleHeader)
            mainView.addSubview(firstNameText)
            firstNameText.addSubview(firstNameLabel)
            firstNameText.addSubview(firstNameBoarder)
            mainView.addSubview(lastNameText)
            lastNameText.addSubview(LastNameLabel)
            lastNameText.addSubview(lastNameBoarder)
            mainView.addSubview(emailText)
            emailText.addSubview(emailLabel)
            emailText.addSubview(emailBoarder)
            mainView.addSubview(passwordText)
            passwordText.addSubview(passwordLabel)
            passwordText.addSubview(passwordBoarder)
            mainView.addSubview(repasswordText)
            repasswordText.addSubview(repasswordLabel)
            repasswordText.addSubview(repasswordBoarder)
            mainView.addSubview(signUpNowButton)
            
            firstNameLabel.frame = CGRect(x: 0, y: -15, width: window.frame.width-20, height: 20)
            LastNameLabel.frame = CGRect(x: 10, y: 5, width: window.frame.width-20, height: 20)
            emailLabel.frame = CGRect(x: 10, y: 5, width: window.frame.width-20, height: 20)
            passwordLabel.frame = CGRect(x: 10, y: 5, width: window.frame.width-20, height: 20)
            repasswordLabel.frame = CGRect(x: 10, y: 5, width: window.frame.width-20, height: 20)
            
            window.addConstraintsFormat(format: "H:|-10-[v0(30)]", views: backView)
            window.addConstraintsFormat(format: "V:|-40-[v0(30)]-40-[v1(30)]-30-[v2(35)]-25-[v3(35)]-25-[v4(35)]-25-[v5(35)]-25-[v6(35)]-60-[v7(50)]", views: backView, titleHeader, firstNameText, lastNameText, emailText, passwordText, repasswordText, signUpNowButton)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: titleHeader)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: firstNameText)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: lastNameText)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: emailText)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: passwordText)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: repasswordText)
            window.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: signUpNowButton)
            window.addConstraintsFormat(format: "H:|[v0(30)]|", views: backImage)
            window.addConstraintsFormat(format: "V:|[v0(30)]|", views: backImage)
            window.addConstraintsFormat(format: "H:|[v0]|", views: createNewView)
            window.addConstraintsFormat(format: "V:|[v0]|", views: createNewView)
            //
            window.addConstraintsFormat(format: "H:|[v0]|", views: firstNameBoarder)
            window.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: firstNameBoarder)
            window.addConstraintsFormat(format: "H:|[v0]|", views: lastNameBoarder)
            window.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: lastNameBoarder)
            window.addConstraintsFormat(format: "H:|[v0]|", views: emailBoarder)
            window.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: emailBoarder)
            window.addConstraintsFormat(format: "H:|[v0]|", views: passwordBoarder)
            window.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: passwordBoarder)
            window.addConstraintsFormat(format: "H:|[v0]|", views: repasswordBoarder)
            window.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: repasswordBoarder)
            
            mainView.frame = CGRect(x: window.frame.width, y: 0, width: window.frame.width, height: window.frame.height)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                
            }, completion: { (Bool) in })
        }
    }
    
    @objc func signUpClicked() {
        if firstNameText.text!.count == 0 {
            firstNameText.becomeFirstResponder()
            showToast(message: "Enter your First Name")
        } else if lastNameText.text!.count == 0 {
            lastNameText.becomeFirstResponder()
            showToast(message: "Enter your Last Name")
        } else if emailText.text!.count == 0 {
            emailText.becomeFirstResponder()
            showToast(message: "Enter your E-mail Address")
        } else if passwordText.text!.count == 0 {
            passwordText.becomeFirstResponder()
            showToast(message: "Enter your Password")
        } else if repasswordText.text!.count == 0{
            repasswordText.becomeFirstResponder()
            showToast(message: "Re-enter your Password")
        } else {
            showToast(message: "Sign up Successful")
        }
    }
    
    @objc func firstNameTapped() {
        selected(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
        
        attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
    }
    
    @objc func lastNameTapped() {
        selected(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        
        attribute(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
    }
    
    @objc func emailTapped() {
        selected(text: emailText, label: emailLabel, boarder: emailBoarder)
        
        attribute(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
        attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
    }
    
    @objc func passwordTapped() {
        selected(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        
        attribute(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
        attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
    }
    
    @objc func repasswordTapped() {
        selected(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
        
        attribute(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
        attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
    }
    
    private func attribute(text : UITextField, label: UILabel, boarder: UIView){
        text.resignFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow {
                text.textColor = UIColor.white
                label.textColor = UIColor.white
                boarder.backgroundColor = UIColor.white
                if text.text!.count == 0 {
                    label.font = label.font.withSize(12)
                    label.frame = CGRect(x: 10, y: 5, width: window.frame.width-20, height: 20)
                }
            }
        }, completion:  nil)
    }
    
    private func selected(text : UITextField, label: UILabel, boarder: UIView) {
        text.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow {
                label.font = label.font.withSize(10)
                label.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
                label.frame = CGRect(x: 0, y: -15, width: window.frame.width-20, height: 20)
                text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
                boarder.backgroundColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            }
        }, completion:  { (Bool) in })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameText {
            attribute(text: firstNameText, label: firstNameLabel, boarder: firstNameBoarder)
            selected(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        } else if textField == lastNameText {
            attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
            selected(text: emailText, label: emailLabel, boarder: emailBoarder)
        } else if textField == emailText {
            attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
            selected(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        } else if textField == passwordText {
            attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
            selected(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
        } else {
            attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
        }
        return false
    }
    
    private func resetAttributes(){
        firstNameText.text = ""
        lastNameText.text = ""
        emailText.text = ""
        passwordText.text = ""
        repasswordText.text = ""
        
        attribute(text: lastNameText, label: LastNameLabel, boarder: lastNameBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        attribute(text: repasswordText, label: repasswordLabel, boarder: repasswordBoarder)
    }
    
    private func resignResponder(){
        firstNameText.resignFirstResponder()
        lastNameText.resignFirstResponder()
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
        repasswordText.resignFirstResponder()
        
        firstNameText.delegate = self
        lastNameText.delegate = self
        emailText.delegate = self
        passwordText.delegate = self
        repasswordText.delegate = self
    }
    
    @objc func closeCell(){
        resignResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            if let window = UIApplication.shared.keyWindow {
                self.mainView.frame = CGRect(x: window.frame.width+30, y: 0, width: window.frame.width, height: window.frame.height)
            }
            
        }, completion: { (Bool) in
            
        })
    }
}
