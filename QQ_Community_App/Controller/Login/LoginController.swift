//
//  LoginController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 24/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Google

class LoginController: UICollectionViewController, UITextFieldDelegate, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    var laterHandler : (() -> Void)?
    let emailBoarder = UIView()
    let passwordBoarder = UIView()
    let createView = UIView()
    let bgKeyboard = UIView()
    
    let fbButton : FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    let googleButton = GIDSignInButton()
    
    let loginView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "login_bg")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let createNewView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "signup")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let emailLabel : UILabel = {
        let label = UILabel()
        label.text = "E-mail Address"
        label.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        label.font = label.font.withSize(10)
        return label
    }()

    let emailText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
//        var placeholder = NSMutableAttributedString()
//        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "E-mail Address", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.8)]))
//        text.setLeftPadding(space: 8)
//        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let passwordLabel : UILabel = {
        let label = UILabel()
        label.text = "Password"
        label.textColor = UIColor.white
        label.font = label.font.withSize(12)
        return label
    }()

    let passwordText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .clear
        text.textColor = UIColor.white
        text.isSecureTextEntry = true
        return text
    }()

    let loginButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.backgroundColor = UIColor.rgba(red: 55, green: 208, blue: 214, alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.setTitle("LOGIN", for: .normal)
        return button
    }()

    let laterButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
        button.setTitle("LATER", for: .normal)
        button.tag = 5
        button.addTarget(self, action: #selector(laterClicked), for: .touchUpInside)
        return button
    }()
    
    let createButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
        button.setTitle("Create Account", for: .normal)
        button.tag = 6
        button.addTarget(self, action: #selector(createClicked), for: .touchUpInside)
        return button
    }()
    
    
    let notMemberLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Not a member?"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = label.font.withSize(13)
        return label
    }()

    let fbView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let fbImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "fb_icon")
        return imageView;
    }()
    
    let googleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()
    
    let googleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "google_icon")
        return imageView;
    }()
    
    let footerLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "or sign in with"
        label.font = label.font.withSize(15)
        return label
    }()
    
    lazy var createUser: CreateUser = {
        let view = CreateUser()
        return view
    }()

    @objc func laterClicked(){
        self.goToNewsController()
    }
    
    @objc func loginClicked(){
        passwordText.text = ""
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
        view.showToast(message: "E-mail and Password do not match.")
    }
    
    private func goToNewsController(){
//        DispatchQueue.main.async(execute: {
//            self.removeSuperview()
            let layout = UICollectionViewFlowLayout()
            let newsViewController = NewsViewController(collectionViewLayout: layout)
//            self.navigationController?.present(newsViewController, animated: true, completion: {})
            self.navigationController?.pushViewController(newsViewController, animated: true)
//        })
    }
    
    @objc func createClicked(){
        createUser.openCell()
    }
    
    private func removeSuperview(){
        loginView.removeFromSuperview()
        emailText.removeFromSuperview()
        passwordText.removeFromSuperview()
        loginButton.removeFromSuperview()
        createButton.removeFromSuperview()
        laterButton.removeFromSuperview()
        notMemberLabel.removeFromSuperview()
        footerLabel.removeFromSuperview()
    }
    
    func setupView() {
        emailBoarder.backgroundColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        passwordBoarder.backgroundColor = UIColor.white
        
        
//        footerLabel.alpha = 0.5
        fbView.alpha = 0.8
        googleView.alpha = 0.8
        fbButton.alpha = 0.8
        
        loginButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(loginClicked)))
        laterButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(laterClicked)))
        createButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createClicked)))
        
        view.addSubview(loginView)
        //text field
        view.addSubview(emailText)
        emailText.addSubview(emailLabel)
        emailText.addSubview(emailBoarder)
        view.addSubview(passwordText)
        passwordText.addSubview(passwordLabel)
        passwordText.addSubview(passwordBoarder)
        //button
        view.addSubview(loginButton)
        view.addSubview(laterButton)
        //create
        view.addSubview(createView)
        view.addSubview(notMemberLabel)
        view.addSubview(createButton)
        view.addSubview(footerLabel)
        view.addSubview(fbButton)
        fbButton.addSubview(fbImage)
        view.addSubview(googleButton)
        googleButton.addSubview(googleImage)
        
        emailLabel.frame = CGRect(x: 0, y: -15, width: view.frame.width-20, height: 20)
        passwordLabel.frame = CGRect(x: 10, y: 5, width: view.frame.width-20, height: 20)
        footerLabel.frame = CGRect(x: view.frame.width/2-100, y: view.frame.height-50, width: 100, height: 30)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: loginView)
        view.addConstraintsFormat(format: "V:|[v0]|", views: loginView)
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width/2+10)-[v0(30)]-10-[v1(30)]", views: fbButton, googleButton)
        view.addConstraintsFormat(format: "V:|-\(view.frame.height-50)-[v0(30)]-30-|", views: fbButton)
        view.addConstraintsFormat(format: "V:|-\(view.frame.height-50)-[v0(30)]-30-|", views: googleButton)
        view.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: laterButton)
        view.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: loginButton)
        view.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: emailText)
        view.addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: passwordText)
        view.addConstraintsFormat(format: "H:|[v0]|", views: emailBoarder)
        view.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: emailBoarder)
        view.addConstraintsFormat(format: "H:|[v0]|", views: passwordBoarder)
        view.addConstraintsFormat(format: "V:|-35-[v0(1)]|", views: passwordBoarder)
        view.addConstraintsFormat(format: "H:|[v0]|", views: fbImage)
        view.addConstraintsFormat(format: "V:|[v0]|", views: fbImage)
        view.addConstraintsFormat(format: "H:|[v0]|", views: googleImage)
        view.addConstraintsFormat(format: "V:|[v0]|", views: googleImage)
        view.addConstraintsFormat(format: "H:|-\(view.frame.width/2-105)-[v0(100)]-2-[v1(110)]", views: notMemberLabel, createButton)
        view.addConstraintsFormat(format: "V:|-\((view.frame.height/2)+150)-[v0(20)]|", views: createButton)
        view.addConstraintsFormat(format: "V:|-\((view.frame.height/2)-80)-[v0(35)]-20-[v1(35)]-30-[v2(40)]-10-[v3(40)]-20-[v4(20)]", views: emailText, passwordText, loginButton, laterButton, notMemberLabel)
        
    }
    
    private func attribute(text : UITextField, label: UILabel, boarder: UIView){
        text.resignFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            text.textColor = UIColor.white
            label.textColor = UIColor.white
            boarder.backgroundColor = UIColor.white
            if text.text!.count == 0 {
                label.font = label.font.withSize(12)
                label.frame = CGRect(x: 10, y: 5, width: self.view.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    private func selected(text : UITextField, label: UILabel, boarder: UIView) {
        text.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            label.font = label.font.withSize(10)
            label.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            label.frame = CGRect(x: 0, y: -15, width: self.view.frame.width-20, height: 20)
            text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            boarder.backgroundColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        }, completion:  { (Bool) in })
    }
    
    @objc private func emailTapped() {
        selected(text: emailText, label: emailLabel, boarder: emailBoarder)
        attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
    }
    
    @objc private func passwordTapped() {
        selected(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
    }

    @objc private func exitKeyboard() {
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
    }
    
    private func setupGestures() {
        emailText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emailTapped)))
        passwordText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(passwordTapped)))
        loginView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitKeyboard)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if result != nil {
            print("DID LOGGED IN WITH FB" ,result)
        }
//        self.goToNewsController()
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailText {
            attribute(text: emailText, label: emailLabel, boarder: emailBoarder)
            selected(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        } else {
            attribute(text: passwordText, label: passwordLabel, boarder: passwordBoarder)
        }
        return false
    }
    
    private func googleSignIn() {
        var error : NSError?
        
        GGLContext.sharedInstance().configureWithError(&error)
        
        if error != nil {
            print("google signin ERROR : ", error!)
            return
        }
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print("GIDSignIn ERROR : ", error!)
            return
        }
        if user.profile.email != nil{
            print("user email from google : ", user.profile.email)
            let alert = UIAlertController(title: "Logged in as \(user.profile.email!)", message: "Your Google Account is already logged in do you want to proceed to home?", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Default action"), style: .default, handler: { _ in
                self.goToNewsController()
            }))
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .cancel, handler: { _ in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func fetchFBEmail() {
        print("already logged in")
        let parameters = ["fields": "email, first_name, last_name"]
        FBSDKGraphRequest(graphPath: "user", parameters: parameters).start(completionHandler: {
            (connection, result, error) -> Void in
            if error != nil {
                print("error from graph ", error!)
                return
            }

            if result != nil{
                print("user email from facebook : ",result!)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        
        emailText.delegate = self
        passwordText.delegate = self
        fbButton.delegate = self
        
        if let token = FBSDKAccessToken.current(){
            fetchFBEmail()
        }
        
        setupView()
        setupGestures()
        googleSignIn()
    }
    
}
