//
//  AboutController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class AboutController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let page1Id = "page1Id",
        page2Id = "page2Id",
        page3Id = "page3Id"
    
    private let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 3
        pc.currentPageIndicatorTintColor = UIColor.white
        pc.pageIndicatorTintColor = UIColor.ThemeDark(alpha: 0.5)
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("NEXT", for: .normal)
        button.tag = 6
        button.addTarget(self, action: #selector(nextClicked), for: .touchUpInside)
        return button
    }()
    
    let nextImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_next")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    let prevButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("PREV", for: .normal)
        button.tag = 6
        button.addTarget(self, action: #selector(prevClicked), for: .touchUpInside)
        return button
    }()
    
    let prevImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_previous")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        return image
    }()
    
    @objc func nextClicked() {
        prevButton.alpha = 1
        if pageControl.currentPage == 1 {
            nextButton.alpha = 0
            pageControl.currentPageIndicatorTintColor = UIColor.ThemeDark(alpha: 1.0)
            pageControl.pageIndicatorTintColor = UIColor.Theme(alpha: 0.5)
            prevButton.setTitleColor(UIColor.ThemeDark(alpha: 1.0), for: .normal)
            prevImage.tintColor = UIColor.ThemeDark(alpha: 1.0)
        } else {
            nextButton.alpha = 1
            pageControl.currentPageIndicatorTintColor = UIColor.white
            pageControl.pageIndicatorTintColor = UIColor.ThemeDark(alpha: 0.5)
        }
        let index = pageControl.currentPage + 1
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = index
    }
    
    @objc func prevClicked() {
        nextButton.alpha = 1
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.ThemeDark(alpha: 0.5)
        prevButton.setTitleColor(UIColor.white, for: .normal)
        prevImage.tintColor = UIColor.white
        
        if pageControl.currentPage == 1 {
            prevButton.alpha = 0
        } else {
            prevButton.alpha = 1
        }
        let index = pageControl.currentPage - 1
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = index
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if(indexPath.item == 1){
            return collectionView.dequeueReusableCell(withReuseIdentifier: page2Id, for: indexPath)
        } else if (indexPath.item == 2){
            return collectionView.dequeueReusableCell(withReuseIdentifier: page3Id, for: indexPath)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: page1Id, for: indexPath)
        return cell
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let x = targetContentOffset.pointee.x
        
        let page = Int(x/view.frame.width)
        pageControl.currentPage = page
        if page == 0 {
            prevButton.alpha = 0
        } else if page == 2 {
            nextButton.alpha = 0
            pageControl.currentPageIndicatorTintColor = UIColor.ThemeDark(alpha: 1.0)
            pageControl.pageIndicatorTintColor = UIColor.Theme(alpha: 0.5)
            prevButton.setTitleColor(UIColor.ThemeDark(alpha: 1.0), for: .normal)
            prevImage.tintColor = UIColor.ThemeDark(alpha: 1.0)
        } else {
            prevButton.alpha = 1
            nextButton.alpha = 1
            pageControl.currentPageIndicatorTintColor = UIColor.white
            pageControl.pageIndicatorTintColor = UIColor.ThemeDark(alpha: 0.5)
            prevButton.setTitleColor(UIColor.white, for: .normal)
            prevImage.tintColor = UIColor.white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.Theme(alpha: 1.0)
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        prevButton.alpha = 0
        
        view.addSubview(pageControl)
        pageControl.addSubview(nextButton)
        nextButton.addSubview(nextImage)
        pageControl.addSubview(prevButton)
        prevButton.addSubview(prevImage)
        collectionView!.showsHorizontalScrollIndicator = false
//        collectionView?.isScrollEnabled = false
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: pageControl)
        view.addConstraintsFormat(format: "V:|-\(view.frame.height-110)-[v0(50)]|", views: pageControl)
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-100)-[v0(80)]|", views: nextButton)
        view.addConstraintsFormat(format: "V:|-15-[v0(25)]|", views: nextButton)
        view.addConstraintsFormat(format: "H:|-65-[v0(15)]|", views: nextImage)
        view.addConstraintsFormat(format: "V:|-5-[v0(15)]|", views: nextImage)
        
        view.addConstraintsFormat(format: "H:|-10-[v0(80)]|", views: prevButton)
        view.addConstraintsFormat(format: "V:|-15-[v0(25)]|", views: prevButton)
        view.addConstraintsFormat(format: "H:|[v0(15)]|", views: prevImage)
        view.addConstraintsFormat(format: "V:|-5-[v0(15)]|", views: prevImage)
        
        collectionView?.register(AboutPage1Cell.self, forCellWithReuseIdentifier: page1Id)
        collectionView?.register(AboutPage2Cell.self, forCellWithReuseIdentifier: page2Id)
        collectionView?.register(AboutPage3Cell.self, forCellWithReuseIdentifier: page3Id)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.isPagingEnabled = true
    }
}
