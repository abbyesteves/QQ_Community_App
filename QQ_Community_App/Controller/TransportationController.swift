//
//  TransportationController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacesSearchController
import UberRides

class TransportationController: UICollectionViewController, MKMapViewDelegate, CLLocationManagerDelegate, UISearchDisplayDelegate {
    
    static var destination = String()
    static var origin = String()
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var googleMapsView : GMSMapView!
    static var regionPin = MKCoordinateRegion()
    
    let googleController = GooglePlacesSearchController(
        apiKey: ApiService().googlePlaceAPIKey,
        placeType: PlaceType.address
    )
    
    let mapView: MKMapView = {
        let map = MKMapView()
        return map
    }()
    let manager = CLLocationManager()
    var once = 0,
    longitudeValue = 0.0,
    latitudeValue = 0.0
    let heightView = CGFloat(300)
    static var currentLongitude = CLLocationDegrees()
    static var currentLatitude = CLLocationDegrees()
    
    let builder = RideParametersBuilder()
    let uberButton : RideRequestButton = {
        let uber = RideRequestButton()
        uber.layer.shadowRadius = 0
//        uber.rideParameters
        return uber
    }()
    let backDetailView = UIView()
    let detailView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 3
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 5
        return view
    }()
    
    let goToDirectionView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let uberView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    let directionView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let destinationLabel : UILabel = {
        let label = UILabel()
        label.text = "Going somewhere?"
        label.numberOfLines = 2
        label.textAlignment = .left
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.font = UIFont.boldSystemFont(ofSize: 14.5)
        return label
    }()
    
    let arrivalLabel : UILabel = {
        let label = UILabel()
        label.text = "input your destination"
        label.textAlignment = .left
        label.textColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        label.font = label.font.withSize(13)
        return label
    }()
    
    let directionLabel : UILabel = {
        let label = UILabel()
        label.text = "know how to get there"
        label.textAlignment = .left
        label.textColor = UIColor.darkGray
        label.font = label.font.withSize(12)
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
//        label.textAlignment = .center;
        return label
    }()

    let addressLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Address"
        return label
    }()

    let longitudeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Longitude"
        return label
    }()

    let latitudeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Latitude"
        return label
    }()

    let longitudeDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(14)
        return label
    }()

    let latitudeDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = label.font.withSize(14)
        return label
    }()

    let addressDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 5
        label.font = label.font.withSize(14)
        return label
    }()

    let directionButtonLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Directions"
        label.textAlignment = .center;
        return label
    }()

    let boarderView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let searchView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 2
        return view
    }()
    
    let searchImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        return imageView
    }()
    
    let currentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 2
        return view
    }()
    
    let currentImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_current_location")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        return imageView
    }()
    
    let transportationView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 2
        return view
    }()
    
    let transportationImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_transportation")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        return imageView
    }()
    
    @objc func goToCoordinates(){
        self.activityIndicator.startAnimating()
        getDirections(latitude: latitudeValue, longitude: longitudeValue)
    }
    
    @objc func bgDismiss(){
        latitudeValue = 0.0
        longitudeValue = 0.0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.backDetailView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.detailView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: self.heightView)
            }
            
        }, completion: { (Bool) in
            
            self.detailView.removeFromSuperview()
            self.titleLabel.removeFromSuperview()
            self.goToDirectionView.removeFromSuperview()
            self.backDetailView.removeFromSuperview()
            
        })
    }
    
    @objc private func directionsTapped(){
        let layout = UICollectionViewFlowLayout()
        let directionsDetail = DirectionsDetail(collectionViewLayout: layout)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        navigationController?.pushViewController(directionsDetail, animated: true)
    }
    
    @objc func searchOptions(){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let searchWindow = UIWindow(frame: UIScreen.main.bounds)
                searchWindow.rootViewController = UIViewController()
                searchWindow.windowLevel = UIWindowLevelAlert + 1;
                searchWindow.makeKeyAndVisible()
                searchWindow.rootViewController?.present(self.googleController, animated: true, completion: nil)
                
                self.googleController.didSelectGooglePlace { (place) -> Void in
                    self.googleController.isActive = false
                    TransportationController.destination = place.name
                    self.setPin(place : place)
                }
            }
        }
    }
    
    @objc func currentLocation(){
        self.destinationLabel.text = "Going somewhere?"
        self.arrivalLabel.text = "input your destination"
        spanToLocation(latitude : TransportationController.currentLatitude, longitude : TransportationController.currentLongitude)
    }
    
    @objc func getTransportation() {
        self.activityIndicator.startAnimating()
        loadTransportation(region: TransportationController.regionPin, searchFor: "Transportation")
    }
    
    //FUNC
    
    //Get current position
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let regionUpdate : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
        TransportationController.regionPin = regionUpdate
        
        TransportationController.currentLongitude = location.coordinate.longitude
        TransportationController.currentLatitude = location.coordinate.latitude
        
        if once == 0 {
//            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
//            self.googleMapsView.animate(to: camera)
            mapView.setRegion(regionUpdate, animated: true)
            loadTransportation(region: regionUpdate, searchFor: "Transportation")
        }
        once = once+1
        self.mapView.showsUserLocation = true
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        renderer.lineWidth = 4.0
        self.activityIndicator.stopAnimating()
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation
        let address = annotation!.subtitle! ?? ""
        let title = annotation!.title! ?? ""
        
        showDetails(address: address, title: title, coordinates: annotation!.coordinate  )
    }
    
    func loadTransportation(region: MKCoordinateRegion, searchFor: String){
        //remove all annotations
//        let allAnnotations = self.mapView.annotations
//        for annotation in allAnnotations {
//            if (annotation.subtitle??.contains("Transportation"))!{
//                print("i found : ")
//                self.mapView.removeAnnotation(annotation)
//            }
//        }
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchFor
        request.region = region
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: {
            response, error in

            guard let response = response else { return }

            if response == nil {
                self.view.showToast(message: "Be sure you are connected to the internet")
            } else {
//                print("MY LOCATIONS RESULTS")
//                print(response.mapItems)

                for item in response.mapItems {
                    let annotation = MKPointAnnotation()
                    let address = String(describing: item.placemark)

                    annotation.coordinate = item.placemark.coordinate
                    annotation.subtitle = "\(address.components(separatedBy: " @")[0]) \n \n Local Tranasportation"
                    annotation.title = item.name
                    

                    DispatchQueue.global(qos: .userInitiated).async {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.mapView.addAnnotation(annotation)
                        }
                    }
                }
            }
        })
    }

    func showDetails(address: String, title: String, coordinates: CLLocationCoordinate2D){
        TransportationController.destination = title
        latitudeValue = coordinates.latitude
        longitudeValue = coordinates.longitude
        
        spanToLocation(latitude : latitudeValue, longitude : longitudeValue)

        if let window = UIApplication.shared.keyWindow {
            titleLabel.text = title
            addressDataLabel.text = address
            latitudeDataLabel.text = String(coordinates.latitude)
            longitudeDataLabel.text = String(coordinates.longitude)

            backDetailView.backgroundColor = UIColor(white: 0, alpha: 0.3)//0.6
            backDetailView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            goToDirectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToCoordinates)))

            window.addSubview(backDetailView)
            window.addSubview(detailView)
            detailView.addSubview(titleLabel)
            detailView.addSubview(goToDirectionView)
            detailView.addSubview(addressLabel)
            detailView.addSubview(addressDataLabel)
            detailView.addSubview(boarderView)
            detailView.addSubview(longitudeLabel)
            detailView.addSubview(latitudeDataLabel)
            detailView.addSubview(longitudeDataLabel)
            detailView.addSubview(latitudeLabel)
            goToDirectionView.addSubview(directionButtonLabel)
            window.addSubview(activityIndicator)

            boarderView.alpha = 0.3
            backDetailView.alpha = 0

            backDetailView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            window.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: addressLabel)
            window.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: addressDataLabel)
            window.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: boarderView)
            window.addConstraintsFormat(format: "H:|[v0]|", views: directionButtonLabel)
            window.addConstraintsFormat(format: "V:|[v0]|", views: directionButtonLabel)

            window.addConstraintsFormat(format: "H:|-10-[v0(\(window.frame.width/2))][v1]", views: latitudeLabel, longitudeLabel)
            window.addConstraintsFormat(format: "V:|-220-[v0(20)]", views: latitudeLabel)
            window.addConstraintsFormat(format: "V:|-220-[v0(20)]", views: longitudeLabel)

            window.addConstraintsFormat(format: "H:|-10-[v0(\(window.frame.width/2))][v1]", views: latitudeDataLabel, longitudeDataLabel)
            window.addConstraintsFormat(format: "V:|-245-[v0(20)]", views: latitudeDataLabel)
            window.addConstraintsFormat(format: "V:|-245-[v0(20)]", views: longitudeDataLabel)

            window.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: titleLabel)
            window.addConstraintsFormat(format: "V:|-10-[v0(20)]-20-[v1(50)]-25-[v2(20)]-5-[v3(40)]-20-[v4(1)]", views: titleLabel, goToDirectionView, addressLabel, addressDataLabel, boarderView)
            window.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: goToDirectionView)
            detailView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: heightView)
            
            window.addConstraintsFormat(format: "H:|[v0]|", views: activityIndicator)
            window.addConstraintsFormat(format: "V:|-\((window.frame.height-200)/2)-[v0(20)]|", views: activityIndicator)



            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {

                self.backDetailView.alpha = 1
                self.detailView.frame = CGRect(x: 0, y: window.frame.height-self.heightView, width: window.frame.width, height: self.heightView)

            }, completion: { (Bool) in })
        }
    }
    
    func spanToLocation(latitude : CLLocationDegrees, longitude : CLLocationDegrees) {
        self.removeOverlays()
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.mapView.setRegion(region, animated: true)
            }
        }
    }
    
    func setPin(place : PlaceDetails){
//        print("selected location : ", place)
        self.getDistanceDuration()
        self.arrivalLabel.text = "Calculating travel..."
        self.removeOverlays()

        let allAnnotations = self.mapView.annotations
        for annotation in allAnnotations {
            if annotation.subtitle! == "no address"{
                self.mapView.removeAnnotation(annotation)
            }
        }
        
        let latitude = place.coordinate.latitude
        let longitude = place.coordinate.longitude
        
        let annotation = MKPointAnnotation()
        annotation.title = place.name
        annotation.subtitle = "no address"
        annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)

        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        TransportationController.regionPin = region
        
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.destinationLabel.text = place.name
                self.mapView.addAnnotation(annotation)
                self.mapView.setRegion(region, animated: true)
            }
        }
    }
    
    private func setUberParameters(place : PlaceDetails){
        let pickupLocation = CLLocation(latitude: TransportationController.currentLatitude, longitude: TransportationController.currentLongitude)
        let dropoffLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        builder.pickupLocation = pickupLocation
        builder.dropoffLocation = dropoffLocation
        builder.dropoffNickname = "QQCommunity"
        builder.dropoffAddress = place.formattedAddress
        uberButton.rideParameters = builder.build()
    }
    
    private func setupGoogleMaps() {
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-220)
        self.googleMapsView = GMSMapView(frame: frame)
        self.view.addSubview(self.googleMapsView)
        self.googleMapsView.isMyLocationEnabled = true

        self.manager.delegate = self
        self.manager.startUpdatingLocation()
    }
    
    private func setupView() {
        view.addSubview(uberView)
        uberView.addSubview(uberButton)
        view.addSubview(directionView)
        directionView.addSubview(destinationLabel)
        directionView.addSubview(arrivalLabel)
        directionView.addSubview(directionLabel)
    }
    
    private func setupConstraints(){
        uberView.frame = CGRect(x: 0, y: view.frame.height-220, width: view.frame.width, height: 60)
        uberButton.frame = CGRect(x: 0, y: 0, width: uberView.frame.width, height: uberView.frame.height)
        
        directionView.frame = CGRect(x: 0, y: view.frame.height-160, width: uberView.frame.width, height: 160)
        destinationLabel.frame = CGRect(x: 20, y: 20, width: view.frame.width, height: 20)
        arrivalLabel.frame = CGRect(x: 20, y: 40, width: view.frame.width, height: 20)
        directionLabel.frame = CGRect(x: 20, y: 60, width: view.frame.width, height: 20)
    }
    
    private func removeOverlays() {
        //remove all overlays before creating one
        let allOverlays = self.mapView.overlays
        self.mapView.removeOverlays(allOverlays)
    }
    
    private func getDirections(latitude: Double, longitude: Double) {
        self.removeOverlays()
        self.getDistanceDuration()
        self.arrivalLabel.text = "Calculating travel..."
        self.destinationLabel.text = TransportationController.destination

        let currentLocation = manager.location?.coordinate
        let destCoordinates = CLLocationCoordinate2DMake(latitude, longitude)

        let currentPlacemark = MKPlacemark(coordinate: currentLocation!)
        let destPlacemark = MKPlacemark(coordinate: destCoordinates)

        let currentItem = MKMapItem(placemark: currentPlacemark)
        let destItem = MKMapItem(placemark: destPlacemark)

        let directionRequest = MKDirectionsRequest()
        directionRequest.source = currentItem
        directionRequest.destination = destItem
        //select data mode of request
        directionRequest.transportType = .automobile

        let directions = MKDirections(request: directionRequest)
        directions.calculate(completionHandler: {
            response, error in

            guard let response = response else {
                if let error = error {
                    print("error getting directions : ",error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    self.view.showToast(message: "Something went wrong.")
                }
                return
            }

            let route = response.routes[0]
            print(" route : ",route)
            self.mapView.add(route.polyline, level: .aboveRoads)

            let rekt = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rekt), animated: true)
            self.bgDismiss()
        })

    }

    private func setupGestures(){
        uberButton.rideParameters = builder.build()
        directionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(directionsTapped)))
    }
    
    private func getDistanceDuration(){
        let dest = TransportationController.destination.replacingOccurrences(of: " ", with: "+")
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=baliuag&destinations=\(dest)&key=AIzaSyAZvtIXyT_o_ys6NYT3sTUTbosqAtKTEFQ"
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = url
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                do {
                    let json = try JSONDecoder().decode(Routes.self, from: data)
                    self.arrivalLabel.text = "\(json.rows[0].elements[0].distance.text)les and takes \(json.rows[0].elements[0].duration.text) of travel"
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }
            }.resume()
        }
    }
    
    func setUpMapsKit(){
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        searchView.alpha = 0.9
        currentView.alpha = 0.9
        transportationView.alpha = 0.9
        //
        self.mapView.delegate = self
        self.mapView.showsBuildings = true
        self.mapView.showsScale = true
        self.mapView.showsPointsOfInterest = true
        self.mapView.showsCompass = true
        
        view.addSubview(mapView)
        view.addSubview(searchView)
        searchView.addSubview(searchImage)
        view.addSubview(currentView)
        currentView.addSubview(currentImage)
        view.addSubview(transportationView)
        transportationView.addSubview(transportationImage)
        
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-40-10)-[v0(40)]|", views: searchView)
        view.addConstraintsFormat(format: "V:|-10-[v0(40)]|", views: searchView)
        view.addConstraintsFormat(format: "H:|-10-[v0(20)]|", views: searchImage)
        view.addConstraintsFormat(format: "V:|-10-[v0(20)]|", views: searchImage)
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-40-10)-[v0(40)]|", views: currentView)
        view.addConstraintsFormat(format: "V:|-50-[v0(40)]|", views: currentView)
        view.addConstraintsFormat(format: "H:|-10-[v0(20)]|", views: currentImage)
        view.addConstraintsFormat(format: "V:|-10-[v0(20)]|", views: currentImage)
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-40-10)-[v0(40)]|", views: transportationView)
        view.addConstraintsFormat(format: "V:|-90-[v0(40)]|", views: transportationView)
        view.addConstraintsFormat(format: "H:|-10-[v0(20)]|", views: transportationImage)
        view.addConstraintsFormat(format: "V:|-10-[v0(20)]|", views: transportationImage)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: mapView)
        view.addConstraintsFormat(format: "V:|[v0(\(view.frame.height-220))]|", views: mapView)
            
        searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchOptions)))
        currentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(currentLocation)))
        transportationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getTransportation)))
    }
    
    
    override func viewDidLoad() {
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
//        setupGoogleMaps()
        setUpMapsKit()
        setupView()
        setupConstraints()
        setupGestures()
    }
}

class GetDetinationDuration {
    var rows: Rows? { //News?
        didSet {
            var elements: Elements? {
                didSet {
                    print(elements?.distance, elements?.duration)
                }
            }
            
        }
    }
}
