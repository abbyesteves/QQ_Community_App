//
//  PrintableFormsController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/01/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import WebKit

class PrintableFormsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    static var urlForm = String()
    static var titleForm = String()
    var forms: [Forms] = {
        var data1 = Forms()
        data1.title = "Department of Public Works and Highways"
        data1.body = " Construction/Maintenance of Flood Mitigation Structures and Drainage Systems – Construction of Flood Control Structures along Bocaue River"
        data1.url = "http://www.dpwh.gov.ph/dpwh/sites/default/files/webform/civil_works/notice_of_award/noa17cc0210.pdf"
        
        var data2 = Forms()
        data2.title = "Department of Public Works and Highways"
        data2.body = "Construction of Revetment along Labangan Channel San Isidro II Section"
        data2.url = "http://www.dpwh.gov.ph/dpwh/sites/default/files/webform/civil_works/notice_of_award/award17cc0094.pdf"
        
        var data3 = Forms()
        data3.title = "Quality Manual"
        data3.body = "It is an exclusive property for BWD and it shall not be reproduced in whole or in part without the approval of the BWD Board of Directors through the General Manager."
        data3.url = "http://www.bocauewater.com/assets/Bocaue-QMM.pdf"
        
        var data4 = Forms()
        data4.title = "Census of Population and Housing"
        data4.body = "Total Population by Province, City, Municipality and Barangay"
        data4.url = "https://psa.gov.ph/sites/default/files/attachments/hsd/pressrelease/Central%20Luzon.pdf"
        
        var data5 = Forms()
        data5.title = "National Irrigation Administration"
        data5.body = "Bulacan SPIP - Bocaue"
        data5.url = "http://region3.nia.gov.ph/sites/r3/files/BULACAN%20SPIP%20-BOCAUE.pdf"
        
        return [data1, data2, data3, data4, data5]
    }()
    private let header: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Printable Forms"
        return label
    }()
    
    var webView : UIWebView = {
        let web = UIWebView()
        return web
    }()
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forms.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PrintableFormsCell
        cell.backgroundColor = UIColor.white
        cell.form = forms[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let form = forms[indexPath.item]
        PrintableFormsController.titleForm = form.title!
        PrintableFormsController.urlForm = form.url!
        viewPdf(url: form.url!, title: form.title!)
//        webDetail.openCell(url : form.url!, sourceName : form.title!)
    }
    
    private func viewPdf(url: String, title: String){
//        print("SELECTED FORM : ", url)
        if let urlView = URL(string: url){
            webView = UIWebView(frame: self.view.frame)
            let urlRequest = URLRequest(url: urlView)
            webView.loadRequest(urlRequest as URLRequest)
            webView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            
            let titleLabel : UILabel = {
                let label = UILabel()
                label.text = title
                label.textColor = UIColor.white
                return label
            }()
            
            let optionImage = UIImage(named: "sharex40")?.withRenderingMode(.alwaysOriginal)
            let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))
            
            let pdfController = UIViewController()
            pdfController.view.addSubview(webView)
            pdfController.navigationItem.titleView = titleLabel
            pdfController.navigationItem.rightBarButtonItems = [optionBarItem]
            pdfController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            self.navigationController?.pushViewController(pdfController, animated: true)
        }
    }
    
    @objc func handleOptions(){
//        let printInfo = UIPrintInfo(dictionary:nil)
//        printInfo.outputType = UIPrintInfoOutputType.general
//        printInfo.jobName = PrintableFormsController.urlSelected
//
//        // Set up print controller
//        let printController = UIPrintInteractionController.shared
//        printController.printInfo = printInfo
//
//        // Do it
//        printController.present(from: self.view.frame, in: self.view, animated: true, completionHandler: nil)
        let text = "Share about \(PrintableFormsController.titleForm)"
        let url:NSURL = NSURL(string: PrintableFormsController.urlForm)!
        let activityVC = UIActivityViewController(activityItems: [text, url], applicationActivities: [])
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    private func setupView(){
        collectionView?.addSubview(header)
//        view.addSubview(header)
        
        header.frame = CGRect(x: 20, y: -60, width: view.frame.width, height: 30)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        collectionView?.scrollsToTop = true
        collectionView?.contentInset = UIEdgeInsetsMake(80, 0, 10, 0)
        collectionView?.backgroundColor = UIColor.Theme(alpha: 1.0)
        collectionView?.register(PrintableFormsCell.self, forCellWithReuseIdentifier: cellId)
    }
}
