//
//  BusinessCenterController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 23/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit

class BusinessCenterController: UICollectionViewController {
    
    let titleHeader: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "Business Centre"
        return label
    }()
    
    let footerText: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.text = "WANT TO SEE YOUR BUSINESS HERE?"
        
        return label
    }()
    
    let bannerHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let bannerFooter: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    //Views
    let Restaurants: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    let Hotels: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    let PersonalCare: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    let Banks: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        return view
    }()
    
    let Education: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        return view
    }()
    
    let Services: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.Theme(alpha: 0.2).cgColor
        view.layer.borderWidth = 0.5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 0.5
        return view
    }()
    
    // Images
    let RestaurantImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_food")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let HotelsImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_bed")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let PersonalImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_barbershop")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let BanksImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_receive_cash")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let EducationImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_graduation_cap")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let ServicesImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_work")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    // Labels
    let RestaurantLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Restaurants"
        label.textAlignment = .center;
        return label
    }()
    
    let HotelsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Hotels"
        label.textAlignment = .center;
        return label
    }()
    
    let PersonalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Personal Care"
        label.textAlignment = .center;
        return label
    }()
    
    let BanksLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Banks"
        label.textAlignment = .center;
        return label
    }()
    
    let EducationLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Educations"
        label.textAlignment = .center;
        return label
    }()
    
    let ServicesLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Theme(alpha: 1.0)
        //            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Services"
        label.textAlignment = .center;
        return label
    }()
    
    lazy var restaurantTab: RestaurantTabController = {
        let launcher = RestaurantTabController()
        return launcher
    }()
    
    private func goToTabs() {
        let newsViewController = TabBarController()
        navigationController?.pushViewController(newsViewController, animated: true)
    }
    
    @objc private func goToServices(){
        TabBarController().selectedIndex(int : 5, tab: "Services")
        self.goToTabs()
    }
    
    @objc private func goToEducation(){
        TabBarController().selectedIndex(int : 4, tab: "Education")
        self.goToTabs()
    }
    
    @objc private func goToBank(){
        TabBarController().selectedIndex(int : 3, tab: "Banks")
        self.goToTabs()
    }
    
    @objc private func goToPersonalCare(){
        TabBarController().selectedIndex(int : 2, tab: "Personal Care")
        self.goToTabs()
    }
    
    @objc private func goToHotels(){
        TabBarController().selectedIndex(int : 1, tab: "Hotels")
        self.goToTabs()
    }
    
    @objc private func goToRestaurant(){
        TabBarController().selectedIndex(int : 0, tab: "Restaurants")
        self.goToTabs()
//        restaurantTab.setup()
    }
    
    @objc func goToNewBusiness(){
        let layout = UICollectionViewFlowLayout()
        let seeBusinessController = SeeBusinessController(collectionViewLayout: layout)
        navigationController?.pushViewController(seeBusinessController, animated: true)
        
        seeBusinessController.navigationItem.title = "Register Business"
    }
    
    // LAYOUT
    private func setUpConstraints(){
        // Computation for the width
        let width = (view.frame.width-320)/2
        let width2 = width+160
        
        // Restaurant
        view.addConstraintsFormat(format: "H:|-\(width)-[v0(160)]|", views: Restaurants)
        view.addConstraintsFormat(format: "V:|-70-[v0(160)]|", views: Restaurants)
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: RestaurantLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: RestaurantLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: RestaurantImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: RestaurantImage)
        
        // Hotels
        view.addConstraintsFormat(format: "H:|-\(width2)-[v0(160)]|", views: Hotels)
        view.addConstraintsFormat(format: "V:|-70-[v0(160)]|", views: Hotels)
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: HotelsLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: HotelsLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: HotelsImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: HotelsImage)
        
        // Personal Care
        view.addConstraintsFormat(format: "H:|-\(width)-[v0(160)]|", views: PersonalCare)
        view.addConstraintsFormat(format: "V:|-230-[v0(160)]|", views: PersonalCare)
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: PersonalLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: PersonalLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: PersonalImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: PersonalImage)
        
        
        // Banks
        view.addConstraintsFormat(format: "H:|-\(width2)-[v0(160)]|", views: Banks)
        view.addConstraintsFormat(format: "V:|-230-[v0(160)]|", views: Banks)
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: BanksLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: BanksLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: BanksImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: BanksImage)
        
        // Education
        view.addConstraintsFormat(format: "H:|-\(width)-[v0(160)]|", views: Education)
        view.addConstraintsFormat(format: "V:|-390-[v0(160)]|", views: Education)
        
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: EducationLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: EducationLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: EducationImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: EducationImage)
        
        // Services
        view.addConstraintsFormat(format: "H:|-\(width2)-[v0(160)]|", views: Services)
        view.addConstraintsFormat(format: "V:|-390-[v0(160)]|", views: Services)
        
        view.addConstraintsFormat(format: "H:|[v0(160)]|", views: ServicesLabel)
        view.addConstraintsFormat(format: "V:|-120-[v0(20)]|", views: ServicesLabel)
        
        view.addConstraintsFormat(format: "H:|-50-[v0(60)]|", views: ServicesImage)
        view.addConstraintsFormat(format: "V:|-40-[v0(60)]|", views: ServicesImage)
        
        
        
        // Layout
        view.addConstraintsFormat(format: "H:|[v0]|", views: footerText)
        view.addConstraintsFormat(format: "V:|[v0(30)]|", views: footerText)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: bannerHeader)
        view.addConstraintsFormat(format: "V:|[v0(200)]|", views: bannerHeader)
        
        view.addConstraintsFormat(format: "H:|-20-[v0]|", views: titleHeader)
        view.addConstraintsFormat(format: "V:|-10-[v0(50)]|", views: titleHeader)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: bannerFooter)
        view.addConstraintsFormat(format: "V:|-\(view.frame.height-95)-[v0(40)]|", views: bannerFooter)
    }

    private func setupView(){
        view.addSubview(bannerHeader)
        view.addSubview(bannerFooter)
        view.addSubview(titleHeader)
        //
        view.addSubview(Restaurants)
        view.addSubview(Hotels)
        view.addSubview(PersonalCare)
        view.addSubview(Banks)
        view.addSubview(Education)
        view.addSubview(Services)
        //footer
        bannerFooter.addSubview(footerText)
        
        Restaurants.addSubview(RestaurantImage)
        Restaurants.addSubview(RestaurantLabel)
        //
        Hotels.addSubview(HotelsLabel)
        Hotels.addSubview(HotelsImage)
        //
        PersonalCare.addSubview(PersonalImage)
        PersonalCare.addSubview(PersonalLabel)
        //
        Banks.addSubview(BanksImage)
        Banks.addSubview(BanksLabel)
        //
        Education.addSubview(EducationImage)
        Education.addSubview(EducationLabel)
        //
        Services.addSubview(ServicesImage)
        Services.addSubview(ServicesLabel)
    }

    private func setUpGestures() {
        Services.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToServices)))
        Education.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToEducation)))
        Banks.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToBank)))
        PersonalCare.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToPersonalCare)))
        Restaurants.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToRestaurant)))
        Hotels.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToHotels)))
        bannerFooter.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToNewBusiness)))
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        setUpGestures()
        setupView()
        setUpConstraints()
    }
}
