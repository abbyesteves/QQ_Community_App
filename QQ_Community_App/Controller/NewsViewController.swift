//
//  mainViewController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 30/10/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import GooglePlacesSearchController

class NewsViewController: UICollectionViewController,
    UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UISearchDisplayDelegate{
    
    let cellId = "cellId",
        healthCellID = "healthCellID",
        PnpCellID = "PnpCellID",
        BfpCellID = "BfpCellID",
        MdrrmoCellID = "MdrrmoCellID",
        twitterId = "twitterId",
        socialId = "socialId"
    let floatMenu = FloatMenu().floatMenu
    let floatImage = FloatMenu().floatImage
    
    let statusBarBackground: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        return view
    }()
    
    lazy var addFloatMenu: FloatMenu = {
        let launcher = FloatMenu()
        return launcher
    }()
    
    lazy var menuLauncher: MenuLauncher = {
        let launcher = MenuLauncher()
        launcher.newsController = self
        return launcher
    }()
    
    lazy var menuBar: MenuBar = {
        let mb = MenuBar()
        mb.newsController = self
        return mb
    }()
    
    lazy var transportationController: TransportationController = {
        let launcher = TransportationController()
        return launcher
    }()
    
    lazy var loginController: LoginController = {
        let launcher = LoginController()
        return launcher
    }()
    
    @objc func FloatMenuTapped() {
        addFloatMenu.Tapped()
    }
    
    private func setupFloatMenu(){
        //float menu
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(floatMenu)
            floatMenu.addSubview(floatImage)
            window.addConstraintsFormat(format: "H:|-10-[v0(35)]|", views: floatImage)
            window.addConstraintsFormat(format: "V:|-10-[v0(35)]|", views: floatImage)
            floatMenu.frame = CGRect(x: window.frame.width-77, y:window.frame.height-80, width: 55, height: 55)
        }
        
        floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FloatMenuTapped)))
    }
    
    private func setupStatusBar(){
        UIApplication.shared.isStatusBarHidden = false
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(statusBarBackground)
            statusBarBackground.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
            statusBarBackground.widthAnchor.constraint(equalTo: window.widthAnchor).isActive = true
            statusBarBackground.heightAnchor.constraint(equalTo: statusBarBackground.heightAnchor, multiplier: 20).isActive = true
            window.addConstraintsFormat(format: "V:|[v0(20)]|", views: statusBarBackground)
        }
    }
    
    private func setupCollectionView(){
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(TwitterTabCell.self, forCellWithReuseIdentifier: twitterId)
        collectionView?.register(SocialTabCell.self, forCellWithReuseIdentifier: socialId)
        collectionView?.register(NewsTabCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(HealthTabCell.self, forCellWithReuseIdentifier: healthCellID)
        collectionView?.register(PnpTabCell.self, forCellWithReuseIdentifier: PnpCellID)
        collectionView?.register(BfpTabCell.self, forCellWithReuseIdentifier: BfpCellID)
        collectionView?.register(MdrrmoTabCell.self, forCellWithReuseIdentifier: MdrrmoCellID)
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.isPagingEnabled = true
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = Int(scrollView.contentOffset.x/7)
        let indexPath = scrollView.contentOffset.x/view.frame.width
        let screenInit = Int(view.frame.width/7)
//        print("scroll : ", position, scrollView.contentOffset.x, screenInit)
        var current = 0
        if indexPath == 0 {
            current = 0
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 1 {
            current = 120
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 2 {
            current = Int((view.frame.width/2)-55)
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 3 {
            current = Int((view.frame.width/2)-75)
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 4 {
            current = Int(view.frame.width-70-50-50)
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 5 {
            current = Int(view.frame.width-70-50)
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else if indexPath == 6 {
            current = Int(view.frame.width-70)
            menuBar.barLeftAnchor?.constant = CGFloat(current)
        } else {
//            if position < screenInit {
//                if menuBar.widthBarView?.constant == (90){
//
//                } else {
//                    menuBar.widthBarView?.constant = CGFloat(120+position)
//                }
//
//            } else {
//
//                menuBar.widthBarView?.constant = CGFloat(90)
//            }
            menuBar.barLeftAnchor?.constant = CGFloat(current+position)
        }
        
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = NSIndexPath(item: Int(index), section: 0)
        if index == 0 {
            menuBar.widthBarView?.constant = 120
        } else if index == 1 {
            menuBar.widthBarView?.constant = 90
        } else  if index == 2 {
            menuBar.widthBarView?.constant = 110
        } else  if index == 3 {
            menuBar.widthBarView?.constant = 150
        } else  if index == 6 {
            menuBar.widthBarView?.constant = 70
        } else {
            menuBar.widthBarView?.constant = 50
        }
        menuBar.collectionView.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func scrollToMenuIndex(menuIndex: Int){
        let indexPath = NSIndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
    
    private func setupHeaderNavbar(ctrl: UIViewController){
//        print("MY ctrl \(ctrl)")
        //setting up navigation menu and options
        let backImage = UIImage(named: "back_arrow")
        navigationController?.navigationBar.barTintColor = UIColor.Theme(alpha: 1.0)
        navigationController?.navigationBar.backgroundColor = UIColor.Theme(alpha: 1.0)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        // Set Logo at tile bar
        let logo = UIImage(named: "ic_toolbar")
        let imageView = UIImageView(image: logo)
        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let menuImage = UIImage(named: "menux40")?.withRenderingMode(.alwaysOriginal)
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        
        let optionImage = UIImage(named: "sharex40")?.withRenderingMode(.alwaysOriginal)
        let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))

        if (ctrl == self) {
            navigationItem.titleView = imageView
            navigationItem.leftBarButtonItems = [menuBarItem]
            navigationItem.rightBarButtonItems = [optionBarItem]
        } else {
            ctrl.navigationItem.titleView = imageView
            ctrl.navigationItem.leftBarButtonItems = [menuBarItem]
            ctrl.navigationItem.rightBarButtonItems = [optionBarItem]
        }
    }
    
    @objc func handleOptions(){
        let text = "Share about your Bocaue Community"
        let url:NSURL = NSURL(string: "http://www.bocaue.com/")!
        let activityVC = UIActivityViewController(activityItems: [text, url], applicationActivities: [])
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func handleMenu(){
        menuLauncher.showMenu()
    }
    
    func showController(menu: Menu){
       
        if (menu.label == "Bocaue Cares") {
            
            let layout = UICollectionViewFlowLayout()
            let newsViewController = NewsViewController(collectionViewLayout: layout)
            navigationController?.pushViewController(newsViewController, animated: true)
            
        } else if (menu.label == "Business Centre"){
            goToBusinessCentre()
        } else if (menu.label == "Transportation"){
            goToTransportation()
        } else if (menu.label == "About Bocaue") {
            goToAbout()
        } else if (menu.label == "Badges") {
            goToBadges()
        } else if (menu.label == "Printable Forms"){
            goToPrintableForms()
        } else if (menu.label == "Survey") {
            goToSurvey()
        } else if (menu.label == "Bocaue Traffic") {
            goToTraffic()
        } else if (menu.label == "Sign in") {
            self.goToLogin()
//            let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .`default`, handler: { _ in
//
//            }))
//
//            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Default action"), style: .`default`, handler: { _ in
//                self.goToLogin()
//            }))
//            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func goToBusinessCentre(){
        
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let businessCenterController = BusinessCenterController(collectionViewLayout: layout)
        setupHeaderNavbar(ctrl: businessCenterController)
        navigationController?.pushViewController(businessCenterController, animated: true)

    }
    
    func goToTraffic(){
        
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let trafficController = TrafficController(collectionViewLayout: layout)
        navigationController?.pushViewController(trafficController, animated: true)
        
        let menuImage = UIImage(named: "menux40")?.withRenderingMode(.alwaysOriginal)
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        
        let optionImage = UIImage(named: "sharex40")?.withRenderingMode(.alwaysOriginal)
        let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "Traffic"
            label.textColor = UIColor.white
            return label
        }()
        trafficController.navigationItem.title = ""
        trafficController.navigationItem.titleView = titleLabel
        trafficController.navigationItem.leftBarButtonItems = [menuBarItem]
        trafficController.navigationItem.rightBarButtonItems = [optionBarItem]
    }
    
    func goToTransportation(){
        
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let transportationController = TransportationController(collectionViewLayout: layout)
        navigationController?.pushViewController(transportationController, animated: true)
        
        let menuImage = UIImage(named: "menux40")?.withRenderingMode(.alwaysOriginal)
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        
        let optionImage = UIImage(named: "sharex40")?.withRenderingMode(.alwaysOriginal)
        let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))
        
//        let searchImage = UIImage(named: "ic_search")?.withRenderingMode(.alwaysOriginal)
//        let searchBarItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(searchOptions))
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "Transportation"
            label.textColor = UIColor.white
            return label
        }()
        transportationController.navigationItem.title = ""
        transportationController.navigationItem.titleView = titleLabel
        transportationController.navigationItem.leftBarButtonItems = [menuBarItem]
        transportationController.navigationItem.rightBarButtonItems = [optionBarItem] //searchBarItem,
        
    }
    
    func goToAbout(){
        
//        let floatMenu = addFloatMenu.floatMenu
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let aboutController = AboutController(collectionViewLayout: layout)
        navigationController?.pushViewController(aboutController, animated: true)
        
    }
    
    func goToLogin(){
        floatMenu.removeFromSuperview()
        statusBarBackground.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let loginController = LoginController(collectionViewLayout: layout)
        self.navigationController?.present(loginController, animated: true, completion: {})
    }
    
    func goToSurvey(){
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let surveyController = SurveyController(collectionViewLayout: layout)
        navigationController?.pushViewController(surveyController, animated: true)
    }
    
    func goToPrintableForms(){
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let printableFormsController = PrintableFormsController(collectionViewLayout: layout)
        navigationController?.pushViewController(printableFormsController, animated: true)
    }
    
    lazy var badgesController: BadgesController = {
        let launcher = BadgesController()
        return launcher
    }()
    
    func goToBadges(){
//        badgesController.setup()
        floatMenu.removeFromSuperview()
        let layout = UICollectionViewFlowLayout()
        let badgesController = BadgesController(collectionViewLayout: layout)
        navigationController?.pushViewController(badgesController, animated: true)
    }
    
    
    private func setupTabMenuBar(){
        view.addSubview(menuBar)
        view.addConstraintsFormat(format: "H:|[v0]|", views: menuBar)
        view.addConstraintsFormat(format: "V:|[v0(50)]", views: menuBar)
        

        menuBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(collectionView)
        //call different cells for indexes
        if (indexPath.item == 1){
            return collectionView.dequeueReusableCell(withReuseIdentifier: twitterId, for: indexPath)
        } else if (indexPath.item == 2){
            return collectionView.dequeueReusableCell(withReuseIdentifier: socialId, for: indexPath)
        } else if (indexPath.item == 3){
            return collectionView.dequeueReusableCell(withReuseIdentifier: healthCellID, for: indexPath)
        } else if (indexPath.item == 4) {
            return collectionView.dequeueReusableCell(withReuseIdentifier: PnpCellID, for: indexPath)
        } else if (indexPath.item == 5) {
            return collectionView.dequeueReusableCell(withReuseIdentifier: BfpCellID, for: indexPath)
        } else if (indexPath.item == 6) {
            return collectionView.dequeueReusableCell(withReuseIdentifier: MdrrmoCellID, for: indexPath)
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height-50)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStatusBar()
        setupCollectionView()
        setupTabMenuBar()
        setupHeaderNavbar(ctrl: self)
        setupFloatMenu()
    }

}
