//
//  TrafficController.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 05/12/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CouchbaseLiteSwift
import GooglePlacesSearchController

class TrafficController: UICollectionViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    // variable animation
    var animator : UIDynamicAnimator!
    var continer : UICollisionBehavior!
    var snap : UISnapBehavior!
    var dynamicItem : UIDynamicItemBehavior!
    var gravity : UIGravityBehavior!
    var panGestureRecognizer : UIPanGestureRecognizer!
    var traffic = "low"
    var iconTraffic = ""
    var titleTraffic = ""
    var floatTapped = "closed"
    
    static var coordinateTapped = CLLocationCoordinate2D()
    static var currentLongitude = CLLocationDegrees()
    static var currentLatitude = CLLocationDegrees()
    
    let googleController = GooglePlacesSearchController(
        apiKey: ApiService().googlePlaceAPIKey,
        placeType: PlaceType.address
    )
    
    // variable map
    var region = MKCoordinateRegion()
    let manager = CLLocationManager()
    var once = 0
    let mapView: MKMapView = {
        let map = MKMapView()
        return map
    }()
    let backDetailView = UIView()
    var trafficPin : TrafficPin!
    var pin : AnnotationPin!
    
    // variable layout
    var tapped = "no"
    var backView = UIView()
    let floatImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_paperplane")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    let floatMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 28
        return view
    }()
    
    let trafficFeedView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 3
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 5
        return view
    }()
    
    let reportView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let closeImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let closeView: UIView = {
        let view = UIView()
        return view
    }()
    
    let arrowIndicatorView: UIView = {
        let view = UIView()
        return view
    }()
    
    let indicatorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "ic_up")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.lightGray
        return imageView;
    }()
    
    let lowLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Low"
        return label
    }()
    
    let lowImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_traffic_light")
        return image
    }()
    
    let lowView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 2
        return view
    }()
    
    let moderateLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Moderate"
        return label
    }()
    
    let moderateImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_traffic_light")
        return image
    }()
    
    let moderateView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 2
        return view
    }()
    
    let heavyLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        label.text = "Heavy"
        return label
    }()
    
    let heavyImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_traffic_light")
        return image
    }()
    
    let heavyView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 2
        return view
    }()
    
    let locationLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = label.font.withSize(13)
        label.textAlignment = .center
        return label
    }()
    
    let descriptionLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(14)
        label.text = "Description:"
        return label
    }()
    
    let typeLable : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        return label
    }()
    
    let typeImage : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = UIColor.white
        imageView.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        imageView.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
        imageView.layer.borderWidth = 7
        imageView.layer.cornerRadius = 50
        return imageView
    }()
    
    let descriptionText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.layer.cornerRadius = 2
        text.setLeftPadding(space: 5)
        text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        return text
    }()
    
    let currentLocationButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("USE CURRENT LOCATION", for: .normal)
        return button
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("SAVE", for: .normal)
        return button
    }()
    
    let searchView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 2
        return view
    }()
    
    let searchImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        return imageView
    }()
    
    let currentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 2
        return view
    }()
    
    let currentImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_current_location")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.rgba(red: 0,green: 122, blue: 255, alpha: 1.0)
        return imageView
    }()
    
    // func @objc

    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.backDetailView.alpha = 0
                    self.trafficFeedView.frame = CGRect(x: 0, y: self.view.frame.height-140, width: self.view.frame.width, height: self.view.frame.height)
                    self.indicatorImage.transform = CGAffineTransform(rotationAngle:0)
                }, completion: { (Bool) in
                    self.backDetailView.removeFromSuperview()
                })
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.backDetailView.alpha = 1
                    self.trafficFeedView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.indicatorImage.transform = CGAffineTransform(rotationAngle: 34.51)
                    self.floatImage.transform = CGAffineTransform(rotationAngle: 0)
                }, completion: { (Bool) in })
            default:
                break
            }
        }
    }
    
    @objc func expand() {
        trafficFeedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(minimize)))
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backDetailView.alpha = 1
            self.trafficFeedView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
             self.indicatorImage.transform = CGAffineTransform(rotationAngle: 34.51)
        }, completion: { (Bool) in })
    }
    
    @objc func minimize(){
        trafficFeedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expand)))
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backDetailView.alpha = 0
            self.trafficFeedView.frame = CGRect(x: 0, y: self.view.frame.height-140, width: self.view.frame.width, height: self.view.frame.height)
            self.indicatorImage.transform = CGAffineTransform(rotationAngle: 0)
        }, completion: { (Bool) in
            self.backDetailView.removeFromSuperview()
        })
    }
    
    @objc func mapViewTapped(sender : UILongPressGestureRecognizer){
        self.iconTraffic = ""
        self.closed()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.floatMenu.frame = CGRect(x: self.view.frame.width-70, y: self.view.frame.height-220, width: 55, height: 55)
            self.floatImage.alpha = 1
        }, completion: { (Bool) in })
        
        removeTrafficLight()
        
        let location = sender.location(in: self.mapView)
        TrafficController.coordinateTapped = mapView.convert(location, toCoordinateFrom: mapView)
        
        pin = AnnotationPin(title: "Report", subtitle: "tell us what you saw", coordinate: TrafficController.coordinateTapped)
        self.mapView.addAnnotation(pin)
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let regionUpdate : MKCoordinateRegion = MKCoordinateRegionMake(TrafficController.coordinateTapped, span)
        self.mapView.setRegion(regionUpdate, animated: true)
    }
    
    @objc func bgDismiss(){
        descriptionText.resignFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.typeImage.alpha = 0
            self.backDetailView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.reportView.frame = CGRect(x: (window.frame.width)/2, y: (window.frame.height)/2, width: 0, height: 0)
                self.typeImage.frame = CGRect(x: (self.reportView.frame.width/2), y: 30, width: 0, height: 0)
                self.lowView.frame = CGRect(x: 20, y: (self.reportView.frame.height/2)-50, width: 0, height: 0)
                self.moderateView.frame = CGRect(x: (self.reportView.frame.width/2), y: (self.reportView.frame.height/2)-50, width: 0, height: 0)
                self.heavyView.frame = CGRect(x: (self.reportView.frame.width-20), y: (self.reportView.frame.height/2)-50, width: 0, height: 0)
                self.currentLocationButton.frame = CGRect(x: 20, y: (self.reportView.frame.height)-50, width: 0, height: 0)
                self.saveButton.frame = CGRect(x: (self.reportView.frame.width-20), y: (self.reportView.frame.height)-50, width: 0, height: 0)
                self.descriptionLable.frame = CGRect(x: 20, y: (self.reportView.frame.height/2)+100, width: 0, height: 0)
                self.descriptionText.frame = CGRect(x: 20, y: (self.reportView.frame.height/2)+130, width: 0, height: 0)
            }
        }, completion: { (Bool) in
            self.reportView.removeFromSuperview()
        })
    }
    
    @objc func FloatMenuTapped() {
        if floatTapped == "opened" {
            closed()
        } else {
            opened()
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.floatImage.transform = CGAffineTransform(rotationAngle: 180)
            }, completion: { (Bool) in
                self.floatTapped = "opened"
            })
        }
    }
    
    @objc func searchOptions(){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let searchWindow = UIWindow(frame: UIScreen.main.bounds)
                searchWindow.rootViewController = UIViewController()
                searchWindow.windowLevel = UIWindowLevelAlert + 1;
                searchWindow.makeKeyAndVisible()
                searchWindow.rootViewController?.present(self.googleController, animated: true, completion: nil)
                
                self.googleController.didSelectGooglePlace { (place) -> Void in
                    self.googleController.isActive = false
                    TransportationController.destination = place.name
                    self.setPin(place : place)
                }
            }
        }
    }
    
    
    @objc func currentLocation(){
        spanToLocation(latitude : TrafficController.currentLatitude, longitude : TrafficController.currentLongitude)
    }
    
    @objc private func saveTapped() {
        let location = TrafficController.coordinateTapped
        //remove all annotations with traffic light annotation
        let allAnnotations = self.mapView.annotations
        DispatchQueue.global(qos: .userInitiated).async(execute : {
            DispatchQueue.main.async(execute: {
                for annotation in allAnnotations {
                    if annotation.title! == "Report"{
                        self.mapView.removeAnnotation(annotation)
                    }
                }
                self.trafficPin = TrafficPin(title: "\(self.titleTraffic)", subtitle: "\(self.traffic)", coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
                self.mapView.addAnnotation(self.trafficPin)
                
                self.pin = AnnotationPin(title: "\(self.titleTraffic)", subtitle: "\(self.traffic)", coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
                self.mapView.addAnnotation(self.pin)
                
                self.mapView.reloadInputViews()
                self.bgDismiss()
            })
        })
    }
    
    
    @objc private func lowTapped() {
        traffic = "low"
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.lowView.alpha = 1.0
            self.moderateView.alpha = 0.5
            self.heavyView.alpha = 0.5
        }, completion: { (Bool) in })
    }
    
    @objc private func moderateTapped() {
        traffic = "moderate"
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.lowView.alpha = 0.5
            self.moderateView.alpha = 1.0
            self.heavyView.alpha = 0.5
        }, completion: { (Bool) in })
    }
    
    @objc private func heavyTapped() {
        traffic = "heavy"
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.lowView.alpha = 0.5
            self.moderateView.alpha = 0.5
            self.heavyView.alpha = 1.0
        }, completion: { (Bool) in })
    }
    
    @objc func selectorTapped(sender: UITapGestureRecognizer) {
        let label = sender.view!.subviews[1] as? UILabel
        let image = label!.text!.lowercased().replacingOccurrences(of: " ", with: "_")
        iconTraffic = "ic_\(image)"
        titleTraffic = label!.text!
        self.closed()
        report(Type: label!.text!, image: "ic_\(image)")
    }
    
    // FUNCTIONS
    
    //Get current position
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let regionUpdate : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
        TrafficController.currentLongitude = location.coordinate.longitude
        TrafficController.currentLatitude = location.coordinate.latitude
        region = regionUpdate
        
        if once == 0 {
            mapView.setRegion(regionUpdate, animated: true)
        }
        once = once+1
        
        self.mapView.showsUserLocation = true
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation is AnnotationPin{
            var newPin = UIImage(named: "ic_traffic_light")
            var transform = CGAffineTransform(scaleX: 0.15, y: 0.15)
            let annotationView = MKAnnotationView(annotation: trafficPin, reuseIdentifier: "setting")
            if iconTraffic != "" {
                newPin = UIImage(named: iconTraffic)
                transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                if traffic == "moderate"{
                    annotationView.backgroundColor = UIColor.TrafficModerate(alpha: 1.0)
                } else if traffic == "heavy" {
                    annotationView.backgroundColor = UIColor.TrafficHeavy(alpha: 1.0)
                } else if traffic == "low" {
                    annotationView.backgroundColor = UIColor.TrafficLow(alpha: 1.0)
                }
            }
            annotationView.image = newPin
            annotationView.transform = transform
            return annotationView
            
        } else if annotation is TrafficPin {
            var newPin = UIImage(named: "ic_traffic_low")
            if traffic == "moderate"{
                newPin = UIImage(named: "ic_traffic_moderate")
            } else if traffic == "heavy" {
                newPin = UIImage(named: "ic_traffic_heavy")
            }
            let annotationView = MKAnnotationView(annotation: trafficPin, reuseIdentifier: "traffic")
            annotationView.image = newPin
            let transform = CGAffineTransform(scaleX: 0.55, y: 0.55)
            annotationView.transform = transform
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation
        let subtitle = annotation!.subtitle! ?? ""
        let title = annotation!.title! ?? ""
        
        removeTrafficLight()
        showVoteDetails(title: title, subtitle: subtitle)
    }
    
    func showVoteDetails(title : String, subtitle: String) {
        print("pin tapped : ",title, subtitle)
    }
    
    func removeTrafficLight() {
        //remove all annotations with traffic light annotation
        let allAnnotations = self.mapView.annotations
        for annotation in allAnnotations {
            if annotation.title! == "Report"{
                self.mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func opened() {
        let subviewsArr = self.mapView.subviews
        for (index, subview) in subviewsArr.enumerated() {
            if (index >= 2 && index != 7) {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    subview.alpha = 1
                }, completion: { (Bool) in })
            }
        }
    }
    
    
    func closed() {
        let subviewsArr = self.mapView.subviews
        
        for (index, subview) in subviewsArr.enumerated() {
            if (index >= 2 && index != 7) {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    subview.alpha = 0
                }, completion: { (Bool) in })
            }
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.floatImage.transform = CGAffineTransform(rotationAngle: 0)
        }, completion: { (Bool) in
            self.floatTapped = "closed"
        })
    }
    
    func setPin(place : PlaceDetails){
        let allAnnotations = self.mapView.annotations
        for annotation in allAnnotations {
            if annotation.subtitle! == "no address"{
                self.mapView.removeAnnotation(annotation)
            }
        }
        
        let latitude = place.coordinate.latitude
        let longitude = place.coordinate.longitude
        
        let annotation = MKPointAnnotation()
        annotation.title = place.name
        annotation.subtitle = place.formattedAddress
        annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        TransportationController.regionPin = region
        
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.mapView.addAnnotation(annotation)
                self.mapView.setRegion(region, animated: true)
            }
        }
    }

    func spanToLocation(latitude : CLLocationDegrees, longitude : CLLocationDegrees) {
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.mapView.setRegion(region, animated: true)
            }
        }
    }
    
    func report(Type: String, image: String) {
        if let window = UIApplication.shared.keyWindow {
            let width = window.frame.width-40
            let height = window.frame.height-220

            closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            lowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(lowTapped)))
            moderateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moderateTapped)))
            heavyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(heavyTapped)))
            saveButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveTapped)))

            locationLable.text = "Bocaue Interchange Overpass Bridge"
            typeLable.text = Type
            typeImage.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)as UIImage?

            backDetailView.backgroundColor = UIColor(white: 0, alpha: 0.6)

            window.addSubview(backDetailView)
            window.addSubview(reportView)
            reportView.addSubview(closeView)
            closeView.addSubview(closeImage)
            reportView.addSubview(typeImage)
            reportView.addSubview(descriptionText)
            reportView.addSubview(typeLable)
            reportView.addSubview(descriptionLable)
            reportView.addSubview(locationLable)
            reportView.addSubview(lowView)
            reportView.addSubview(currentLocationButton)
            reportView.addSubview(saveButton)
            lowView.addSubview(lowImage)
            lowView.addSubview(lowLable)
            reportView.addSubview(moderateView)
            moderateView.addSubview(moderateImage)
            moderateView.addSubview(moderateLable)
            reportView.addSubview(heavyView)
            heavyView.addSubview(heavyImage)
            heavyView.addSubview(heavyLable)


            backDetailView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            reportView.frame = CGRect(x: (window.frame.width)/2, y: (window.frame.height)/2, width: 0, height: 0)
            closeView.frame = CGRect(x: width-25, y: 10, width: 15, height: 15)
            closeImage.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
            typeImage.frame = CGRect(x: (reportView.frame.width/2), y: 30, width: 0, height: 0)
            lowView.frame = CGRect(x: 20, y: (reportView.frame.height/2)-50, width: 0, height: 0)
            moderateView.frame = CGRect(x: (reportView.frame.width/2), y: (reportView.frame.height/2)-50, width: 0, height: 0)
            heavyView.frame = CGRect(x: (reportView.frame.width-20), y: (reportView.frame.height/2)-50, width: 0, height: 0)
            currentLocationButton.frame = CGRect(x: 20, y: (reportView.frame.height)-50, width: 0, height: 0)
            saveButton.frame = CGRect(x: (reportView.frame.width-20), y: (reportView.frame.height)-50, width: 0, height: 0)
            descriptionLable.frame = CGRect(x: 20, y: (reportView.frame.height/2)+100, width: 0, height: 0)
            descriptionText.frame = CGRect(x: 20, y: (reportView.frame.height/2)+130, width: 0, height: 0)

            window.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: typeLable)
            window.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: locationLable)
            window.addConstraintsFormat(format: "V:|-150-[v0(20)][v1(20)]", views: typeLable, locationLable)
            window.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: lowImage)
            window.addConstraintsFormat(format: "H:|[v0]|", views: lowLable)
            window.addConstraintsFormat(format: "H:|[v0]|", views: moderateLable)
            window.addConstraintsFormat(format: "H:|[v0]|", views: heavyLable)
            window.addConstraintsFormat(format: "V:|-5-[v0][v1(25)]-5-|", views: lowImage, lowLable)
            window.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: moderateImage)
            window.addConstraintsFormat(format: "V:|-5-[v0][v1(25)]-5-|", views: moderateImage, moderateLable)
            window.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: heavyImage)
            window.addConstraintsFormat(format: "V:|-5-[v0][v1(25)]-5-|", views: heavyImage, heavyLable)

            backDetailView.alpha = 0
            moderateView.alpha = 0.5
            heavyView.alpha = 0.5


            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.typeImage.alpha = 1
                self.backDetailView.alpha = 1
                self.reportView.frame = CGRect(x: (window.frame.width-width)/2, y: (window.frame.height-height)/2, width: width, height: height)
                self.typeImage.frame = CGRect(x: (self.reportView.frame.width/2-50), y: 30, width: 100, height: 100)
                self.lowView.frame = CGRect(x: 20, y: (self.reportView.frame.height-50)/2, width: 80, height: 110)
                self.moderateView.frame = CGRect(x: (self.reportView.frame.width-80)/2, y: (self.reportView.frame.height-50)/2, width: 80, height: 110)
                self.heavyView.frame = CGRect(x: (self.reportView.frame.width-100), y: (self.reportView.frame.height-50)/2, width: 80, height: 110)
                self.currentLocationButton.frame = CGRect(x: 20, y: (self.reportView.frame.height)-50, width: (self.reportView.frame.width/2)-20, height: 30)
                self.saveButton.frame = CGRect(x: (self.reportView.frame.width/2)+5, y: (self.reportView.frame.height)-50, width:  self.reportView.frame.width/2-25, height: 30)
                self.descriptionLable.frame = CGRect(x: 20, y: (self.reportView.frame.height/2)+100, width: self.reportView.frame.width-40, height: 20)
                self.descriptionText.frame = CGRect(x: 20, y: (self.reportView.frame.height/2)+130, width: self.reportView.frame.width-40, height: 30)
            }, completion: { (Bool) in })
        }
    }
    
    func setUpMapsKit(){
        
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        manager.startUpdatingLocation()
        mapView.showsTraffic = true
        
        self.mapView.delegate = self
        backDetailView.backgroundColor = UIColor(white: 0, alpha: 0.6)
        indicatorImage.alpha = 0.5
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mapViewTapped)))
        
        //gestures for view
        trafficFeedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expand)))
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        trafficFeedView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        trafficFeedView.addGestureRecognizer(swipeDown)
        //
        
        // add mapview
        view.addSubview(mapView)
        //set up float menu
        self.setUpFloatMenu()
        // add to view
        view.addSubview(backDetailView)
        view.addSubview(trafficFeedView)
        trafficFeedView.addSubview(arrowIndicatorView)
        arrowIndicatorView.addSubview(indicatorImage)
        
        view.addSubview(searchView)
        searchView.addSubview(searchImage)
        view.addSubview(currentView)
        currentView.addSubview(currentImage)
        
        // constraints
        backDetailView.alpha = 0
        floatImage.alpha = 0
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-40-10)-[v0(40)]|", views: searchView)
        view.addConstraintsFormat(format: "V:|-10-[v0(40)]|", views: searchView)
        view.addConstraintsFormat(format: "H:|-10-[v0(20)]|", views: searchImage)
        view.addConstraintsFormat(format: "V:|-10-[v0(20)]|", views: searchImage)
        
        view.addConstraintsFormat(format: "H:|-\(view.frame.width-40-10)-[v0(40)]|", views: currentView)
        view.addConstraintsFormat(format: "V:|-50-[v0(40)]|", views: currentView)
        view.addConstraintsFormat(format: "H:|-10-[v0(20)]|", views: currentImage)
        view.addConstraintsFormat(format: "V:|-10-[v0(20)]|", views: currentImage)
        
        view.addConstraintsFormat(format: "H:|[v0]|", views: mapView)
        view.addConstraintsFormat(format: "H:|[v0]|", views: trafficFeedView)
        view.addConstraintsFormat(format: "H:|[v0]|", views: backDetailView)
        view.addConstraintsFormat(format: "V:|[v0]|", views: backDetailView)
        view.addConstraintsFormat(format: "H:|[v0(\(view.frame.width))]|", views: arrowIndicatorView)
        view.addConstraintsFormat(format: "V:|-5-[v0(20)]", views: arrowIndicatorView)
        view.addConstraintsFormat(format: "H:|-\(view.frame.width/2-7.5)-[v0(15)]|", views: indicatorImage)
        view.addConstraintsFormat(format: "V:|[v0(15)]|", views: indicatorImage)
        view.addConstraintsFormat(format: "V:|[v0(\(view.frame.height-200))][v1(200)]", views: mapView, trafficFeedView)
        
        searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchOptions)))
        currentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(currentLocation)))
        
    }
    
    
    
    func setUpFloatMenu(){
        let title = ["Fire Incident", "Road Blocked", "Car Crash", "Road Construction", "Traffic Jam"]
        let icons = ["ic_fire_incident", "ic_road_blocked", "ic_car_crash", "ic_road_construction", "ic_traffic_jam"]
        
        var y = view.frame.height-340
        
        for (index, icon) in icons.enumerated() {
            let titleLabel: UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.text = title[index]
                label.backgroundColor = UIColor.black
                label.font = label.font.withSize(12)
                label.textAlignment = .center
                label.layer.cornerRadius = 3
                label.layer.shadowColor = UIColor.black.cgColor
                label.layer.shadowOpacity = 0.3
                label.layer.shadowOffset = CGSize(width: 3, height: 3)
                label.layer.shadowRadius = 2
                label.viewWithTag(1)
                return label
            }()
            
            let iconImage: UIImageView = {
                let image = UIImageView()
                image.safeAreaInsetsDidChange()
                image.image = UIImage(named: icon)?.withRenderingMode(.alwaysTemplate)as UIImage?
                image.tintColor = UIColor.white
                return image
            }()
            
            let roundView: UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.ThemeDarkSecond(alpha: 1.0)
                view.layer.shadowColor = UIColor.black.cgColor
                view.layer.shadowOpacity = 0.3
                view.layer.shadowOffset = CGSize(width: 3, height: 3)
                view.layer.shadowRadius = 2
                view.layer.cornerRadius = 20
                return view
            }()
            
            self.mapView.addSubview(roundView)
            roundView.addSubview(iconImage)
            roundView.addSubview(titleLabel)
            
            
            iconImage.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
            roundView.alpha = 0
            roundView.frame = CGRect(x: self.view.frame.width-60, y: y, width: 40, height: 40)
            let h = self.view.widthEstimation(text: titleLabel.text!, size: 12)
            self.view.addConstraintsFormat(format: "H:|-(\(-10-h))-[v0(\(h))]|", views: titleLabel)
            self.view.addConstraintsFormat(format: "V:|-8-[v0(25)]|", views: titleLabel)
            
            roundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            y = y-50
        }

        
        mapView.addSubview(floatMenu)
        floatMenu.addSubview(floatImage)
        floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FloatMenuTapped)))
        
        floatImage.frame = CGRect(x: 13, y: 15, width: 28, height: 28)
        floatMenu.frame = CGRect(x: view.frame.width-70, y: view.frame.height-270, width: 0, height: 0)
    }
    
    
    func getLabelsInView(view: UIView) -> [UILabel] {
        var results = [UILabel]()
        for subview in view.subviews as [UIView] {
            if let labelView = subview as? UILabel {
                results += [labelView]
            } else {
                results += getLabelsInView(view: subview)
            }
        }
        return results
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        
        setUpMapsKit()
        //Doña Remedios Trinidad Hwy, Baliuag, Bulacan
    }
    
}

class TrafficPin : NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
    }
}

class AnnotationPin : NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
    }
}
