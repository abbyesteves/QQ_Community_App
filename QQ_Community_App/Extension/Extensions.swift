//
//  extensions.swift
//  QQ_Community_App
//
//  Created by Abby Esteves on 07/11/2017.
//  Copyright © 2017 Abby Esteves. All rights reserved.
//

import UIKit
import TwitterKit

extension UIColor {
    static func Theme(alpha: CGFloat) -> UIColor {
        //green
        //return UIColor(red: 143/255, green: 203/255, blue: 23/255, alpha: alpha)
        return UIColor(red: 75/255, green: 189/255, blue: 197/255, alpha: alpha)
    }
    
    static func ThemeLight(alpha: CGFloat) -> UIColor {
        return UIColor(red: 155/255, green: 229/255, blue: 233/255, alpha: alpha)
    }
    
    static func Background(alpha: CGFloat) -> UIColor {
        return UIColor(red: 231/255, green: 231/255, blue: 243/255, alpha: alpha)
    }
    
    static func ThemeDark(alpha: CGFloat) -> UIColor {
        //return UIColor(red: 129/255, green: 179/255, blue: 20/255, alpha: alpha)
        return UIColor(red: 64/255, green: 164/255, blue: 170/255, alpha: alpha)
    }
    
    static func ThemeDarkSecond(alpha: CGFloat) -> UIColor {
        return UIColor(red: 17/255, green: 97/255, blue: 44/255, alpha: alpha)
        //return UIColor(red: 2/255, green: 58/255, blue: 93/255, alpha: alpha)
    }
    
    static func TrafficLow(alpha: CGFloat) -> UIColor {
        return UIColor(red: 114/255, green: 185/255, blue: 62/255, alpha: alpha)
    }
    
    static func TrafficModerate(alpha: CGFloat) -> UIColor {
        return UIColor(red: 226/255, green: 218/255, blue: 50/255, alpha: alpha)
    }
    
    static func TrafficHeavy(alpha: CGFloat) -> UIColor {
        return UIColor(red: 171/255, green: 62/255, blue: 57/255, alpha: alpha)
    }
    
    static func rgba(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}

extension UIView {

    func addConstraintsFormat(format: String, views: UIView...) {
        var viewsDictionary = [String:UIView]();
        for(index, view) in views.enumerated(){
            let setKey = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[setKey] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }

    func showToast(message : String) {
        if let window = UIApplication.shared.keyWindow {
            let toastLabel = UILabel(frame: CGRect(x: window.frame.size.width/2 - 115, y: window.frame.size.height-100, width: 230, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.textAlignment = .center;
            toastLabel.font = toastLabel.font.withSize(13)
            toastLabel.textAlignment = .center
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 17.5;
            toastLabel.clipsToBounds  =  true
            window.addSubview(toastLabel)
            UIView.animate(withDuration: 7.0, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    
    func estimatedHeight(text: String, attribute: [NSAttributedStringKey: Any]) -> CGFloat {
        let size = CGSize(width: frame.width-150-15, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimated = NSString(string: text).boundingRect(with: size, options: options, attributes: attribute, context: nil)
        return estimated.size.height
    }
    
    func rating(rate: CGFloat, horizontal : CGFloat, vertical : CGFloat){
        var x = rate
        var five = 1
        var h = horizontal
        let v = vertical
        while five <= 5 {
            let Star: UIImageView = {
                let imageView = UIImageView()
                imageView.tintColor = UIColor.ThemeDarkSecond(alpha: 1.0)
                return imageView
            }()
            
            if x <= 0.9 && x >= 0.5 {
                Star.image = UIImage(named: "ic_half_star")?.withRenderingMode(.alwaysTemplate)as UIImage?
            } else if x <= 0.4 && x >= 0 {
                Star.image = UIImage(named: "ic_empty_star")?.withRenderingMode(.alwaysTemplate)as UIImage?
            } else {
                Star.image = UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate)as UIImage?
            }
            
            addSubview(Star)
            addConstraintsFormat(format: "H:|-\(h)-[v0(20)]|", views: Star)
            addConstraintsFormat(format: "V:|-\(v)-[v0(20)]-5-|", views: Star)

            
            five = five+1
            h = h+22
            if x >= 1{
                x = x-1
            } else if x <= 0.9 {
                x = x-x
            }
        }
    }
    
    func alertView(title : String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func textField(labelText : String, viewColor : String) {
        let boarder = UIView()
        let textField: UITextField = {
            let text = UITextField()
            text.borderStyle = .none
            text.backgroundColor = .clear
            text.textColor = UIColor.white
            text.isSecureTextEntry = true
            return text
        }()
        let label : UILabel = {
            let label = UILabel()
            label.text = labelText
            label.textColor = UIColor.white
            label.font = label.font.withSize(12)
            return label
        }()
        
        addSubview(textField)
        textField.addSubview(label)
        textField.addSubview(boarder)
        
        addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: textField)
        addConstraintsFormat(format: "V:|[v0(30)]|", views: textField)
    }
    
    func widthEstimation(text : String, size : CGFloat) -> CGFloat {
        var whitespace = CGFloat()
        for char in text {
            if char == " " {
                whitespace = whitespace + 1
            }
        }
        
        let spaces = whitespace*3.5
        let count = CGFloat(text.count)
        let font = CGFloat(size-spaces)
        
        return count*font
    }
    
    func heightEstimation(text : String, width: CGFloat, size: CGFloat, defaultHeight: CGFloat) -> CGFloat{
        let count = CGFloat(text.count)
        let numberPerLine = CGFloat(width/size)
        let lines = count/numberPerLine
        let height = round(lines)*20
        if height <= 0 {
            return defaultHeight
        }
        return height
    }
    
    func grayScale(image : UIImageView) {
        let context = CIContext(options: nil)
        let filter = CIFilter(name: "CIPhotoEffectNoir")
        filter!.setValue(CIImage(image: image.image!), forKey: kCIInputImageKey)
        let output = filter!.outputImage
        let cgimg = context.createCGImage(output!,from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        image.image = processedImage
    }
    
    func imbedIcon(string : String, image : String) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: image)
        attachment.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
        let attachmentStr = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "")
        myString.append(attachmentStr)
        let myString1 = NSMutableAttributedString(string: string)
        myString.append(myString1)
        return myString
    }
}

extension UITextField {
    
    func setLeftPadding(space: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: space, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "MMMM dd, yyyy" // 'at' hh:mm a
        return formatter
    }()
}

extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension UICollectionViewCell {
    class ListTimelineViewController: TWTRTimelineViewController{
        override func viewDidLoad() {
            super.viewDidLoad()
            self.dataSource = TWTRUserTimelineDataSource(screenName: "PhBocaue", apiClient: TWTRAPIClient())
        }
    }
}

extension Data {
    var htmlToAttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        return Data(utf8).htmlToAttributedString
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


// LAYOUT
//private func setupConstraints(){
//
//}
//
//private func setupView(){
//
//}
//
//private func setupGestures() {
//
//}

