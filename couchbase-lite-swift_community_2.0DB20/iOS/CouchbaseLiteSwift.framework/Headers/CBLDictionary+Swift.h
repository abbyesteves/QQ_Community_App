//
//  CBLDictionary+Swift.h
//  CouchbaseLite
//
//  Created by Pasin Suriyentrakorn on 5/8/17.
//  Copyright © 2017 Couchbase. All rights reserved.
//

#import "CBLDictionary.h"

@interface CBLDictionary ()

@property (weak, nonatomic, nullable) id swiftObject;

@end
