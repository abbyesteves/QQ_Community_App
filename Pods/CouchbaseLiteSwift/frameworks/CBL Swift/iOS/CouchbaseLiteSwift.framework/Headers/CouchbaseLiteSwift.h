//
//  CouchbaseLiteSwift.h
//  CouchbaseLiteSwift
//
//  Created by Jens Alfke on 2/9/17.
//  Copyright © 2017 Couchbase. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for CouchbaseLiteSwift.
FOUNDATION_EXPORT double CouchbaseLiteSwiftVersionNumber;

//! Project version string for CouchbaseLiteSwift.
FOUNDATION_EXPORT const unsigned char CouchbaseLiteSwiftVersionString[];

#import "CouchbaseLite.h"
#import "CBLArray+Swift.h"
#import "CBLDictionary+Swift.h"
#import "CBLPredicateQuery.h"
